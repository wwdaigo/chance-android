package br.com.iterative.chance.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.FeedAdapter;
import br.com.iterative.chance.models.Post;
import br.com.iterative.chance.models.VerPostRequest;
import br.com.iterative.chance.models.VerPostResponse;
import br.com.iterative.chance.views.FeedItemView;
import br.com.iterative.rede.chance.R;

public class FeedDetalheFragment extends Fragment {

    private FeedItemView feedItemView;
    private int postId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        feedItemView = (FeedItemView) inflater.inflate(R.layout.feed_item,
                container, false);

        return feedItemView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Texto na TitleBar indicando que o usuario esta vendo o perfil de
        // algu�m
        ((PrincipalActivity) getActivity()).setTitleBarText("Post");

        this.postId = getArguments().getInt(Helper.POST_ID, -1);

        new ObterPerfilTask().execute();
    }

    private final class ObterPerfilTask extends
            AsyncTask<Object, Object, VerPostResponse> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            getActivity().findViewById(R.id.tela).setVisibility(View.INVISIBLE);
            getActivity().findViewById(R.id.progressoItem).setVisibility(
                    View.VISIBLE);
        }

        @Override
        protected void onPostExecute(VerPostResponse result) {
            super.onPostExecute(result);

            Post post = new Post();
            post.Autor = result.Autor;
            post.DataCriacao = result.DataCriacao;
            post.Localizacao = result.Localizacao;
            post.Texto = result.Texto;
            post.Contagios = result.Contagios;
            post.Comentarios = result.Comentarios;
            post.Midias = result.Midias;
            post.Pessoas = result.Pessoas;
            post.PostId = result.PostId;
            post.JaContagiei = result.JaContagiei;
            post.JaDenunciei = result.JaDenunciei;

            Context context = FeedDetalheFragment.this.getActivity();

            try {
                FeedAdapter.carregaFeedItemView(FeedDetalheFragment.this.getActivity(), context,
                        FeedDetalheFragment.this.feedItemView, post, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

            getActivity().findViewById(R.id.tela).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.progressoItem).setVisibility(
                    View.INVISIBLE);
        }

        @Override
        protected VerPostResponse doInBackground(Object... arg0) {
            VerPostRequest request = new VerPostRequest(Helper.getToken(),
                    FeedDetalheFragment.this.postId);

            return Helper.post(Helper.URL_POST_POST_VER_POST, request,
                    VerPostResponse.class);
        }
    }
}
