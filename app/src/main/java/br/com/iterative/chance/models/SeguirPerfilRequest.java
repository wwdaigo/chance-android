package br.com.iterative.chance.models;

public class SeguirPerfilRequest extends RequestBase {
	public int PerfilId;

	public SeguirPerfilRequest(String Token, int PerfilId) {
		this.Token = Token;
		this.PerfilId = PerfilId;
	}
}
