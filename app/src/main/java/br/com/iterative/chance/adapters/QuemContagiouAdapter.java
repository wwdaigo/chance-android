package br.com.iterative.chance.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.models.Autor;
import br.com.iterative.chance.models.Contagiou;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 12/22/14.
 */
public class QuemContagiouAdapter extends BaseAdapter {


    private ImageView fotoPerfil;
    private TextView nomePerfil;
    private View cell;
    private Context context;
    private Contagiou[] pessoas;
    private DialogInterface dialogInterface;

    private LayoutInflater layoutInflater;

    public QuemContagiouAdapter(Context context, Contagiou[] pessoas, DialogInterface dialogInterface) {

        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.pessoas = pessoas;
        this.dialogInterface = dialogInterface;

    }


    @Override
    public int getCount() {
        return pessoas.length;
    }

    @Override
    public Autor getItem(int i) {
        return pessoas[i].Autor;
    }

    @Override
    public long getItemId(int i) {
        return pessoas[i].Autor.PerfilId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final Autor pessoa = getItem(i);

        LinearLayout marcarBeneficiadoItemView;

        if (view != null) {
            marcarBeneficiadoItemView = (LinearLayout)view;
        } else {
            marcarBeneficiadoItemView = (LinearLayout) View.inflate(context,
                    R.layout.marcar_beneficiado_item, null);
        }



        fotoPerfil = (ImageView) marcarBeneficiadoItemView.findViewById(R.id.foto_perfil);
        nomePerfil = (TextView) marcarBeneficiadoItemView.findViewById(R.id.nome_perfil);

        Helper.downloadImageFromFoto(fotoPerfil, pessoa.Foto);
        nomePerfil.setText(pessoa.Apelido);



        cell = marcarBeneficiadoItemView.findViewById(R.id.cell);

        cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentActivity activity = (FragmentActivity) context;
                FragmentManager fragmentManager = activity
                        .getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager
                        .beginTransaction();
                transaction.addToBackStack(null);

                Fragment fragment = new PerfilFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Helper.PERFIL_ID, pessoa.PerfilId);
                fragment.setArguments(bundle);

                transaction.replace(
                        R.id.main_frame,
                        fragment,
                        "fragment"
                                + fragmentManager
                                .getBackStackEntryCount());
                transaction.commit();

                dialogInterface.dismiss();

            }
        });


        Helper.downloadImageFromFoto(fotoPerfil, pessoa.Foto);
        nomePerfil.setText(pessoa.Apelido);


        return marcarBeneficiadoItemView;
    }
}
