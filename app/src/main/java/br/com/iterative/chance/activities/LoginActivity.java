package br.com.iterative.chance.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.models.AutenticarContaRequest;
import br.com.iterative.chance.models.AutenticarContaResponse;
import br.com.iterative.chance.models.RecuperarSenhaRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.rede.chance.R;

public class LoginActivity extends Activity {

    private ProgressDialog progressDialog;

    private br.com.iterative.chance.activities.PrincipalActivity activity;

	private final class RecuperarSenhaListener implements DialogInterface.OnClickListener {
		private final TextView email;

		private RecuperarSenhaListener(TextView email) {
			this.email = email;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {

			new AsyncTask<String, Object, ResponseBase>() {

				protected void onPostExecute(ResponseBase result) {
					if (result.Sucesso) {
						Toast.makeText(LoginActivity.this,
								getResources().getString(R.string.response_recuperar_senha),
								Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(LoginActivity.this, result.Mensagem, Toast.LENGTH_LONG).show();
					}
				};

				protected ResponseBase doInBackground(String... params) {
					RecuperarSenhaRequest request = new RecuperarSenhaRequest(params[0]);

					return Helper.post(Helper.BASE_URL + "/conta/recuperarsenha", request, ResponseBase.class);

				}
			}.execute(email.getText().toString());
		}
	}

	private final class AutenticarTask extends AsyncTask<EditText, Object, AutenticarContaResponse> {



		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected void onPostExecute(AutenticarContaResponse result) {
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			if (result.Sucesso) {

				Helper.setToken(result.Token);
				Helper.setPerfil(result.PerfilId);
                Helper.setLoggedWithFacebook(false);

				Intent intent = new Intent(getBaseContext(), PrincipalActivity.class);
				startActivity(intent);
				finish();
			} else {
				new AlertDialog.Builder(LoginActivity.this).setMessage(result.Mensagem).setPositiveButton("OK", null).create().show();
			}
		}

		@Override
		protected AutenticarContaResponse doInBackground(EditText... params) {
			AutenticarContaRequest request = new AutenticarContaRequest(params[0].getText().toString(), params[1].getText().toString());

			return Helper.post(Helper.BASE_URL + "Conta/Autenticar", request, AutenticarContaResponse.class);
		}
	}

	private EditText email;
	private EditText senha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.login);

		email = (EditText) findViewById(R.id.email);
		senha = (EditText) findViewById(R.id.senha);

		findViewById(R.id.botaoEsqueceuSenha).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

				View view = View.inflate(LoginActivity.this, R.layout.esqueci_senha, null);

				final TextView email = (TextView) view.findViewById(R.id.email);

				builder.setView(view);

				builder.setTitle(getResources().getString(R.string.recuperar_senha));

				builder.setPositiveButton(getResources().getString(R.string.enviar), new RecuperarSenhaListener(email));

				builder.create().show();

			}
		});

		findViewById(R.id.botaoEntrar).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (TextUtils.isEmpty(email.getText())) {
					Toast.makeText(LoginActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					email.requestFocus();
				} else if (TextUtils.isEmpty(senha.getText())) {
					Toast.makeText(LoginActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					senha.requestFocus();
				} else {
                    progressDialog = Helper.showProgressDialog(LoginActivity.this);
					new AutenticarTask().execute(email, senha);
				}
			}
		});
	}
}
