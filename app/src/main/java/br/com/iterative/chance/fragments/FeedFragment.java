package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import br.com.iterative.chance.FacebookUtil;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.FeedAdapter;
import br.com.iterative.chance.adapters.FeedEmptyAdapter;
import br.com.iterative.chance.models.FeedsRequest;
import br.com.iterative.chance.models.FeedsResponse;
import br.com.iterative.rede.chance.R;

public class FeedFragment extends Fragment {

    private ListView listView;
    private int perfilId;
    private ProgressBar progressBar;
    private boolean recarregarTela = false;
    private String urlPost;
    private VerFeedsTask verFeedsTask;
    private boolean cu;
    private FacebookUtil facebookUtil;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((PrincipalActivity) getActivity()).setLoading(true);

        View view = inflater.inflate(R.layout.feed, container, false);

        this.listView = ((ListView) view.findViewById(R.id.posts));
        this.progressBar = (ProgressBar) view.findViewById(R.id.progresso);

        recarregarTela = true;

        facebookUtil = new FacebookUtil(savedInstanceState, getActivity());


        this.cu = getArguments().getBoolean("CodigoUnico", false);

        this.urlPost = getArguments().getString(Helper.URL);
        this.perfilId = getArguments().getInt(Helper.PERFIL_ID);

        if (urlPost.equals(Helper.URL_POST_FEED_DE_AMIGOS)) {
            ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.ajuda_de_amigos));
        } else if (urlPost.equals(Helper.URL_POST_FEED_GLOBAL)) {
            ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.contagie_se));
        } else if (urlPost.equals(Helper.URL_POST_FEED_DE_PERFIL)) {
            ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.perfil));
        }

        if (recarregarTela) {
            recarregarTela = false;

            //this.progressBar.setVisibility(View.VISIBLE);
            this.listView.setVisibility(View.INVISIBLE);

            verFeedsTask = new VerFeedsTask();
            verFeedsTask.execute(Helper.getToken(), String
                    .valueOf(this.perfilId < 0 ? Helper.getPerfil()
                            : this.perfilId));
        }


        return view;
    }

    @Override
    public void onDestroy() {
        if (verFeedsTask != null) {
            verFeedsTask.cancel(true);
        }
        this.listView.setAdapter(null);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    private final class VerFeedsTask extends
            AsyncTask<String, Object, FeedsResponse> {

        protected FeedsResponse doInBackground(String... params) {
            try {
                if (!isCancelled()) {
                    FeedsRequest request = new FeedsRequest(params[0],
                            Integer.parseInt(params[1]));

                    return Helper.post(FeedFragment.this.urlPost, request,
                            FeedsResponse.class);
                } else {
                    return null;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(FeedsResponse result) {
            if (result == null || isCancelled()) {
                ((PrincipalActivity) getActivity()).setLoading(false);
                return;
            }
            if (result.Sucesso) {

                if (result.QtdAmigos == 0 && cu) {

                    ListAdapter adapter = new FeedEmptyAdapter(getActivity());
                    FeedFragment.this.listView.setAdapter(adapter);
                    FeedFragment.this.listView.setVisibility(View.VISIBLE);

                } else {
                    ListAdapter adapter = new FeedAdapter(FeedFragment.this, result.Posts, facebookUtil);
                    FeedFragment.this.listView.setAdapter(adapter);
                    FeedFragment.this.listView.setVisibility(View.VISIBLE);
                }
            } else {
                new AlertDialog.Builder(FeedFragment.this.getActivity())
                        .setMessage(result.Mensagem)
                        .setPositiveButton("OK", null).create().show();
            }

            ((PrincipalActivity) getActivity()).setLoading(false);
        }
    }
}
