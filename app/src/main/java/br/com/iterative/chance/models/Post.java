package br.com.iterative.chance.models;

import java.io.Serializable;

import br.com.iterative.chance.Helper;

public class Post implements Serializable {

	private static final long serialVersionUID = -5618960051909397564L;

	public boolean JaContagiei;
	
	public boolean JaDenunciei;
	
	public Pessoa Autor;

	public int Comentarios;

	public int Contagios;

	public String DataCriacao;

	public Localizacao Localizacao;

	public Midia[] Midias;

	public Pessoa[] Pessoas;

	public int PostId;

	public String Texto;

	public String getTruncatedTexto() {
		if (Texto == null) {
			return "";
		} else if (Texto.length() > Helper.SIZE_TRUNCATED_TEXTO) {
			return Texto.substring(0, Helper.SIZE_TRUNCATED_TEXTO) + "...";
		} else {
			return Texto;
		}
	}

	public boolean isContagiado() {
		return JaContagiei;
	}

	public boolean isDenunciado() {
		return JaDenunciei;
	}

	public void setContagiado(boolean contagiado) {
		this.JaContagiei = contagiado;
	}

	public void setDenunciado(boolean denunciado) {
		this.JaDenunciei = denunciado;
	}
}
