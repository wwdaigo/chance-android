package br.com.iterative.chance.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.com.iterative.chance.Helper;
import br.com.iterative.rede.chance.R;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.chance.tasks.SeguirPerfilTask;
import br.com.iterative.chance.tasks.SolicitarAmizadeTask;
import br.com.iterative.chance.views.ProcurarAmigoItemView;

public class ListarAmigosAdapter extends BaseAdapter {

	private Pessoa[] dataSet;

	public ListarAmigosAdapter(Pessoa[] pessoas) {
		this.dataSet = pessoas;
	}

	private void configurarBotoes(
			final ProcurarAmigoItemView procurarAmigoItemView,
			final Pessoa pessoa, final Context context) {

		procurarAmigoItemView.getBotaoAdicionar().setVisibility(View.INVISIBLE);
		procurarAmigoItemView.getBotaoSeguir().setVisibility(View.INVISIBLE);
	}

	@Override
	public int getCount() {
		return this.dataSet.length;
	}

	@Override
	public Object getItem(int position) {
		return this.dataSet[position];
	}

	@Override
	public long getItemId(int position) {
		return this.dataSet[position].PerfilId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ProcurarAmigoItemView procurarAmigoItemView;
		final Pessoa pessoa = this.dataSet[position];
		final Context context = parent.getContext();

		if (convertView != null) {
			procurarAmigoItemView = (ProcurarAmigoItemView) convertView;
		} else {
			procurarAmigoItemView = (ProcurarAmigoItemView) View.inflate(
					context, R.layout.procurar_amigo_item, null);
		}

		Helper.downloadImageFromFoto(procurarAmigoItemView.getFotoPerfil(),
				pessoa.Foto);

		procurarAmigoItemView.getFotoPerfil().setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						FragmentActivity activity = (FragmentActivity) v
								.getContext();
						FragmentManager fragmentManager = activity
								.getSupportFragmentManager();
						FragmentTransaction transaction = fragmentManager
								.beginTransaction();
						transaction.addToBackStack(null);

						Fragment fragment = new PerfilFragment();
						Bundle bundle = new Bundle();
						bundle.putInt(Helper.PERFIL_ID, pessoa.PerfilId);
						fragment.setArguments(bundle);

						transaction.replace(
								R.id.main_frame,
								fragment,
								"fragment"
										+ fragmentManager
												.getBackStackEntryCount());
						transaction.commit();
					}
				});

		procurarAmigoItemView.getNomePerfil().setText(pessoa.Apelido);

		configurarBotoes(procurarAmigoItemView, pessoa, context);

		return procurarAmigoItemView;
	}

}
