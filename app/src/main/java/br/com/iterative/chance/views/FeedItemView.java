package br.com.iterative.chance.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.iterative.chance.adapters.FeedAdapter;
import br.com.iterative.rede.chance.R;

public class FeedItemView extends FrameLayout {

	private TextView apelido;

	private LinearLayout beneficiados;

	private ImageButton botaoDenunciar;

	private TextView botaoVerMais;

	private ImageButton botaoContagiar;

	private ImageButton botaoComentar;
	private Button botaoExcluir;

	private LinearLayout comentar;

	private TextView comentarios;

	private TextView contagiados;

	private LinearLayout contagiar;

	private TextView dataHora;

	private ImageView imagem;

	private ImageView imagemPerfil;

	private ImageView imgBeneficiado1;

	private ImageView imgBeneficiado2;

	private ImageView imgBeneficiado3;

	private ImageView imgBeneficiado4;

	private TextView local;

	private LinearLayout midias;
	
	private LinearLayout midia;

	private TextView texto;
	
	private FeedAdapter feedAdapter;

    private View buttonFacebook;

    public View getButtonFacebook() {
        return buttonFacebook;
    }

    public FeedAdapter getFeedAdapter() {
		return feedAdapter;
	}

	public void setFeedAdapter(FeedAdapter feedAdapter) {
		this.feedAdapter = feedAdapter;
	}

	public FeedItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TextView getApelido() {
		return apelido;
	}

	public LinearLayout getBeneficiados() {
		return beneficiados;
	}

	public ImageButton getBotaoDenunciar() {
		return botaoDenunciar;
	}

	public TextView getBotaoVerMais() {
		return botaoVerMais;
	}

	public ImageButton getBotaoContagiar() {
		return botaoContagiar;
	}

	public ImageButton getBotaoComentar() {
		return botaoComentar;
	}

	public LinearLayout getComentar() {
		return comentar;
	}

	public TextView getComentarios() {
		return comentarios;
	}

	public TextView getContagiados() {
		return contagiados;
	}

	public LinearLayout getContagiar() {
		return contagiar;
	}

	public TextView getDataHora() {
		return dataHora;
	}

	public ImageView getImagem() {
		return imagem;
	}

	public ImageView getImagemPerfil() {
		return imagemPerfil;
	}

	public ImageView getImgBeneficiado1() {
		return imgBeneficiado1;
	}

	public ImageView getImgBeneficiado2() {
		return imgBeneficiado2;
	}

	public ImageView getImgBeneficiado3() {
		return imgBeneficiado3;
	}

	public ImageView getImgBeneficiado4() {
		return imgBeneficiado4;
	}

	public TextView getLocal() {
		return local;
	}

	public LinearLayout getMidias() {
		return midias;
	}
	
	public LinearLayout getMidia() {
		return midia;
	}

	public TextView getTexto() {
		return texto;
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

        buttonFacebook = (View)this.findViewById(R.id.button_facebook);

		imagemPerfil = (ImageView) this.findViewById(R.id.imagemPerfil);
		apelido = ((TextView) this.findViewById(R.id.apelido));
		dataHora = ((TextView) this.findViewById(R.id.dataHora));
		local = ((TextView) this.findViewById(R.id.local));
		texto = ((TextView) this.findViewById(R.id.texto));


		contagiados = ((TextView) this.findViewById(R.id.contagiados));
		botaoContagiar = ((ImageButton) this.findViewById(R.id.botaoContagiar));
		contagiar = ((LinearLayout) this.findViewById(R.id.contagiar));

		comentarios = ((TextView) this.findViewById(R.id.comentarios));
		botaoComentar = ((ImageButton) this.findViewById(R.id.botaoComentar));
		comentar = ((LinearLayout) this.findViewById(R.id.comentar));
		botaoVerMais = ((TextView) this.findViewById(R.id.botaoVerMais));
		botaoDenunciar = ((ImageButton) this.findViewById(R.id.botaoDenunciar));
		botaoExcluir = ((Button) findViewById(R.id.botaoExcluir));
		imagem = ((ImageView) this.findViewById(R.id.imagem));
		beneficiados = ((LinearLayout) this.findViewById(R.id.beneficiados));
		imgBeneficiado1 = ((ImageView) this.findViewById(R.id.imgBeneficiado1));
		imgBeneficiado2 = ((ImageView) this.findViewById(R.id.imgBeneficiado2));
		imgBeneficiado3 = ((ImageView) this.findViewById(R.id.imgBeneficiado3));
		imgBeneficiado4 = ((ImageView) this.findViewById(R.id.imgBeneficiado4));
		midias = (LinearLayout) this.findViewById(R.id.midias);
		midia = (LinearLayout) this.findViewById(R.id.midia);
	}
	private int postId;
	
	
	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public void excluir(){
		getFeedAdapter().excluir(postId);
	}

	public Button getBotaoExcluir() {
		return botaoExcluir;
	}

	public void setBotaoExcluir(Button botaoExcluir) {
		this.botaoExcluir = botaoExcluir;
	}
}
