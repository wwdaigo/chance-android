package br.com.iterative.chance.models;

import android.widget.MultiAutoCompleteTextView;

public class PedirAjudaRequest extends RequestBase {
	public PedirAjudaRequest(MultiAutoCompleteTextView texto, MultiAutoCompleteTextView textoContato, Localizacao localizacao, int categoriaId, int tipoAjuda) {
		this.Texto = texto.getText().toString();
		this.TextoDeContato = textoContato.getText().toString();
		this.Localizacao = localizacao;
		this.CategoriaId = categoriaId;
		this.TipoAjuda = tipoAjuda;
	}

	public String Texto;
	public String TextoDeContato;
	public int CategoriaId;
	public Localizacao Localizacao;
	public int TipoAjuda;
	public int PedidoId;
}
