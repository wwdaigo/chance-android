package br.com.iterative.chance.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.iterative.chance.Helper;
import br.com.iterative.rede.chance.R;
import br.com.iterative.chance.models.Pessoa;

public class BenfeitoresAdapter extends BaseAdapter {

	@Override
	public int getCount() {
		return dataSet.length;
	}

	public Object getItem(int arg0) {
		return this.dataSet[arg0];
	}

	@Override
	public long getItemId(int position) {
		return this.dataSet[position].PerfilId;
	}

	private Pessoa[] dataSet;

	public BenfeitoresAdapter(Pessoa[] dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView != null) {
			return convertView;
		}

		View view = View.inflate(parent.getContext(), R.layout.benfeitor_item,
				null);

		Helper.downloadImageFromFoto(
				((ImageView) view.findViewById(R.id.imageView1)),
				dataSet[position].Foto);

		((TextView) view.findViewById(R.id.textView1))
				.setText(dataSet[position].Apelido);
		return view;
	}

}
