package br.com.iterative.chance.models;

public class FeedsRequest extends RequestBase {
	public int PerfilId;

	public FeedsRequest(String Token, int PerfilId) {
		this.Token = Token;
		this.PerfilId = PerfilId;
	}
}
