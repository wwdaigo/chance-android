package br.com.iterative.chance.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.fragments.ProcurarAmigoFragment;
import br.com.iterative.chance.views.FeedItemView;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 10/17/14.
 */
public class FeedEmptyAdapter extends BaseAdapter {

    private Activity mContext;

    public FeedEmptyAdapter(Activity activity){

        this.mContext = activity;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int i) {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;

        if (view == null) {
            LayoutInflater li = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.item_feed_empty, null);

            Button btnAmigos = (Button)v.findViewById(R.id.button_amigos);

            btnAmigos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Fragment fragment = new ProcurarAmigoFragment();

                    FragmentActivity activity = (FragmentActivity)view.getContext();
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.addToBackStack(null);

                    transaction.replace(R.id.main_frame, fragment);
                    transaction.commit();


                }
            });



        }

        return v;

    }
}
