package br.com.iterative.chance.tasks;

import android.os.AsyncTask;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.listeners.QuemContagiouListener;
import br.com.iterative.chance.models.BuscarComentariosRequest;
import br.com.iterative.chance.models.QuemContagiouResponse;

/**
 * Created by pedrohenriqueborges on 12/22/14.
 */
public class QuemContagiouTask extends AsyncTask<Integer, Object, QuemContagiouResponse> {


    QuemContagiouListener listener;


    public QuemContagiouTask(QuemContagiouListener listener) {
        this.listener = listener;
    }

    @Override
    protected QuemContagiouResponse doInBackground(Integer... integers) {

        BuscarComentariosRequest request = new BuscarComentariosRequest(integers[0]);

        return Helper.post(Helper.BASE_URL + "post/QuemContagiou", request, QuemContagiouResponse.class);

    }

    @Override
    protected void onPostExecute(QuemContagiouResponse quemContagiouResponse) {
        super.onPostExecute(quemContagiouResponse);

        listener.quemContagiou(quemContagiouResponse.Contagiou);

    }
}
