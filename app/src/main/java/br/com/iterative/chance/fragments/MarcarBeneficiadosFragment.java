package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.adapters.MarcarBeneficiadosAdapter;
import br.com.iterative.chance.models.ProcurarAmigosRequest;
import br.com.iterative.chance.models.ProcurarAmigosResponse;
import br.com.iterative.rede.chance.R;

public class MarcarBeneficiadosFragment extends Fragment {

    TextView txtProcurar;
    private ListView listaBeneficiados;
    private View screenLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        screenLayout = inflater.inflate(R.layout.marcar_beneficiados, container,
                false);

        return screenLayout;
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        listaBeneficiados = (ListView) screenLayout
                .findViewById(R.id.listaBeneficiados);
        txtProcurar = (TextView) screenLayout.findViewById(R.id.txtProcurar);

        ((Button) screenLayout.findViewById(R.id.botaoBuscar))
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        new RealizarBuscaTask()
                                .execute(MarcarBeneficiadosFragment.this.txtProcurar
                                        .getText().toString());

                    }
                });

    }

    @Override
    public void onStop() {
        this.listaBeneficiados.setAdapter(null);
        super.onStop();
    }

    private final class RealizarBuscaTask extends
            AsyncTask<String, Object, ProcurarAmigosResponse> {

        private String query;

        @Override
        protected ProcurarAmigosResponse doInBackground(String... params) {

            query = params[0];
            ProcurarAmigosRequest request = new ProcurarAmigosRequest(
                    Helper.getToken(), query);

            return Helper.post(Helper.URL_POST_RELACIONAMENTO_PROCURAR_AMIGOS,
                    request, ProcurarAmigosResponse.class);
        }

        protected void onPostExecute(ProcurarAmigosResponse result) {
            if (result.Sucesso) {

                MarcarBeneficiadosAdapter adapter = new MarcarBeneficiadosAdapter(
                        result.Pessoas);
                MarcarBeneficiadosFragment.this.listaBeneficiados
                        .setAdapter(adapter);
                MarcarBeneficiadosFragment.this.listaBeneficiados
                        .setVisibility(View.VISIBLE);
            } else {
                new AlertDialog.Builder(
                        MarcarBeneficiadosFragment.this.getActivity())
                        .setMessage(result.Mensagem)
                        .setPositiveButton("OK", null).create().show();
            }
        }
    }
}
