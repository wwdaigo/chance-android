package br.com.iterative.chance.models;

public class VerPostResponse extends ResponseBase {

	public Pessoa Autor;
	public int Comentarios;
	public int Contagios;
	public String DataCriacao;
	public boolean JaContagiei;
	public boolean JaDenunciei;
	public Localizacao Localizacao;
	public Midia[] Midias;
	public Pessoa[] Pessoas;
	public int PostId;
	public String Texto;
}
