package br.com.iterative.chance.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.iterative.rede.chance.R;

public class CategoriaAjudaItemView extends LinearLayout {

	private LinearLayout cell;
	private ImageView foto;
	private TextView nome;
	private TextView descricao;

    public CheckBox getCheckSelecionada() {
        return checkSelecionada;
    }

    private CheckBox checkSelecionada;

	public CategoriaAjudaItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LinearLayout getCell() {
		return cell;
	}

	public ImageView getFoto() {
		return foto;
	}

	public TextView getNome() {
		return nome;
	}

	public TextView getDescricao() {
		return descricao;
	}

	@Override
	protected void onFinishInflate() {
		foto = (ImageView) findViewById(R.id.foto);
		nome = (TextView) findViewById(R.id.nome);
		descricao = (TextView) findViewById(R.id.descricao);
		cell = (LinearLayout) findViewById(R.id.cell);

        checkSelecionada = (CheckBox)findViewById(R.id.check_selecionada);

	}
}
