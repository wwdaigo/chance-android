package br.com.iterative.chance.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import br.com.iterative.chance.Helper;
import br.com.iterative.rede.chance.R;
import br.com.iterative.chance.models.CriarContaRequest;
import br.com.iterative.chance.models.CriarContaResponse;

public class CadastroActivity extends Activity {

	private final class CriarContaTask extends AsyncTask<EditText, Object, CriarContaResponse> {

		protected void onPostExecute(CriarContaResponse result) {
			if (result.Sucesso) {
				Helper.setPerfil(result.PerfilId);
				Helper.setToken(result.Token);
                Helper.setLoggedWithFacebook(false);

				Intent intent = new Intent(getBaseContext(), PrincipalActivity.class);
				startActivity(intent);
				finish();
			} else {
				new AlertDialog.Builder(CadastroActivity.this).setMessage(result.Mensagem).setPositiveButton("OK", null).create().show();
			}
		}

		@Override
		protected CriarContaResponse doInBackground(EditText... params) {
			CriarContaRequest request = new CriarContaRequest(params[0].getText().toString(), params[1].getText().toString(), params[2].getText().toString(),
					params[3].getText().toString());
			
			return Helper.post(Helper.BASE_URL + "Conta/Criar", request, CriarContaResponse.class);
		}
	}

	private EditText nome;
	private EditText sobrenome;
	private EditText email;
	private EditText senha;
	private EditText confirmarSenha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro);

		nome = (EditText) findViewById(R.id.nome);
		sobrenome = (EditText) findViewById(R.id.sobrenome);
		email = (EditText) findViewById(R.id.email);
		senha = (EditText) findViewById(R.id.senha);
		confirmarSenha = (EditText) findViewById(R.id.confirmarSenha);

		findViewById(R.id.botaoCadastrar).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (TextUtils.isEmpty(nome.getText().toString())) {
					Toast.makeText(CadastroActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					nome.requestFocus();
				} else

				if (TextUtils.isEmpty(sobrenome.getText().toString())) {
					Toast.makeText(CadastroActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					sobrenome.requestFocus();
				} else

				if (TextUtils.isEmpty(email.getText().toString())) {
					Toast.makeText(CadastroActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					email.requestFocus();
				} else

				if(!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
					Toast.makeText(CadastroActivity.this, getResources().getString(R.string.email_invalido), Toast.LENGTH_LONG).show();
					email.requestFocus();
				} else

				if (TextUtils.isEmpty(senha.getText().toString())) {
					Toast.makeText(CadastroActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					senha.requestFocus();
				} else

				if (TextUtils.isEmpty(confirmarSenha.getText().toString())) {
					Toast.makeText(CadastroActivity.this, getResources().getString(R.string.campo_obrigatorio), Toast.LENGTH_LONG).show();
					confirmarSenha.requestFocus();
				} else

				if (!senha.getText().toString().equals(confirmarSenha.getText().toString())) {
					Toast.makeText(getBaseContext(), getResources().getString(R.string.senhas_nao_coincidem), Toast.LENGTH_SHORT).show();
					return;
				} else {
					new CriarContaTask().execute(nome, sobrenome, email, senha);
				}
			}
		});
	}
}
