package br.com.iterative.chance.models;

public class ComentarRequest extends RequestBase {

	public int PostId;
	public String Comentario;

	public ComentarRequest(String Token, int PostId, String Comentario) {
		this.Token = Token;
		this.PostId = PostId;
		this.Comentario = Comentario;
	}
}
