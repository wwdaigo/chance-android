package br.com.iterative.chance.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import br.com.iterative.rede.chance.R;

public class SobreActivity extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sobre);
		WebView mbrowser = (WebView) findViewById(R.id.webView);
		mbrowser.getSettings().setJavaScriptEnabled(true);
		mbrowser.loadUrl(getResources().getString(R.string.url_sobre));
	}
}
