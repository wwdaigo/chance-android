package br.com.iterative.chance.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pedrohenriqueborges on 10/27/14.
 */
public class Filtro implements Serializable {

    int raio;
    Localizacao localizacao;
    TipoFiltro tipoFiltro;
    List<Integer> categoriaId;

    public List<Integer> getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(List<Integer> categoriaId) {
        this.categoriaId = categoriaId;
    }

    public int getRaio() {
        return raio;
    }

    public void setRaio(int raio) {
        this.raio = raio;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public TipoFiltro getTipoFiltro() {
        return tipoFiltro;
    }

    public void setTipoFiltro(TipoFiltro tipoFiltro) {
        this.tipoFiltro = tipoFiltro;
    }

    public enum TipoFiltro{

        filtroAmigos,
        filtroProximidade

    }

}

