package br.com.iterative.chance;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import br.com.iterative.chance.activities.GpsActivity;
import br.com.iterative.chance.models.CriarPostRequest;
import br.com.iterative.chance.models.Midia;
import br.com.iterative.chance.models.Post;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 10/20/14.
 */
public class FacebookUtil {

    public static final int FACEBOOK_PERMISSION_CODE = 12000;

    public final List<String> PERMISSIONS = Arrays.asList("publish_actions");
    private final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private final String TAG = "FACEBOOK_PUBLISH";
    public boolean pendingPublishReauthorization = false;
    private UiLifecycleHelper uiLifecycleHelper;
    private Activity activity;


    public FacebookUtil(Bundle savedInstanceState, Activity activity) {

        uiLifecycleHelper = new UiLifecycleHelper(activity, null);
        uiLifecycleHelper.onCreate(savedInstanceState);
        this.activity = activity;

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        uiLifecycleHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });
    }

    public void onResume() {

        uiLifecycleHelper.onResume();
    }

    public void onPause() {

        uiLifecycleHelper.onPause();
    }

    public void onDestroy() {
        uiLifecycleHelper.onDestroy();
    }


    public void publishStory(Post request, final Activity activity,int postId) {


        Bundle postParams = new Bundle();
        postParams.putString("name", "Chance");
        postParams.putString("caption", "De uma chance para essa rede de ajuda contagiar você! ");
        postParams.putString("description", request.Texto );
        postParams.putString("link", Helper.VIEW_POST_URL + postId);


        if(request.Midias !=null)
         {
            for (Midia media : request.Midias) {
                postParams.putString("picture", media.UrlThumb);
                break;
            }
        }
        else {
            postParams.putString("picture", "http://www.iterative.com.br/mobile/chance/large.png");
        }


          WebDialog feedDialog = (
                    new WebDialog.FeedDialogBuilder(activity,
                            activity.getResources().getString(R.string.app_id),
                            postParams))
                    .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                        @Override
                        public void onComplete(Bundle values,
                                               FacebookException error) {
                            if (error == null) {
                                // When the story is posted, echo the success
                                // and the post Id.
                                final String postId = values.getString("post_id");
                                if (postId != null) {
                                    Toast.makeText(activity,
                                            "Posted story, id: "+postId,
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    // User clicked the Cancel button
                                    Toast.makeText(activity.getApplicationContext(),
                                            "Publish cancelled",
                                            Toast.LENGTH_SHORT).show();
                                }
                            } else if (error instanceof FacebookOperationCanceledException) {
                                // User clicked the "x" button
                                Toast.makeText(activity.getApplicationContext(),
                                        "Publish cancelled",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // Generic, ex: network error
                                Toast.makeText(activity.getApplicationContext(),
                                        "Error posting story",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                    })
                    .build();


            feedDialog.show();



//        Request.Callback callback = new Request.Callback() {
//            public void onCompleted(Response response) {
//                JSONObject graphResponse = response
//                        .getGraphObject()
//                        .getInnerJSONObject();
//                String postId = null;
//                try {
//                    postId = graphResponse.getString("id");
//                } catch (JSONException e) {
//                    Log.i(TAG,
//                            "JSON error " + e.getMessage());
//                }
//                FacebookRequestError error = response.getError();
//                if (error != null) {
//                    Toast.makeText(activity
//                                    .getApplicationContext(),
//                            error.getErrorMessage(),
//                            Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(activity
//                                    .getApplicationContext(),
//                            postId,
//                            Toast.LENGTH_LONG).show();
//                }
//            }
//        };
//
//        Request requestWeb = new Request(session, "me/feed", postParams,
//                HttpMethod.POST, callback);
//
//        RequestAsyncTask task = new RequestAsyncTask(requestWeb);
//        task.execute();


    }


    public void publishStory(CriarPostRequest request, final Activity activity , WebDialog.OnCompleteListener listener,int postId) {





        Bundle postParams = new Bundle();
        postParams.putString("name", "Chance");
        postParams.putString("caption", "De uma chance para essa rede de ajuda contagiar você! ");
        postParams.putString("description", request.Texto);
        postParams.putString("link", Helper.VIEW_POST_URL + postId);

        if(request.Midias !=null){

            for (String media : request.Midias) {

                postParams.putString("picture", media);
                break;
            }
        }
        else {
            postParams.putString("picture", "http://www.iterative.com.br/mobile/chance/large.png");
        }
        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(activity,
                        activity.getResources().getString(R.string.app_id),
                        postParams))
                .setOnCompleteListener(listener)
                .build();
        feedDialog.show();



//        Request.Callback callback = new Request.Callback() {
//            public void onCompleted(Response response) {
//                JSONObject graphResponse = response
//                        .getGraphObject()
//                        .getInnerJSONObject();
//                String postId = null;
//                try {
//                    postId = graphResponse.getString("id");
//                } catch (JSONException e) {
//                    Log.i(TAG,
//                            "JSON error " + e.getMessage());
//                }
//                FacebookRequestError error = response.getError();
//                if (error != null) {
//                    Toast.makeText(activity
//                                    .getApplicationContext(),
//                            error.getErrorMessage(),
//                            Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(activity
//                                    .getApplicationContext(),
//                            postId,
//                            Toast.LENGTH_LONG).show();
//                }
//            }
//        };
//
//        Request requestWeb = new Request(session, "me/feed", postParams,
//                HttpMethod.POST, callback);
//
//        RequestAsyncTask task = new RequestAsyncTask(requestWeb);
//        task.execute();


    }

    public boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }


    public void onSaveInstanceState(Bundle outState) {

        outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
        uiLifecycleHelper.onSaveInstanceState(outState);
    }


    public void onCreateView(Bundle outState) {

        pendingPublishReauthorization =
                outState.getBoolean(PENDING_PUBLISH_KEY, false);
    }


    public void onSessionStateChange(Session session, SessionState state, Exception exception, CriarPostRequest request) {

        if (pendingPublishReauthorization &&
                state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
            pendingPublishReauthorization = false;
//            publishStory(session, request);
        }
    }
}
