package br.com.iterative.chance.listeners;

import java.io.File;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import br.com.iterative.chance.Helper;
import br.com.iterative.rede.chance.R;
import br.com.iterative.chance.models.PostarMidiaResponse;

public final class AlterarFotoListener implements OnClickListener {
	private final class AlterarFotoTask extends AsyncTask<String, Object, PostarMidiaResponse> {
		protected void onPostExecute(PostarMidiaResponse result) {

			if(result == null)
				return;

			ImageView foto = (ImageView) activity.findViewById(R.id.foto);

			Helper.downloadImageFromURL(foto, result.UrlThumb);
			
			foto.setTag(result.MidiaId);
		}

		protected PostarMidiaResponse doInBackground(String... params) {
			try {
				File file = new File(params[0]);

				String[] strings = params[0].split("/");

				String json = HttpRequest.post(Helper.URL_POST_MIDIA_POST_FILE)
                        .part("Token", Helper.getToken())
                        .part("file", strings[strings.length - 1], file)
                        .part("DeviceToken", Helper.getRegId())
                        .part("Plataforma", Helper.PLATAFORMA)
                        .body();

				Log.d("response", json);

				return new Gson().fromJson(json, PostarMidiaResponse.class);
			} catch (HttpRequest.HttpRequestException e) {
				e.printStackTrace();
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	private Activity activity;
	private String caminho;

	public AlterarFotoListener(Activity activity) {
		this.activity = activity;
	}

	public AlterarFotoListener(Activity activity, String caminho) {
		this.activity = activity;
		this.caminho = caminho;
	}

	public void enviar() {
		new AlterarFotoTask().execute(this.caminho);
	}

	public void onClick(View v) {
		Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT).putExtra(Intent.EXTRA_LOCAL_ONLY, true);
		photoPickerIntent.setType("image/* video/*");
		activity.startActivityForResult(photoPickerIntent, Helper.CHANGE_PHOTO);
	}
}