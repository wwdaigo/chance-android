package br.com.iterative.chance.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.iterative.rede.chance.R;

public class OfertaAjudaItemView extends LinearLayout {

	private ImageButton botao1;

	private ImageButton botaoVer;

	private TextView categoria;

	private TextView dataCriacao;

	private ImageView fotoPerfil;

	private TextView localizacao;

	private TextView textNome;

	public OfertaAjudaItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImageButton getBotao1() {
		return botao1;
	}

	public ImageButton getBotaoVer() {
		return botaoVer;
	}

	public TextView getCategoria() {
		return categoria;
	}

	public TextView getDataCriacao() {
		return dataCriacao;
	}

	public ImageView getFotoPerfil() {
		return fotoPerfil;
	}

	public TextView getLocalizacao() {
		return localizacao;
	}

	public TextView getTextNome() {
		return textNome;
	}

	@Override
	protected void onFinishInflate() {
		fotoPerfil = (ImageView) findViewById(R.id.foto_perfil);
		textNome = (TextView) findViewById(R.id.text_nome);
		categoria = (TextView) findViewById(R.id.categoria);
		dataCriacao = (TextView) findViewById(R.id.data_criacao);
		localizacao = (TextView) findViewById(R.id.localizacao);
		botao1 = (ImageButton) findViewById(R.id.botao1);
		botaoVer = (ImageButton) findViewById(R.id.botao_ver);
	}
}
