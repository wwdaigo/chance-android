package br.com.iterative.chance.fragments;

import android.os.AsyncTask;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.adapters.PedidoAjudaAdapter;
import br.com.iterative.chance.models.PedidosPorCategoriaRequest;
import br.com.iterative.chance.models.PedidosPorCategoriaResponse;

public final class ListarPedidosPorCategoriaTask extends AsyncTask<PedidosPorCategoriaRequest, Object, PedidosPorCategoriaResponse> {
    private ListaPedidosAjudaFragment fragment;

    public ListarPedidosPorCategoriaTask(ListaPedidosAjudaFragment listaPedidosAjudaFragment) {
        this.fragment = listaPedidosAjudaFragment;
    }

    @Override
    protected void onPostExecute(PedidosPorCategoriaResponse result) {
        super.onPostExecute(result);

        if (result.Sucesso) {
            fragment.categoria.setText(result.Categoria.Nome);
            Helper.downloadImageFromFoto(fragment.foto, result.Categoria.Foto);

            fragment.lista.setAdapter(new PedidoAjudaAdapter(this.fragment, result.Pedidos));
        }
    }

    @Override
    protected PedidosPorCategoriaResponse doInBackground(PedidosPorCategoriaRequest... params) {
        return Helper.post(Helper.BASE_URL + "/ajuda/pedidosporcategoria", params[0], PedidosPorCategoriaResponse.class);
    }
}