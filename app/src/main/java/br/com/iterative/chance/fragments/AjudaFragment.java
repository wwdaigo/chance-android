package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.ResumoAjudaAdapter;
import br.com.iterative.chance.models.ResumoAjudaRequest;
import br.com.iterative.chance.models.ResumoAjudaResponse;
import br.com.iterative.rede.chance.R;

public class AjudaFragment extends Fragment {

    public boolean recarregarTela = false;
    private ExpandableListView listaAjuda;
    private ProgressBar progresso;
    private ResumoAjudaTask resumoAjudaTask;
    private View screen;

    @Override
    public void onStart() {
        super.onStart();

        // "aqui_vai_um_texto"
        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.central_de_ajuda));

        listaAjuda = (ExpandableListView) getActivity().findViewById(R.id.listaAjuda);

        if (recarregarTela) {
            recarregarTela = false;

            //this.progresso.setVisibility(View.VISIBLE);
            this.listaAjuda.setVisibility(View.INVISIBLE);

            resumoAjudaTask = new ResumoAjudaTask();
            resumoAjudaTask.execute();
        }
    }

    @Override
    public void onDestroy() {
        if (resumoAjudaTask != null) {
            resumoAjudaTask.cancel(true);
        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        screen = inflater.inflate(R.layout.ajuda, container, false);
        progresso = (ProgressBar) screen.findViewById(R.id.progresso);
        listaAjuda = (ExpandableListView) screen.findViewById(R.id.listaAjuda);
        recarregarTela = true;
        return screen;
    }

    private final class ResumoAjudaTask extends AsyncTask<Object, Object, ResumoAjudaResponse> {

        protected ResumoAjudaResponse doInBackground(Object... params) {
            ResumoAjudaRequest request = new ResumoAjudaRequest(Helper.getToken());

            return Helper.post(Helper.URL_POST_AJUDA_RESUMO, request, ResumoAjudaResponse.class);
        }

        protected void onPostExecute(ResumoAjudaResponse result) {
            if (result.Sucesso) {

                ResumoAjudaAdapter adapter = new ResumoAjudaAdapter(AjudaFragment.this.getActivity(), result, AjudaFragment.this);
                AjudaFragment.this.listaAjuda.setAdapter(adapter);
                AjudaFragment.this.listaAjuda.setDividerHeight(0);
                AjudaFragment.this.listaAjuda.setGroupIndicator(null);
                AjudaFragment.this.listaAjuda.setClickable(true);
//				AjudaFragment.this.listaAjuda.expandGroup(0);
//				AjudaFragment.this.listaAjuda.expandGroup(1);
//				AjudaFragment.this.listaAjuda.expandGroup(2);
//				AjudaFragment.this.listaAjuda.expandGroup(3);
//				AjudaFragment.this.listaAjuda.expandGroup(4);
                AjudaFragment.this.progresso.setVisibility(View.INVISIBLE);
                AjudaFragment.this.listaAjuda.setVisibility(View.VISIBLE);
            } else {
                new AlertDialog.Builder(AjudaFragment.this.getActivity()).setMessage(result.Mensagem).setPositiveButton("OK", null).create().show();
            }
        }
    }
}
