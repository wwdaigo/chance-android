package br.com.iterative.chance.listeners;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import br.com.iterative.chance.fragments.ListaPedidosAjudaFragment;
import br.com.iterative.chance.fragments.PedirAjudaFragment;
import br.com.iterative.rede.chance.R;

public final class PedirAjudaListener implements OnClickListener {
	private final Fragment fragment;
	private int categoriaId;
	private int pedidoId;
	private String categoriaFoto;
	public PedirAjudaListener(Fragment fragment, int categoriaId, String categoriaFoto) {
		this.fragment = fragment;
		this.categoriaId = categoriaId;
		this.categoriaFoto = categoriaFoto;
	}

	public PedirAjudaListener(Fragment fragment, int categoriaId, String categoriaFoto, int pedidoId) {
		this(fragment, categoriaId, categoriaFoto);

		this.pedidoId = pedidoId;
	}

	@Override
	public void onClick(View v) {
		FragmentManager fragmentManager = this.fragment.getActivity().getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.addToBackStack(null);
		PedirAjudaFragment ajudaFragment = new PedirAjudaFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("categoriaId", categoriaId);
		bundle.putString("categoriaFoto", categoriaFoto);
		bundle.putInt("tipoAjuda", Integer.parseInt(v.getTag().toString()));
		if(pedidoId > 0)
			bundle.putInt("pedidoId", pedidoId);
		ajudaFragment.setArguments(bundle);
		transaction.replace(R.id.main_frame, ajudaFragment);
		transaction.commit();
	}
}