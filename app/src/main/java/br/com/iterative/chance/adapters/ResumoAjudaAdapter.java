package br.com.iterative.chance.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.AjudaFragment;
import br.com.iterative.chance.fragments.ListaPedidosAjudaFragment;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.fragments.SolicitacoesAjudaFragment;
import br.com.iterative.chance.models.Foto;
import br.com.iterative.chance.models.Oferta;
import br.com.iterative.chance.models.ResumoAjudaDataSet;
import br.com.iterative.chance.models.ResumoAjudaResponse;
import br.com.iterative.chance.tasks.AceitarOfertaTask;
import br.com.iterative.chance.tasks.ExcluirOfertaTask;
import br.com.iterative.chance.tasks.RecusarOfertaTask;
import br.com.iterative.chance.views.AjudaOferecidaItemView;
import br.com.iterative.chance.views.CategoriaAjudaItemView;
import br.com.iterative.chance.views.OfertaAjudaItemView;
import br.com.iterative.rede.chance.R;

public class ResumoAjudaAdapter extends BaseExpandableListAdapter {

	public Context context;
	public Fragment fragment;
	private ResumoAjudaDataSet dataSet;
	private int groupHeight;

	public ResumoAjudaAdapter(Context context, ResumoAjudaResponse dataSet, AjudaFragment fragment) {
		this.context = context;
		this.dataSet = new ResumoAjudaDataSet(dataSet.Categorias, 
				dataSet.MinhasOfertasPorPedido, 
				dataSet.OferecidasParaPedido,
				dataSet.MinhasOfertasPorOferecimento, 
				dataSet.AceitasPorOferta);
		this.fragment = fragment;
	}

    public ResumoAjudaAdapter(Context context, ResumoAjudaResponse dataSet, SolicitacoesAjudaFragment fragment) {
        this.context = context;
        this.dataSet = new ResumoAjudaDataSet(dataSet.Categorias,
                dataSet.MinhasOfertasPorPedido,
                dataSet.OferecidasParaPedido,
                dataSet.MinhasOfertasPorOferecimento,
                dataSet.AceitasPorOferta);
        this.fragment = fragment;
    }

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		switch (groupPosition) {
		case 0:
			return this.dataSet.MinhasOfertasPorPedido[childPosition];
		case 1:
			return this.dataSet.OferecidasParaPedido[childPosition];
		case 2:
			return this.dataSet.MinhasOfertasPorOferecimento[childPosition];
		case 3:
			return this.dataSet.AceitasPorOferta[childPosition];
		case 4:
			return this.dataSet.Categorias[childPosition];
		default:
			return null;
		}
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		switch (groupPosition) {
		case 0:
			return this.dataSet.MinhasOfertasPorPedido[childPosition].hashCode();
		case 1:
			return this.dataSet.OferecidasParaPedido[childPosition].hashCode();
		case 2:
			return this.dataSet.MinhasOfertasPorOferecimento[childPosition].hashCode();
		case 3:
			return this.dataSet.AceitasPorOferta[childPosition].hashCode();
		case 4:
			return this.dataSet.Categorias[childPosition].hashCode();
		default:
			return -1;
		}
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		switch (groupPosition) {
		case 0:
			return this.dataSet.MinhasOfertasPorPedido.length;
		case 1:
			return this.dataSet.OferecidasParaPedido.length;
		case 2:
			return this.dataSet.MinhasOfertasPorOferecimento.length;
		case 3:
			return this.dataSet.AceitasPorOferta.length;
		case 4:
			return this.dataSet.Categorias.length;
		default:
			return 0;
		}
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		switch (groupPosition) {
		case 1:
			AjudaOferecidaItemView ajudaOferecidaItemView = (AjudaOferecidaItemView) View.inflate(this.context, R.layout.ajuda_oferecida_item, null);
			return preencherAjudaOferecidaItemView(ajudaOferecidaItemView, childPosition);
		case 0:
			OfertaAjudaItemView ofertaAjudaItemView = (OfertaAjudaItemView) View.inflate(this.context, R.layout.oferta_ajuda_item, null);

			return preencherOfertaAjudaItemView(ofertaAjudaItemView, dataSet.MinhasOfertasPorPedido[childPosition]);
		case 3:
			AjudaOferecidaItemView ajudaAceitaItemView = (AjudaOferecidaItemView) View.inflate(this.context, R.layout.ajuda_oferecida_item, null);
			return preencherAjudaAceitaItemView(ajudaAceitaItemView, childPosition);
		case 2:
			OfertaAjudaItemView pedidoAjudaItemView = (OfertaAjudaItemView) View.inflate(this.context, R.layout.oferta_ajuda_item, null);

			return preencherOfertaAjudaItemView(pedidoAjudaItemView, dataSet.MinhasOfertasPorOferecimento[childPosition]);
		default:
			CategoriaAjudaItemView categoriaAjudaItemView = (CategoriaAjudaItemView) View.inflate(this.context, R.layout.categoria_ajuda_item, null);
			return preencherCategoriaAjudaItemView(categoriaAjudaItemView, childPosition);
		}

	}

	@Override
	public Object getGroup(int groupPosition) {
		switch (groupPosition) {
		case 0:
			return this.dataSet.MinhasOfertasPorPedido;
		case 1:
			return this.dataSet.OferecidasParaPedido;
		case 2:
            return this.dataSet.MinhasOfertasPorOferecimento;
		case 3:
            return this.dataSet.AceitasPorOferta;

		default:
			return null;
		}
	}

	@Override
	public int getGroupCount() {
		return 4;
	}

	@Override
	public long getGroupId(int groupPosition) {
		switch (groupPosition) {
		case 0:
			return this.dataSet.MinhasOfertasPorPedido.hashCode();
		case 1:
			return this.dataSet.OferecidasParaPedido.hashCode();
		case 2:
            return this.dataSet.MinhasOfertasPorOferecimento.hashCode();
		case 3:
            return this.dataSet.AceitasPorOferta.hashCode();

		default:
			return -1;
		}
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		View view;

		if (convertView != null) {
			view = convertView;
		} else {
			view = View.inflate(this.context, R.layout.ajuda_parent, null);
		}

		switch (groupPosition) {
		case 0:
			((TextView) view.findViewById(R.id.titulo)).setText(ChanceApplication.getContext().getResources().getString(R.string.contatos_quem_te_ajudar));
			break;
		case 1:
			((TextView) view.findViewById(R.id.titulo)).setText(ChanceApplication.getContext().getResources().getString(R.string.oferta_ajuda));
			break;
		case 2:
			((TextView) view.findViewById(R.id.titulo)).setText(ChanceApplication.getContext().getResources().getString(R.string.contatos_quem_ajudar));
			break;
		case 3:
			((TextView) view.findViewById(R.id.titulo)).setText(ChanceApplication.getContext().getResources().getString(R.string.pedido_de_ajuda));
			break;
		/*case 4:
			((TextView) view.findViewById(R.id.titulo)).setText(ChanceApplication.getContext().getResources().getString(R.string.onde_oferecer_onde_ajudar));
			break;*/
		default:
			break;
		}
		
//		if (view.getHeight() > 0) this.groupHeight = view.getHeight();
//
//		if (getChildrenCount(groupPosition) == 0 && isExpanded) {
//			view.setVisibility(View.GONE);
//			view.setHeight(0);
//		} else {
//			view.setVisibility(View.VISIBLE);
//			if (groupHeight > 0) view.setHeight(groupHeight);
//		}
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}



	private AjudaOferecidaItemView preencherAjudaOferecidaItemView(final AjudaOferecidaItemView ajudaOferecidaItemView, final int position) {

        try {
            ajudaOferecidaItemView.getCategoria().setText(dataSet.OferecidasParaPedido[position].PedidoDeAjuda.Categoria.Descricao);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
            Date convertedDate = new Date();
            try {
                convertedDate = dateFormat.parse(dataSet.OferecidasParaPedido[position].PedidoDeAjuda.DataCriacao + " +000");
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("convertedDate TY" + dateFormatOut.format(convertedDate));

            ajudaOferecidaItemView.getDataCriacao().setText(dateFormatOut.format(convertedDate));
            ajudaOferecidaItemView.getLocalizacao().setText(dataSet.OferecidasParaPedido[position].PedidoDeAjuda.Localizacao.Descricao);
            ajudaOferecidaItemView.getTextNome().setText(dataSet.OferecidasParaPedido[position].Benfeitor.Apelido);

            ajudaOferecidaItemView.getBotao1().setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    //Toast.makeText(ajudaOferecidaItemView.getContext(), "IMPLEMENTAR!", Toast.LENGTH_SHORT).show();
                    new RecusarOfertaTask(ResumoAjudaAdapter.this).execute(dataSet.OferecidasParaPedido[position]);
                }
            });

            ajudaOferecidaItemView.getBotao2().setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    //Toast.makeText(ajudaOferecidaItemView.getContext(), "IMPLEMENTAR!", Toast.LENGTH_SHORT).show();
                    new AceitarOfertaTask(ResumoAjudaAdapter.this).execute(dataSet.OferecidasParaPedido[position]);
                }
            });

            Helper.downloadImageFromFoto(ajudaOferecidaItemView.getFotoPerfil(), dataSet.OferecidasParaPedido[position].Benfeitor.Foto);

            final int i = position;
            ajudaOferecidaItemView.getFotoPerfil().setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    FragmentActivity activity = (FragmentActivity) v.getContext();
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.addToBackStack(null);

                    Fragment fragment = new PerfilFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(Helper.PERFIL_ID, dataSet.OferecidasParaPedido[i].Benfeitor.PerfilId);
                    fragment.setArguments(bundle);

                    transaction.replace(R.id.main_frame, fragment);
                    transaction.commit();
                }
            });
        }
        catch (Exception e){
            Toast.makeText(context, ((Activity)context).getResources().getString(R.string.falha_conectar_servidor), Toast.LENGTH_LONG).show();
        }
		
		return ajudaOferecidaItemView;
	}

    private AjudaOferecidaItemView preencherAjudaAceitaItemView(final AjudaOferecidaItemView ajudaOferecidaItemView, final int position) {

        ajudaOferecidaItemView.getCategoria().setText(dataSet.AceitasPorOferta[position].PedidoDeAjuda.Categoria.Descricao);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dataSet.AceitasPorOferta[position].PedidoDeAjuda.DataCriacao+" +000");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("convertedDate TY"+dateFormatOut.format(convertedDate));

        ajudaOferecidaItemView.getDataCriacao().setText(dateFormatOut.format(convertedDate));
        ajudaOferecidaItemView.getLocalizacao().setText(dataSet.AceitasPorOferta[position].PedidoDeAjuda.Localizacao.Descricao);
        ajudaOferecidaItemView.getTextNome().setText(dataSet.AceitasPorOferta[position].Benfeitor.Apelido);

        ajudaOferecidaItemView.getBotao1().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ajudaOferecidaItemView.getContext(), "IMPLEMENTAR!", Toast.LENGTH_SHORT).show();
                new RecusarOfertaTask(ResumoAjudaAdapter.this).execute(dataSet.AceitasPorOferta[position]);
            }
        });

        ajudaOferecidaItemView.getBotao2().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ajudaOferecidaItemView.getContext(), "IMPLEMENTAR!", Toast.LENGTH_SHORT).show();
                new AceitarOfertaTask(ResumoAjudaAdapter.this).execute(dataSet.AceitasPorOferta[position]);
            }
        });

        Helper.downloadImageFromFoto(ajudaOferecidaItemView.getFotoPerfil(), dataSet.AceitasPorOferta[position].Benfeitor.Foto);

        final int i = position;
        ajudaOferecidaItemView.getFotoPerfil().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentActivity activity = (FragmentActivity) v.getContext();
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack(null);

                Fragment fragment = new PerfilFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Helper.PERFIL_ID, dataSet.AceitasPorOferta[i].Benfeitor.PerfilId);
                fragment.setArguments(bundle);

                transaction.replace(R.id.main_frame, fragment);
                transaction.commit();
            }
        });

        return ajudaOferecidaItemView;
    }


	private CategoriaAjudaItemView preencherCategoriaAjudaItemView(final CategoriaAjudaItemView categoriaAjudaItemView, final int position) {

		categoriaAjudaItemView.getNome().setText(dataSet.Categorias[position].Nome);
		categoriaAjudaItemView.getDescricao().setText(dataSet.Categorias[position].Descricao);
		Helper.downloadImageFromFoto(categoriaAjudaItemView.getFoto(), dataSet.Categorias[position].Foto);

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentManager fragmentManager = ((FragmentActivity) ResumoAjudaAdapter.this.context).getSupportFragmentManager();
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				transaction.addToBackStack(null);
				transaction.replace(R.id.main_frame, new ListaPedidosAjudaFragment(dataSet.Categorias[position].CategoriaId, dataSet.Categorias[position].Foto.Url));
				transaction.commit();
			}
		};

		categoriaAjudaItemView.getFoto().setOnClickListener(onClickListener);
		categoriaAjudaItemView.getCell().setOnClickListener(onClickListener);

		return categoriaAjudaItemView;

	}

	private OfertaAjudaItemView preencherOfertaAjudaItemView(final OfertaAjudaItemView ofertaAjudaItemView, final Oferta oferta) {

		final Foto autorFoto = oferta.Autor.Foto;
		final String txtNome = oferta.Autor.Apelido;
		final String categoriaDescricao = oferta.Categoria.Nome;
		final String dataCriacao = oferta.DataCriacao;
		final String localizacaoDescricao = oferta.Localizacao.Descricao;
		final String texto = oferta.Texto;
		final String textoDeContato = oferta.TextoDeContato;

		Helper.downloadImageFromFoto(ofertaAjudaItemView.getFotoPerfil(), autorFoto);
		
		ofertaAjudaItemView.getFotoPerfil().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentActivity activity = (FragmentActivity) v.getContext();
				FragmentManager fragmentManager = activity.getSupportFragmentManager();
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				transaction.addToBackStack(null);

				Fragment fragment = new PerfilFragment();
				Bundle bundle = new Bundle();
				bundle.putInt(Helper.PERFIL_ID, oferta.Autor.PerfilId);
				fragment.setArguments(bundle);

				transaction.replace(R.id.main_frame, fragment);
				transaction.commit();
			}
		});
		
	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
	    Date convertedDate = new Date();
	    String outDate;
	    try {
	        convertedDate = dateFormat.parse(dataCriacao+" +000");
	        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	        outDate = dateFormatOut.format(convertedDate);
	    } 
	    catch (ParseException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        outDate = dataCriacao; 
	    }
		
		ofertaAjudaItemView.getTextNome().setText(txtNome);
		ofertaAjudaItemView.getCategoria().setText(categoriaDescricao);
		ofertaAjudaItemView.getDataCriacao().setText(outDate);
		ofertaAjudaItemView.getLocalizacao().setText(localizacaoDescricao);

		ofertaAjudaItemView.getBotao1().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(ofertaAjudaItemView.getContext(), "IMPLEMENTAR!", Toast.LENGTH_SHORT).show();
				new ExcluirOfertaTask(ResumoAjudaAdapter.this).execute(oferta);
			}
		});

		ofertaAjudaItemView.getBotaoVer().setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder builder = new Builder(ResumoAjudaAdapter.this.context);

				View view = View.inflate(ResumoAjudaAdapter.this.context, R.layout.oferta_ajuda_detalhe, null);
			   
			    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
			    Date convertedDate = new Date();
			    String outDate;
			    try {
			        convertedDate = dateFormat.parse(dataCriacao+" +000");
			        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			        outDate = dateFormatOut.format(convertedDate);
			    } 
			    catch (ParseException e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			        outDate = dataCriacao; 
			    }
			    
			    
				Helper.downloadImageFromFoto((ImageView) view.findViewById(R.id.foto), autorFoto);
				((TextView) view.findViewById(R.id.text_nome)).setText(txtNome);
				((TextView) view.findViewById(R.id.categoria)).setText(categoriaDescricao);
				((TextView) view.findViewById(R.id.data_criacao)).setText(outDate);
				((TextView) view.findViewById(R.id.localizacao)).setText(localizacaoDescricao);

				((TextView) view.findViewById(R.id.texto)).setText(texto);
				((TextView) view.findViewById(R.id.texto_contato)).setText(textoDeContato);

				builder.setTitle("Oferta de Ajuda").setPositiveButton("Fechar", null).setView(view).create().show();
			}
		});

		return ofertaAjudaItemView;
	}
}