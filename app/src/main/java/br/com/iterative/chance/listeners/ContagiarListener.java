package br.com.iterative.chance.listeners;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.adapters.FeedAdapter;
import br.com.iterative.chance.models.ContagiarRequest;
import br.com.iterative.chance.models.Post;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.chance.views.FeedItemView;
import br.com.iterative.rede.chance.R;

public class ContagiarListener implements OnClickListener {

	private final class ContagiarTask extends
			AsyncTask<Post, Object, ResponseBase> {

		private ProgressDialog progressDialog;
		private Context context;

		public ContagiarTask(Context context) {
			this.context = context;
		}

		protected ResponseBase doInBackground(Post... params) {
			ContagiarRequest request = new ContagiarRequest(Helper.getToken(),
					params[0].PostId);

			return Helper.post(Helper.URL_POST_POST_CONTAGIAR, request,
					ResponseBase.class);
		}
		
		protected void onPostExecute(ResponseBase result) {

			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			if (!result.Sucesso) {
				ContagiarListener.this.feedItemView.getBotaoContagiar()
						.setImageResource(R.drawable.icocontagiar);
				ContagiarListener.this.post.setContagiado(false);
				ContagiarListener.this.post.Contagios -= 1;

				ContagiarListener.this.feedItemView.getContagiados().setText(
						String.valueOf(ContagiarListener.this.post.Contagios));

				Toast.makeText(context, ChanceApplication.getContext().getResources().getString(R.string.erro_contagiar_post),
						Toast.LENGTH_LONG).show();
			} else {
				post.setContagiado(true);
			}
		}

		protected void onPreExecute() {
			progressDialog = Helper.showProgressDialog(context);
			ContagiarListener.this.feedItemView.getBotaoContagiar()
					.setImageResource(R.drawable.icocontagiarativo);
			ContagiarListener.this.post.setContagiado(true);
			ContagiarListener.this.post.Contagios += 1;

			ContagiarListener.this.feedItemView.getContagiados().setText(
					String.valueOf(ContagiarListener.this.post.Contagios));
		}
	}

	private Post post;
	private Context context;
	private FeedItemView feedItemView;
    private FeedAdapter adapter;

	public ContagiarListener(Context context, Post post,
			FeedItemView feedItemView) {
		this.post = post;
		this.context = context;
		this.feedItemView = feedItemView;
	}

    public ContagiarListener(Context context, Post post,
                             FeedItemView feedItemView, FeedAdapter adapter) {
        this.post = post;
        this.context = context;
        this.feedItemView = feedItemView;
        this.adapter = adapter;

    }

	@Override
	public void onClick(View v) {
		if (!this.post.isContagiado()) {
			new ContagiarTask(context).execute(post);
            this.feedItemView.getFeedAdapter().onContagiar();
		}
	}

}
