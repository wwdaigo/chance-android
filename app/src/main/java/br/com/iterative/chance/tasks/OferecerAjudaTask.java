package br.com.iterative.chance.tasks;

import java.text.MessageFormat;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.os.AsyncTask;
import android.text.Html;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.adapters.PedidoAjudaAdapter;
import br.com.iterative.chance.models.OferecerAjudaRequest;
import br.com.iterative.chance.models.Pedido;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.rede.chance.R;

public final class OferecerAjudaTask extends AsyncTask<Pedido, Object, ResponseBase> {

	private final PedidoAjudaAdapter pedidoAjudaAdapter;

	public OferecerAjudaTask(PedidoAjudaAdapter pedidoAjudaAdapter) {
		this.pedidoAjudaAdapter = pedidoAjudaAdapter;
	}

	private Pedido pedido;

	@Override
	protected void onPostExecute(ResponseBase result) {
		super.onPostExecute(result);

		if (result.Sucesso) {
			AlertDialog.Builder builder = new Builder(this.pedidoAjudaAdapter.fragment.getActivity());

			builder.setTitle(ChanceApplication.getContext().getResources().getString(R.string.oferta_ajuda_enviada))
					.setMessage(
							Html.fromHtml(MessageFormat.format(ChanceApplication.getContext().getResources().getString(R.string.aguarde_resposta_mais_info), pedido.Autor.Apelido)))
					.setPositiveButton("OK", null);

			builder.create().show();
		} else {
			Toast.makeText(pedidoAjudaAdapter.fragment.getActivity(), result.Mensagem, Toast.LENGTH_LONG).show();
		}

	}

	@Override
	protected ResponseBase doInBackground(Pedido... params) {
		this.pedido = params[0];

		OferecerAjudaRequest request = new OferecerAjudaRequest(params[0].PedidoId);

		return Helper.post(Helper.BASE_URL + "/ajuda/oferecer", request, ResponseBase.class);
	}
}