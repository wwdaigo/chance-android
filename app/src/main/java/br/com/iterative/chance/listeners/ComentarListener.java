package br.com.iterative.chance.listeners;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.ComentarioFragment;
import br.com.iterative.chance.fragments.FeedDetalheFragment;
import br.com.iterative.chance.models.Post;
import br.com.iterative.chance.views.FeedItemView;
import br.com.iterative.rede.chance.R;

public class ComentarListener implements OnClickListener {

	private Context context;
	private FeedItemView feedItemView;
	private Post post;
	private Activity activity;

	public ComentarListener(Activity activity,Context context, final Post post, final FeedItemView feedItemView) {
		this.activity = activity;
		this.post = post;
		this.context = context;
		this.feedItemView = feedItemView;
	}

	public void onClick(View dialog) {

		FragmentActivity activity = (FragmentActivity) context;

		FragmentManager fragmentManager = activity.getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.addToBackStack(null);

		Fragment fragment = new ComentarioFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(Helper.POST_ID, post.PostId);
		fragment.setArguments(bundle);
		Helper.setFeedItemViewAtual(feedItemView);

		transaction.replace(R.id.main_frame, fragment);
		transaction.commit();
/*
		Intent intent;
		intent = new Intent(activity, ComentarioActivity.class);
		intent.putExtra(Helper.POST_ID, post.PostId);
		activity.startActivity(intent);
		*/
	}
}
