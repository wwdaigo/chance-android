package br.com.iterative.chance.models;

public class OfertaRespostaRequest extends RequestBase {
	public int BenfeitorId;
	public int PedidoId;
	
	public OfertaRespostaRequest(int benfeitorId, int pedidoId) {
		this.BenfeitorId = benfeitorId;
		this.PedidoId = pedidoId;
	}
}