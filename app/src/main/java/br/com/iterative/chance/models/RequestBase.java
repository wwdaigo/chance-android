package br.com.iterative.chance.models;

import java.util.Locale;
import android.provider.Settings.Secure;
import br.com.iterative.chance.Helper;

public class RequestBase {
	public String Idioma;
	public int Plataforma;
	public String Versao;
	public String Token;
	public String DeviceToken;

	public RequestBase() {
		this.Idioma = Locale.getDefault().toString();
		this.Versao = "1.34";
		this.Plataforma = 2;
		this.Token = Helper.getToken();
		this.DeviceToken = "";
	}
}
