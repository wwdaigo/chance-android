package br.com.iterative.chance.adapters;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.listeners.ExcluirComentarioListener;
import br.com.iterative.chance.models.Comentario;
import br.com.iterative.chance.views.ComentarioView;
import br.com.iterative.rede.chance.R;

public class ComentarioAdapter  extends BaseAdapter {
	private Comentario[] comentarios;
	private Activity activity;

	public ComentarioAdapter(Activity activity, Comentario[] comentarios) {
		this.comentarios = comentarios;
		this.activity = activity;
	}

	public int getCount() {
		return this.comentarios.length;
	}

	public Object getItem(int position) {
		return this.comentarios[position];
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ComentarioView view;

		if (convertView != null) {
			view = (ComentarioView) convertView;
		} else {
			view = (ComentarioView) View.inflate(activity, R.layout.comentario, null);
		}
		view.comentarioId = comentarios[position].ComentarioId;
		view.comentarioAdapter = this;
		view.perfilId = comentarios[position].Autor.PerfilId;
		view.apelido.setText(comentarios[position].Autor.Apelido);
		view.texto.setText(comentarios[position].Texto);
		Helper.downloadImageFromFoto(view.foto, comentarios[position].Autor.Foto);
		if(comentarios[position].Autor.PerfilId == Helper.getPerfilConta().PerfilId){
			view.botaoExcluir.setVisibility(View.VISIBLE);
			view.botaoExcluir.setOnClickListener(new ExcluirComentarioListener(activity, comentarios[position], view));
		} else {
			view.botaoExcluir.setVisibility(View.GONE);
			view.botaoExcluir.setOnClickListener(null);
			
		}

		return view;
	}
	
	public void excluir(int comentarioId){
		  List<Comentario> result = new LinkedList<Comentario>();

		    for(Comentario item : comentarios)
		        if(item.ComentarioId != comentarioId)
		            result.add(item);

		    comentarios = result.toArray(new Comentario[]{});

		notifyDataSetChanged();
	}
}
