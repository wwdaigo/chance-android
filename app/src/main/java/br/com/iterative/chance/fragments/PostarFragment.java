package br.com.iterative.chance.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.WebDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import br.com.iterative.chance.FacebookUtil;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.models.CriarPostRequest;
import br.com.iterative.chance.models.CriarPostResponse;
import br.com.iterative.chance.models.Localizacao;
import br.com.iterative.chance.models.PostarMidiaResponse;
import br.com.iterative.chance.models.VerPerfilContaRequest;
import br.com.iterative.chance.models.VerPerfilContaResponse;
import br.com.iterative.rede.chance.R;

public class PostarFragment extends Fragment implements OnClickListener {

    public static AlertDialog alertMarcar;
    public static PostarFragment instance;
    private static HashMap<String, String> idXNamePessoasOuEmailsSelecionadas;
    private CriarPostRequest request = null;
    private List<String> listTempSelectedUriPaths;
    private List<String> mediaIds;
    private FacebookUtil facebookUtil;
    private CheckBox checkFacebook;
    private ImageButton botaoAdicionarAmigo;
    private ImageButton botaoAdicionarImagem;
    private ImageButton botaoLocalizacao;
    private ToggleButton botaoAjudei;
    private ToggleButton botaoFuiAjudado;
    private Button botaoPostar;
    private ImageView fotoPerfil;
    private ProgressDialog progressDialog;
    private LinearLayout layoutPessoasAdicionadas;
    private Localizacao localizacao;
    private LocationClient locationClient;
    private OnClickListener getLocation = new OnClickListener() {

        @Override
        public void onClick(View v) {

            if (PostarFragment.this.localizacao != null) {
                PostarFragment.this.localizacao = null;
                PostarFragment.this.botaoLocalizacao
                        .setImageResource(R.drawable.btnlocalizacao);
                return;
            }

            progressDialog = Helper.showProgressDialog(getActivity());

            int available = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity());

            if (available != ConnectionResult.SUCCESS) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.erro_localizacao),
                        Toast.LENGTH_LONG).show();
            } else {
                try {
                    locationClient = new LocationClient(getActivity(),
                            new ConnectionCallbacks() {

                                @Override
                                public void onConnected(Bundle arg0) {
                                    try {
                                        Location lastLocation = locationClient
                                                .getLastLocation();

                                        Geocoder geo = new Geocoder(
                                                getActivity()
                                                        .getApplicationContext(),
                                                Locale.getDefault());

                                        List<Address> addresses = geo.getFromLocation(
                                                lastLocation.getLatitude(),
                                                lastLocation.getLongitude(), 1);
                                        if (addresses.isEmpty()) {
                                            // "Waiting for Location"
                                        } else {
                                            if (addresses.size() > 0) {

                                                PostarFragment.this.localizacao = new Localizacao();

                                                PostarFragment.this.localizacao.Latitude = (float) lastLocation
                                                        .getLatitude();
                                                PostarFragment.this.localizacao.Longitude = (float) lastLocation
                                                        .getLongitude();
                                                PostarFragment.this.localizacao.Descricao = String
                                                        .format("%s, %s",
                                                                addresses
                                                                        .get(0)
                                                                        .getSubAdminArea(),
                                                                addresses
                                                                        .get(0)
                                                                        .getAdminArea());

                                                if (progressDialog != null
                                                        && progressDialog
                                                        .isShowing()) {
                                                    progressDialog.dismiss();
                                                }

                                                PostarFragment.this.botaoLocalizacao
                                                        .setImageResource(R.drawable.btnlocalizacaoativo);

                                                // "aqui_vai_um_texto"
                                                String mensagem = getResources().getString(R.string.seu_endereco_definido_para) + " "
                                                        + localizacao.Descricao;

                                                Toast.makeText(getActivity(),
                                                        mensagem,
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }
                                        }
                                    } catch (Exception e) {
                                        if (progressDialog != null
                                                && progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        Toast.makeText(getActivity(), getResources().getString(R.string.erro_localizacao), Toast.LENGTH_LONG).show();

                                        e.printStackTrace();
                                    }

                                }

                                @Override
                                public void onDisconnected() {
                                    if (progressDialog != null
                                            && progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                }
                            }, new OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(
                                ConnectionResult arg0) {
                            if (progressDialog != null
                                    && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    });

                    locationClient.connect();
                } catch (Exception e) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    e.printStackTrace();
                }
            }

        }
    };
    private List<String> pessoas;
    private LinearLayout screen;
    private MultiAutoCompleteTextView textPost;
    private Dialog dialogImagem;
    private ObterPerfilTask obterPerfilTask;
    private int tipo;

    public static HashMap<String, String> getIdXNamePessoasOuEmailSelecionados() {
        if (idXNamePessoasOuEmailsSelecionadas == null) {
            idXNamePessoasOuEmailsSelecionadas = new HashMap<String, String>();
        }
        return idXNamePessoasOuEmailsSelecionadas;
    }

    public static void saveSelectPessoaOuEmailTemp(Context context,
                                                   String pessoaIdOuEmail, String pessoaApelidoOuEmail) {
        try {
            if (!getIdXNamePessoasOuEmailSelecionados()
                    .containsKey(pessoaIdOuEmail)) {
                getIdXNamePessoasOuEmailSelecionados().put(pessoaIdOuEmail,
                        pessoaApelidoOuEmail);
                Toast.makeText(context,
                        pessoaApelidoOuEmail + " " + context.getResources().getString(R.string.adicionado_sucesso_exclamacao),
                        Toast.LENGTH_SHORT).show();

                alertMarcar.dismiss();


            } else {
                Toast.makeText(context,
                        pessoaApelidoOuEmail + " " + context.getResources().getString(R.string.ja_foi_adicionado),
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String file, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private void sendImageToServer(String uriPath) {
        File file = new File(uriPath);

        //String[] strings = uriPath.split("/");
        AsyncHttpResponseHandler httpResponseHandler = createHTTPResponseHandler(file.length());

        RequestParams params = new RequestParams();

        String contentType = null;
        try {
            ContentResolver cr = getActivity().getContentResolver();
            contentType = cr.getType(Uri.fromFile(file));
            String extension = MimeTypeMap.getFileExtensionFromUrl(uriPath);
            if (extension != null) {
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                contentType = mime.getMimeTypeFromExtension(extension);


                if(contentType.contains("image")){
                    tipo = 1;
                }
                else {
                    tipo = 2;

//                    params.put("Content-Type",contentType);
//                    params.put("Content-Disposition","form-data; name=\"data\"; filename="+file.getName());
                }

            }


//            "Content-Type" -> "video/quicktime"

            //"Content-Disposition" -> "form-data; name="data"; filename="capturedvideo.MOV""



            params.put("Idioma", Locale.getDefault().toString());
            params.put("Token", Helper.getToken());
            params.put("file", file, contentType);
            params.put("DeviceToken", Helper.getRegId());
            params.put("Plataforma", Helper.PLATAFORMA);
            params.put("Tipo",tipo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(180 * 1000);
        client.post(Helper.URL_POST_MIDIA_POST_FILE, params, httpResponseHandler);
/*
        String json = HttpRequest.post(Helper.URL_POST_MIDIA_POST_FILE)
				.part("Token", Helper.getToken())
				.part("file", strings[strings.length - 1], file).body();

		Log.d("response", json);
*/
    }

    public AsyncHttpResponseHandler createHTTPResponseHandler(long length) {

        AsyncHttpResponseHandler handler = new ResponseHandler(length);

        return handler;
    }

    public List<String> getListTempSelectedUriPaths() {
        if (listTempSelectedUriPaths == null) {
            listTempSelectedUriPaths = new ArrayList<String>();
        }
        return listTempSelectedUriPaths;
    }

    public List<String> getMediaIds() {
        if (mediaIds == null) {
            mediaIds = new ArrayList<String>();
        }
        return mediaIds;
    }

    public void saveTempFileRealPath(String fileRealPath) {

        if (!getListTempSelectedUriPaths().contains(fileRealPath)) {
            sendImageToServer(fileRealPath);
        }
    }

    private void adicionaPessoasEmailsSelecionadas() {
        layoutPessoasAdicionadas.removeAllViews();
        for (String pessoaOuEmail : getIdXNamePessoasOuEmailSelecionados()
                .values()) {
            TextView textView = new TextView(getActivity());
            textView.setText(pessoaOuEmail + ";");
            layoutPessoasAdicionadas.addView(textView);
        }
        if (getIdXNamePessoasOuEmailSelecionados().size() > 0) {
            botaoAdicionarAmigo.setImageResource(R.drawable.btnaddamigoativo);
        } else {
            botaoAdicionarAmigo.setImageResource(R.drawable.btnaddamigo);
        }
    }

    private void configuraBotaoAdicionarAmigo(final Activity context) {
        botaoAdicionarAmigo = (ImageButton) context
                .findViewById(R.id.botao_adicionar_amigo);

        botaoAdicionarAmigo.setOnClickListener(new OnClickListener() {

            private View viewAdicionarAmigos;

            public void onClick(View v) {

                viewAdicionarAmigos = View.inflate(v.getContext(),
                        R.layout.forma_de_marcar_amigo, null);

                AlertDialog.Builder builder = new Builder(v.getContext());

                PostarFragment.getIdXNamePessoasOuEmailSelecionados().keySet()
                        .addAll(pessoas);

                final AlertDialog dialog = builder.setView(viewAdicionarAmigos)
                        .setNegativeButton("Cancelar", null).create();

                viewAdicionarAmigos.findViewById(R.id.botaoEscolheAmigo)
                        .setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                Log.d("click", v.toString());
                                dialog.dismiss();

                                PostarFragment f = (PostarFragment) ((FragmentActivity) context)
                                        .getSupportFragmentManager()
                                        .findFragmentByTag("PostarFragment");
                                f.showDialogMarcarBeneficiados();

                            }
                        });

                viewAdicionarAmigos.findViewById(R.id.botaoDigitaEmail)
                        .setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                Log.d("click", v.toString());
                                dialog.dismiss();

                                PostarFragment f = (PostarFragment) ((FragmentActivity) context)
                                        .getSupportFragmentManager()
                                        .findFragmentByTag("PostarFragment");
                                f.showDialogMarcarEmail();

                            }
                        });

                dialog.show();
            }
        });

    }

    @SuppressLint("InlinedApi")
    private void configuraBotaoAdicionarImagem(final Activity context) {
        botaoAdicionarImagem = (ImageButton) context.findViewById(R.id.botao_adicionar_imagem);

        botaoAdicionarImagem.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                createDialogImagens(PostarFragment.this.getActivity());
                dialogImagem.show();
            }
        });
    }

    private void createDialogImagens(Context context) {
        dialogImagem = new Dialog(context);
        dialogImagem.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogImagem.setContentView(R.layout.galeria);
        dialogImagem.findViewById(R.id.botaoAdicionar).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                        .putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                photoPickerIntent.setType("image/* video/*");
                startActivityForResult(photoPickerIntent, Helper.SELECT_PHOTO);
                //PostarFragment.this.botaoAdicionarImagem.setImageResource(R.drawable.btnaddimagemativo); // findme
            }
        });

        dialogImagem.findViewById(R.id.botaoFechar).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialogImagem.dismiss();
            }
        });

    }

    private void configuraBotaoAdicionarLocalizacao(final Activity context) {
        botaoLocalizacao = (ImageButton) context
                .findViewById(R.id.botao_localizacao);
        botaoLocalizacao.setOnClickListener(this.getLocation);
    }

    private void configuraBotaoPostar(final Activity context) {
        botaoPostar = (Button) context.findViewById(R.id.botao_postar);
        botaoPostar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


//                Session session = Session.getActiveSession();
//
//                if (session != null) {
//
//                    // Check for publish permissions
//                    List<String> permissions = session.getPermissions();
//                    if (!facebookUtil.isSubsetOf(facebookUtil.PERMISSIONS, permissions)) {
//                        facebookUtil.pendingPublishReauthorization = true;
//                        Session.NewPermissionsRequest newPermissionsRequest = new Session
//                                .NewPermissionsRequest(getActivity(), facebookUtil.PERMISSIONS);
//
//                        newPermissionsRequest.setRequestCode(FacebookUtil.FACEBOOK_PERMISSION_CODE);
//
//                        session.requestNewPublishPermissions(newPermissionsRequest);
//                        return;
//                    }
//
//                }

                new CriarPostTask(getActivity()).execute();


            }
        });
    }


    private void onSessionStateChange(Session session, SessionState state, Exception exception) {

        facebookUtil.onSessionStateChange(session, state, exception, request);
    }

    private void configurarImagemPerfil(final Activity context) {
        fotoPerfil = (ImageView) context.findViewById(R.id.foto_perfil);
        if (Helper.getPerfilConta() != null) {
            Helper.downloadImageFromURL(fotoPerfil,
                    Helper.getPerfilConta().URLFoto);
        } else {
            obterPerfilTask = new ObterPerfilTask();
            obterPerfilTask.execute();
        }
    }

    @Override
    public void onStop() {
        super.onStop();


        if (obterPerfilTask != null) {
            obterPerfilTask.cancel(true);
        }
    }

    private void configuraText(final Activity context) {
        textPost = (MultiAutoCompleteTextView) context
                .findViewById(R.id.text_post);

        textPost.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    // position the text type in the left top corner
                    textPost.setGravity(Gravity.LEFT | Gravity.TOP);
                } else {
                    // no text entered. Center the hint text.
                    textPost.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
                }
            }
        });
    }

    private void finalizaEnvioDePost(int postId) {

        if (checkFacebook.isChecked()) {

            facebookUtil.publishStory(request, getActivity(), new WebDialog.OnCompleteListener() {

                @Override
                public void onComplete(Bundle values,
                                       FacebookException error) {
                    if (error == null) {
                        // When the story is posted, echo the success
                        // and the post Id.
                        final String postId = values.getString("post_id");
                        if (postId != null) {

                        } else {
                            // User clicked the Cancel button
                            Toast.makeText(getActivity().getApplicationContext(),
                                    "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else if (error instanceof FacebookOperationCanceledException) {
                        // User clicked the "x" button
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Publish cancelled",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        // Generic, ex: network error
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Error posting story",
                                Toast.LENGTH_SHORT).show();
                    }

                    ((PrincipalActivity) getActivity()).navegarParaFeedDeAmigos();

                }


            },postId);

        }


    }

    private CriarPostRequest getCriarPostRequest() {

        String convertedPessoas[] = new String[pessoas.size()];
        pessoas.toArray(convertedPessoas);

        String convertedMidias[] = new String[getMediaIds().size()];
        getMediaIds().toArray(convertedMidias);

        int tipoAjuda = botaoAjudei.isChecked() ? 0 : 1;

        return new CriarPostRequest(Helper.getToken(),
                PostarFragment.this.textPost.getText().toString(),
                localizacao == null ? new Localizacao() : localizacao,
                convertedPessoas, convertedMidias, tipoAjuda);

    }

    private void inicializar() {

        final Activity context = getActivity();

        pessoas = new ArrayList<String>();

        configurarImagemPerfil(context);
        configuraBotaoAdicionarAmigo(context);
        configuraBotaoAdicionarImagem(context);
        configuraBotaoAdicionarLocalizacao(context);

        configuraText(context);
        configuraBotaoPostar(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        facebookUtil = new FacebookUtil(savedInstanceState, getActivity());

        screen = (LinearLayout) inflater.inflate(R.layout.postar, container,
                false);

        PostarFragment.getIdXNamePessoasOuEmailSelecionados().clear();
        getListTempSelectedUriPaths().clear();
        getMediaIds().clear();
        return screen;
    }

    @Override
    public void onStart() {
        super.onStart();

        instance = this;
        // "aqui_vai_um_texto"
        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.contagiar));

        layoutPessoasAdicionadas = (LinearLayout) screen
                .findViewById(R.id.layout_pessoas_adicionadas);

        FragmentActivity activity = getActivity();

        botaoAjudei = (ToggleButton) activity.findViewById(R.id.botaoAjudei);
        botaoFuiAjudado = (ToggleButton) activity.findViewById(R.id.botaoFuiAjudado);

        checkFacebook = (CheckBox) activity.findViewById(R.id.check_facebook);

        botaoAjudei.setOnClickListener(this);
        botaoFuiAjudado.setOnClickListener(this);
        onClick(botaoAjudei);

        inicializar();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        facebookUtil.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        facebookUtil.onResume();

        instance = this;
    }

    @Override
    public void onPause() {
        super.onPause();
        facebookUtil.onPause();
    }

    public void showDialogMarcarBeneficiados() {
        View viewMarcarBeneficiados = View.inflate(getActivity(),
                R.layout.marcar_benefeciados_fragment, null);

        AlertDialog.Builder builder = new Builder(getActivity());

        builder.setView(viewMarcarBeneficiados).setNegativeButton(getResources().getString(R.string.cancelar),
                null);

        alertMarcar = builder.create();
        alertMarcar.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                adicionaPessoasEmailsSelecionadas();
            }
        });
        alertMarcar.show();
    }

    public void showDialogMarcarEmail() {
        View viewMarcarBeneficiados = View.inflate(getActivity(),
                R.layout.marcar_email, null);

        final EditText email = (EditText) viewMarcarBeneficiados
                .findViewById(R.id.email);

        AlertDialog.Builder builder = new Builder(getActivity());

        builder.setView(viewMarcarBeneficiados)
                .setNegativeButton(getResources().getString(R.string.cancelar), null)
                .setPositiveButton(getResources().getString(R.string.marcar),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                String emailText = email.getText().toString();
                                if (Helper.isValidEmailAddress(emailText)) {
                                    PostarFragment
                                            .saveSelectPessoaOuEmailTemp(
                                                    getActivity(), emailText,
                                                    emailText);
                                    adicionaPessoasEmailsSelecionadas();
                                } else {
                                    new AlertDialog.Builder(getActivity())
                                            .setMessage(getResources().getString(R.string.email_invalido))
                                            .setPositiveButton("OK", null)
                                            .create().show();
                                }

                            }
                        }).create().show();
    }

    @Override
    public void onClick(View v) {
        if (v == botaoAjudei) {
            botaoFuiAjudado.setChecked(false);
            botaoAjudei.setChecked(true);
        } else if (v == botaoFuiAjudado) {
            botaoFuiAjudado.setChecked(true);
            botaoAjudei.setChecked(false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        facebookUtil.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        facebookUtil.onActivityResult(requestCode, resultCode, data);

    }

    private final class CriarPostTask extends
            AsyncTask<CriarPostRequest, Object, CriarPostResponse> {

        ProgressDialog progressDialog;
        private Context context;

        public CriarPostTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = Helper.showProgressDialog(context);
        }

        @Override
        protected CriarPostResponse doInBackground(CriarPostRequest... params) {

            request = PostarFragment.this.getCriarPostRequest();
/*
            for (int i = 0; i < request.Midias.length; i++) {
				sendImageToServer(request, i);
			}
*/
            request.Pessoas = new String[]{};

            request.Localizacao = PostarFragment.this.localizacao;

            request.Pessoas = PostarFragment
                    .getIdXNamePessoasOuEmailSelecionados().keySet()
                    .toArray(request.Pessoas);

            return Helper.post(Helper.URL_POST_POST_CRIAR, request,
                    CriarPostResponse.class);
        }


        protected void onPostExecute(CriarPostResponse result) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (result.Sucesso) {

                new AlertDialog.Builder(this.context)
                        .setMessage(getResources().getString(R.string.post_enviado_sucesso))
                        .setPositiveButton("OK", null).create().show();

                PostarFragment.this.finalizaEnvioDePost(result.PostId);
            } else {
                String msg = result.Mensagem;
                if (msg.equals("The Texto field is required."))
                    msg = getResources().getString(R.string.digite_algo_compartilhado_enviar);

                new AlertDialog.Builder(this.context)
                        .setMessage(msg)
                        .setPositiveButton("OK", null).create().show();
            }
        }


    }

    private class ResponseHandler extends TextHttpResponseHandler {
        private long length;

        public ResponseHandler(long length) {
            super();
            this.length = length;
        }

        @Override
        public void onStart() {
            super.onStart();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getActivity().getResources().getString(R.string.uploading_media));

            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setProgress(0);
            progressDialog.setMax((int) length);
            progressDialog.show();
        }

        @Override
        public void onProgress(int position, int length) {
            super.onProgress(position, length);

            progressDialog.setProgress(position);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers,
                              String responseString) {

            PostarMidiaResponse response = new Gson().fromJson(responseString, PostarMidiaResponse.class);
            getMediaIds().add(String.valueOf(response.MidiaId));

// findme

            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getActivity().getResources().getDisplayMetrics());
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, getActivity().getResources().getDisplayMetrics());

            LayoutParams params = new LinearLayout.LayoutParams(width, height);

            params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getActivity().getResources().getDisplayMetrics());
            params.rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getActivity().getResources().getDisplayMetrics());
            params.topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getActivity().getResources().getDisplayMetrics());
            params.bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getActivity().getResources().getDisplayMetrics());

            ImageView imgMidia = new ImageView(getActivity());
            imgMidia.setLayoutParams(params);
            imgMidia.setBackgroundResource(R.drawable.molduraimagem);
            imgMidia.setScaleType(ScaleType.CENTER_CROP);
            imgMidia.setTag(response.Url);
            if (response.UrlThumb != null) {
                Helper.downloadImageFromURL(imgMidia, response.UrlThumb);
            }

            LinearLayout medias = ((LinearLayout) dialogImagem.findViewById(R.id.midias));


            medias.addView(imgMidia, medias.getChildCount() - 1, params);

            PostarFragment.this.botaoAdicionarImagem.setImageResource(R.drawable.btnaddimagemativo);
            progressDialog.dismiss();

        }

        @Override
        public void onFailure(int statusCode, Header[] headers,
                              String responseString, Throwable throwable) {
            progressDialog.dismiss();
            System.out.println("o erro foi " + responseString);
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_uploading_media), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            super.onFinish();
        }

    }

    private final class ObterPerfilTask extends
            AsyncTask<Object, Object, VerPerfilContaResponse> {

        protected VerPerfilContaResponse doInBackground(Object... params) {
            VerPerfilContaRequest request = new VerPerfilContaRequest(
                    Helper.getToken(), Helper.getPerfil());

            VerPerfilContaResponse response = Helper.post(
                    Helper.URL_POST_CONTA_VER_PERFIL, request,
                    VerPerfilContaResponse.class);

            Helper.setPerfilConta(response);

            return response;
        }

        @Override
        protected void onPostExecute(VerPerfilContaResponse result) {
            if (!isCancelled()) {
                Helper.downloadImageFromURL(PostarFragment.this.fotoPerfil,
                        result.URLFoto);
            }
        }
    }
}
