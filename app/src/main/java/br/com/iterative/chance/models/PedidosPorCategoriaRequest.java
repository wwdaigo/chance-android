package br.com.iterative.chance.models;

import java.util.List;

public class PedidosPorCategoriaRequest extends RequestBase {

    public PedidosPorCategoriaRequest() {

		/*Localizacao=new Localizacao();
		Localizacao.Latitude = (float) 0.736842734;
		Localizacao.Longitude = (float) 0.293746823;
		Localizacao.Descricao = "S�o Paulo - SP";*/
    }

    public PedidosPorCategoriaRequest(Integer id,List<Categoria>categorias) {
        CategoriaId = id;
		/*Localizacao=new Localizacao();
		Localizacao.Latitude = (float) 0.736842734;
		Localizacao.Longitude = (float) 0.293746823;
		Localizacao.Descricao = "S�o Paulo - SP";*/
        Categorias = categorias;
    }


    public PedidosPorCategoriaRequest(int id) {
		CategoriaId = id;
		/*Localizacao=new Localizacao();
		Localizacao.Latitude = (float) 0.736842734;
		Localizacao.Longitude = (float) 0.293746823;
		Localizacao.Descricao = "S�o Paulo - SP";*/
	}
	
	public PedidosPorCategoriaRequest(int id, int raio, Localizacao localizacao) {
		CategoriaId = id;
		Localizacao=localizacao;
		Raio = raio;
	}

    public PedidosPorCategoriaRequest(Integer id, int raio, Localizacao localizacao,List<Categoria>categorias) {
        CategoriaId = id;
        Localizacao=localizacao;
        Raio = raio;
        Categorias = categorias;
    }

	public Integer CategoriaId;
	public Localizacao Localizacao;
	public int Raio;
    public List<Categoria> Categorias;
}
