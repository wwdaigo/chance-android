package br.com.iterative.chance.activities;

import com.google.gson.Gson;

import br.com.iterative.chance.adapters.FullScreenImageAdapter;
import br.com.iterative.chance.models.Midia;
import br.com.iterative.rede.chance.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

public class FullScreenViewActivity extends Activity{

	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);
		
		Intent intent = getIntent();
		String _midias = intent.getStringExtra("br.com.iterative.chance.MIDIAS");
		String _posicao = intent.getStringExtra("br.com.iterative.chance.POSICAO");
		String _asset = intent.getStringExtra("br.com.iterative.chance.ASSET");
		
		viewPager = (ViewPager) findViewById(R.id.pager);

		Gson gson = new Gson();
		
		int posicao = Integer.parseInt(_posicao);
		Midia[] midias = gson.fromJson(_midias, Midia[].class);

		if (_asset == null) _asset = "";
		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, midias, _asset.equals("true"));

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(posicao);
	}
}
