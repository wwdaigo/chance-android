package br.com.iterative.chance.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.Session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.FacebookUtil;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.fragments.FeedDetalheFragment;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.listeners.ComentarListener;
import br.com.iterative.chance.listeners.ContagiarListener;
import br.com.iterative.chance.listeners.DenunciarListener;
import br.com.iterative.chance.listeners.ExcluirPostListener;
import br.com.iterative.chance.listeners.QuemContagiouListener;
import br.com.iterative.chance.listeners.VerImagemListener;
import br.com.iterative.chance.models.Autor;
import br.com.iterative.chance.models.Contagiou;
import br.com.iterative.chance.models.CriarPostRequest;
import br.com.iterative.chance.models.Midia;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.chance.models.Post;
import br.com.iterative.chance.tasks.QuemContagiouTask;
import br.com.iterative.chance.views.FeedItemView;
import br.com.iterative.rede.chance.R;

public class FeedAdapter extends BaseAdapter  {

    private static Fragment fragment;
    private static FacebookUtil facebookUtil;
    private Post[] dataSet;

    public FeedAdapter(Fragment fragment, Post[] Posts) {
        this.fragment = fragment;
        this.dataSet = Posts;
    }

    public FeedAdapter(Fragment fragment, Post[] Posts, FacebookUtil facebookUtil) {
        this.fragment = fragment;
        this.dataSet = Posts;
        this.facebookUtil = facebookUtil;
    }


    private static void configuraAutor(final Post post, final FeedItemView feedItemView) {

        Helper.downloadImageFromFoto(feedItemView.getImagemPerfil(), post.Autor.Foto);

        feedItemView.getImagemPerfil().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentActivity activity = (FragmentActivity) v.getContext();
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack(null);

                Fragment fragment = new PerfilFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(Helper.PERFIL_ID, post.Autor.PerfilId);
                fragment.setArguments(bundle);

                transaction.replace(R.id.main_frame, fragment);
                transaction.commit();
            }
        });

        feedItemView.getApelido().setText(post.Autor.Apelido);


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
        Date convertedDate = new Date();
        String outDate;
        try {
            convertedDate = dateFormat.parse(post.DataCriacao + " +000");
            SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            outDate = dateFormatOut.format(convertedDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            outDate = post.DataCriacao;
        }

        feedItemView.getDataHora().setText(outDate);
        feedItemView.getLocal().setText(post.Localizacao.Descricao);
    }

    private static void configuraBeneficiados(final FeedItemView feedItemView, final Pessoa[] pessoas) {
        feedItemView.getBeneficiados().setOnClickListener(new OnClickListener() {

            private ListView lista;
            private View viewBenfeitores;

            @Override
            public void onClick(View v) {

                viewBenfeitores = View.inflate(v.getContext(), R.layout.lista_benfeitores, null);

                lista = (ListView) viewBenfeitores.findViewById(R.id.lista);

                ListAdapter adapter = new BenfeitoresAdapter(pessoas);

                lista.setAdapter(adapter);

                AlertDialog.Builder builder = new Builder(v.getContext());

                builder.setTitle(pessoas.length + (pessoas.length > 2 ? " " + ChanceApplication.getContext().getResources().getString(R.string.pessoas_ajudadas) : " " + ChanceApplication.getContext().getResources().getString(R.string.pessoa_ajudada))).setView(viewBenfeitores)
                        .setPositiveButton("Fechar", null).create().show();
            }
        });

        configuraImagenBeneficiarios(feedItemView, pessoas);
    }

    private static void configuraBotaoComentar(final Activity activity, final FeedItemView feedItemView, Post post, Context context) {
        feedItemView.getComentarios().setText(String.valueOf(post.Comentarios));
        feedItemView.getComentar().setOnClickListener(new ComentarListener(activity, context, post, feedItemView));
        feedItemView.getBotaoComentar().setOnClickListener(new ComentarListener(activity, context, post, feedItemView));
    }

    private static void configuraBotaoContagiar(final FeedItemView feedItemView, final Post post, final Context context) {
        // TODO: Colocar a R.drawable.icocontagiar_ativo se vier a informa��o do
        // server que esse post j� foi contagiado
        if (post.isContagiado()) {
            feedItemView.getBotaoContagiar().setImageResource(R.drawable.icocontagiarativo);
            feedItemView.getBotaoContagiar().setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((PrincipalActivity)context).setLoading(true);


                   new QuemContagiouTask(new QuemContagiouListener() {
                       @Override
                       public void quemContagiou(Contagiou[] pessoas) {

                           showDialogCategoria(pessoas);

                           ((PrincipalActivity)context).setLoading(false);

                       }
                   }).execute(post.PostId);



                }
            });
        } else {
            feedItemView.getBotaoContagiar().setImageResource(R.drawable.icocontagiar);
            feedItemView.getBotaoContagiar().setOnClickListener(new ContagiarListener(context, post, feedItemView));
            feedItemView.getContagiar().setOnClickListener(new ContagiarListener(context, post, feedItemView));
        }
        feedItemView.getContagiados().setText(String.valueOf(post.Contagios));
    }

    private static void configuraBotaoDenunciar(final FeedItemView feedItemView, Post post, Context context) {
        // TODO: Colocar a R.drawable.icodenunciar_ativo se vier a informa��o do
        // server que esse post j� foi contagiado
        if (post.Autor.PerfilId == Helper.getPerfilConta().PerfilId) {
            feedItemView.getBotaoExcluir().setVisibility(View.VISIBLE);
            feedItemView.getBotaoExcluir().setOnClickListener(new ExcluirPostListener(context, post, feedItemView));
            feedItemView.getBotaoDenunciar().setVisibility(View.GONE);

        } else {
            feedItemView.getBotaoExcluir().setVisibility(View.GONE);
            feedItemView.getBotaoExcluir().setOnClickListener(null);
            feedItemView.getBotaoDenunciar().setVisibility(View.VISIBLE);
            if (post.isDenunciado()) {
                feedItemView.getBotaoDenunciar().setImageResource(R.drawable.icodenunciarativo);
                feedItemView.getBotaoDenunciar().setOnClickListener(null);
            } else {
                feedItemView.getBotaoDenunciar().setImageResource(R.drawable.icodenunciar);
                feedItemView.getBotaoDenunciar().setOnClickListener(new DenunciarListener(context, post, feedItemView));
            }
        }

    }

    /**
     * As 4 imagens sempre v�o ocupar o espa�o na tela. Se vier s� 1,coloca no
     * slot superior esquerdo, se vier duas, coloca nos slots superiores, se
     * vier 3 coloca 2 nos superiores e 1 no inferior esquerdo, se vier 4,
     * preencher todas as imagens. Se vier 5 ou mais, preencho no modelo de tres
     * e coloco os tres pontos
     */
    private static void configuraImagenBeneficiarios(FeedItemView feedItemView, Pessoa[] pessoas) {

        feedItemView.getImgBeneficiado1().setVisibility(View.INVISIBLE);
        feedItemView.getImgBeneficiado2().setVisibility(View.INVISIBLE);
        feedItemView.getImgBeneficiado3().setVisibility(View.INVISIBLE);
        feedItemView.getImgBeneficiado4().setVisibility(View.INVISIBLE);

        // TODO: Melhorar essa l�gica e performance
        switch (pessoas.length) {
            case 0:
                break;
            case 1:
                feedItemView.getImgBeneficiado1().setVisibility(View.VISIBLE);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado1(), pessoas[0].Foto);
                break;
            case 2:
                feedItemView.getImgBeneficiado1().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado2().setVisibility(View.VISIBLE);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado1(), pessoas[0].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado2(), pessoas[1].Foto);
                break;
            case 3:
                feedItemView.getImgBeneficiado1().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado2().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado3().setVisibility(View.VISIBLE);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado1(), pessoas[0].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado2(), pessoas[1].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado3(), pessoas[2].Foto);
                break;
            case 4:
                feedItemView.getImgBeneficiado1().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado2().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado3().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado4().setVisibility(View.VISIBLE);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado1(), pessoas[0].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado2(), pessoas[1].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado3(), pessoas[2].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado4(), pessoas[3].Foto);
                break;

            default:
                feedItemView.getImgBeneficiado1().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado2().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado3().setVisibility(View.VISIBLE);
                feedItemView.getImgBeneficiado4().setVisibility(View.VISIBLE);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado1(), pessoas[0].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado2(), pessoas[1].Foto);
                Helper.downloadImageFromFoto(feedItemView.getImgBeneficiado3(), pessoas[2].Foto);
                feedItemView.getImgBeneficiado4().setImageResource(R.drawable.btntrespontos);

                break;
        }

    }

    private static void configuraMidia(FeedItemView feedItemView, Midia[] midias, Context context) {

        if (midias.length == 0) {
            feedItemView.getMidias().setVisibility(View.GONE);
            feedItemView.getMidia().setVisibility(View.GONE);
        } else if (midias.length == 1) {
            feedItemView.getMidia().setVisibility(View.VISIBLE);
            feedItemView.getMidias().setVisibility(View.GONE);
            feedItemView.getMidia().removeAllViews();
            for (int i = 0; i < midias.length; i++) {
                Midia midia = midias[i];

                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, context.getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 225, context.getResources().getDisplayMetrics());

                LayoutParams params = new LinearLayout.LayoutParams(width, height);

                params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                params.rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                params.topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                params.bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());

                ImageView imgMidia = new ImageView(context);
                imgMidia.setLayoutParams(params);
                imgMidia.setBackgroundResource(R.drawable.molduraimagem);
                imgMidia.setScaleType(ScaleType.CENTER_CROP);
                imgMidia.setTag(midia.Url);
                imgMidia.setOnClickListener(new VerImagemListener(midias, i));

                feedItemView.getMidia().addView(imgMidia, params);

                if (midia.UrlThumb != null) {
                    Helper.downloadImageFromURL(imgMidia, midia.UrlThumb);
                }
            }
        } else {
            feedItemView.getMidias().setVisibility(View.VISIBLE);
            feedItemView.getMidia().setVisibility(View.GONE);
            feedItemView.getMidias().removeAllViews();
            for (int i = 0; i < midias.length; i++) {
                Midia midia = midias[i];

                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, context.getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, context.getResources().getDisplayMetrics());

                LayoutParams params = new LinearLayout.LayoutParams(width, height);

                params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                params.rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                params.topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
                params.bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());

                ImageView imgMidia = new ImageView(context);
                imgMidia.setLayoutParams(params);
                imgMidia.setBackgroundResource(R.drawable.molduraimagem);
                imgMidia.setScaleType(ScaleType.CENTER_CROP);
                imgMidia.setTag(midia.Url);
                imgMidia.setOnClickListener(new VerImagemListener(midias, i));

                feedItemView.getMidias().addView(imgMidia, params);

                if (midia.UrlThumb != null) {
                    Helper.downloadImageFromURL(imgMidia, midia.UrlThumb);
                }
            }
        }

    }

    private static void configuraTexto(final FeedItemView feedItemView, final Post post, boolean detalhe) {

        try {
            if (detalhe) {
                feedItemView.getTexto().setText(post.Texto);
                feedItemView.getBotaoVerMais().setVisibility(View.GONE);
            } else {
                feedItemView.getTexto().setText(post.getTruncatedTexto());

                feedItemView.getBotaoVerMais().setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        FragmentActivity activity = (FragmentActivity) v.getContext();

                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.addToBackStack(null);

                        Fragment fragment = new FeedDetalheFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt(Helper.POST_ID, post.PostId);
                        fragment.setArguments(bundle);

                        transaction.replace(R.id.main_frame, fragment);
                        transaction.commit();
                    }
                });
            }
        }
        catch (Exception e){
            Toast.makeText(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.falha_conectar_servidor), Toast.LENGTH_LONG).show();
        }
    }

    public static void carregaFeedItemView(Activity activity, Context context, FeedItemView feedItemView, Post post, boolean detalhe) {

        configuraAutor(post, feedItemView);

        configuraBeneficiados(feedItemView, post.Pessoas);

        configuraTexto(feedItemView, post, detalhe);

        configuraMidia(feedItemView, post.Midias, context);

        configuraBotaoContagiar(feedItemView, post, context);

        configuraBotaoComentar(activity, feedItemView, post, context);

        configuraBotaoDenunciar(feedItemView, post, context);

        configuraBotaoFace(feedItemView, post, context);
    }

    private static void configuraBotaoFace(final FeedItemView feedItemView, final Post post, Context context) {

        if (!Helper.getLoggetWithFacebook()) {

            feedItemView.getButtonFacebook().setVisibility(View.GONE);
            return;
        }

        feedItemView.getButtonFacebook().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());


                builder.setTitle(R.string.compartilhar_face);
                builder.setMessage(R.string.compartilhar_face_msg);

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        CriarPostRequest request = new CriarPostRequest();


                        facebookUtil.publishStory(post, fragment.getActivity(),post.PostId);

                        dialogInterface.dismiss();
                    }
                });

                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });


                AlertDialog alertDialog = builder.create();

                alertDialog.show();

            }
        });

    }

    private static void showDialogCategoria(Contagiou[] pessoas) {

        final Dialog dialog = new Dialog(fragment.getActivity());
        dialog.setTitle("Quem contagiou este post.");
        dialog.setContentView(R.layout.dialog_categoria);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);


        ListView listView = (ListView) dialog.findViewById(R.id.list_categorias);

        Button buttonClose = (Button)dialog.findViewById(R.id.button_close);

        buttonClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



        QuemContagiouAdapter produtoAdapter = new QuemContagiouAdapter(fragment.getActivity(), pessoas, dialog);

        listView.setAdapter(produtoAdapter);


        try {
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public int getCount() {
        return this.dataSet.length;
    }

    @Override
    public Object getItem(int arg0) {
        return this.dataSet[arg0];
    }

    @Override
    public long getItemId(int position) {
        return this.dataSet[position].PostId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final FeedItemView feedItemView;
        Context context;

        if (fragment != null && parent == null) {
            context = fragment.getActivity();
        } else {
            context = parent.getContext();
        }


        if (context == null) {
            context = ChanceApplication.getContext();
        }

        if (convertView != null) {
            feedItemView = (FeedItemView) convertView;
        } else {
            feedItemView = (FeedItemView) View.inflate(context, R.layout.feed_item, null);
        }

        Post post = this.dataSet[position];
        feedItemView.setPostId(post.PostId);
        feedItemView.setFeedAdapter(FeedAdapter.this);

        carregaFeedItemView(fragment.getActivity(), context, feedItemView, post, false);

        return feedItemView;
    }

    public void excluir(int postId) {
        List<Post> result = new LinkedList<Post>();

        for (Post item : dataSet)
            if (item.PostId != postId)
                result.add(item);

        dataSet = result.toArray(new Post[]{});

        notifyDataSetChanged();
    }


   public void onContagiar(){

       notifyDataSetChanged();
   }


}
