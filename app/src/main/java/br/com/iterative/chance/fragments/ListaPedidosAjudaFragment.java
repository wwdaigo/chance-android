package br.com.iterative.chance.fragments;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.List;
import java.util.Locale;

import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.listeners.PedirAjudaListener;
import br.com.iterative.chance.models.Categoria;
import br.com.iterative.chance.models.Localizacao;
import br.com.iterative.chance.models.PedidosPorCategoriaRequest;
import br.com.iterative.rede.chance.R;

@SuppressLint("ValidFragment")
public class ListaPedidosAjudaFragment extends Fragment implements OnClickListener {

    private static final String TAG = "ListaPedidosAjudaFragment";

    public ListView lista;

    public ImageView foto;

    public TextView categoria;

    private ToggleButton botaoAmigos;

    private ToggleButton botaoProximidade;

    private LinearLayout proximidadeBusca;

    private SeekBar raio;

    private TextView raioNumero;

    private Button raioBusca;

    private int categoriaId;
    private String categoriaFoto;

    private Categoria[] categorias;

    public ListaPedidosAjudaFragment(Categoria[] categorias) {

        this.categorias = categorias;
    }

    public ListaPedidosAjudaFragment(int categoriaId, String categoriaFoto) {
        this.categoriaId = categoriaId;
        this.categoriaFoto = categoriaFoto;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        return inflater.inflate(R.layout.lista_pedido_ajuda, container, false);
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");

        super.onStart();

        FragmentActivity activity = getActivity();

        categoria = (TextView) activity.findViewById(R.id.categoria);
        foto = (ImageView) activity.findViewById(R.id.foto);
        lista = (ListView) activity.findViewById(R.id.lista);

        botaoAmigos = (ToggleButton) activity.findViewById(R.id.botaoAmigos);
        botaoProximidade = (ToggleButton) activity.findViewById(R.id.botaoProximidade);

        proximidadeBusca = (LinearLayout) activity.findViewById(R.id.proximidadeBusca);

        raioNumero = (TextView) activity.findViewById(R.id.raioNumero);
        raioBusca = (Button) activity.findViewById(R.id.raioBusca);

        raio = (SeekBar) activity.findViewById(R.id.raio);
        raio.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress + 1;
                raioNumero.setText(progress + " km");
            }
        });

        botaoAmigos.setOnClickListener(this);
        botaoProximidade.setOnClickListener(this);
        raioBusca.setOnClickListener(this);

        onClick(botaoAmigos);

        activity.findViewById(R.id.botaoPedirAjuda).setOnClickListener(new PedirAjudaListener(this, categoriaId, categoriaFoto));
        activity.findViewById(R.id.botaoOferecerAjuda).setOnClickListener(new PedirAjudaListener(this, categoriaId, categoriaFoto));
    }

    @Override
    public void onClick(View v) {
        if (v == botaoAmigos) {
            botaoProximidade.setChecked(false);
            botaoAmigos.setChecked(true);
            proximidadeBusca.setVisibility(View.GONE);

            new ListarPedidosPorCategoriaTask(this).execute(new PedidosPorCategoriaRequest(categoriaId));
        } else if (v == botaoProximidade) {
            botaoProximidade.setChecked(true);
            botaoAmigos.setChecked(false);
            proximidadeBusca.setVisibility(View.VISIBLE);

            new ListarPedidosPorCategoriaTask(this).execute(new PedidosPorCategoriaRequest(categoriaId, 1, null));
        } else if (v == raioBusca) {
            botaoProximidade.setChecked(true);
            botaoAmigos.setChecked(false);
            proximidadeBusca.setVisibility(View.VISIBLE);

            final ListaPedidosAjudaFragment fragment = this;

            try {

                if (getActivity() instanceof PrincipalActivity) {

                    Location lastLocation = ((PrincipalActivity) getActivity()).mLocationClient.getLastLocation();

                    if (lastLocation == null) {

                        Toast.makeText(getActivity(), getResources().getString(R.string.erro_localizacao), Toast.LENGTH_LONG).show();
                        return;
                    }

                    Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());  //geo = new Geocoder(getActivity(),Locale.getDefault());

                    if (!geo.isPresent()) {

                        Localizacao localizacao = new Localizacao();
                        localizacao = new Localizacao();
                        localizacao.Latitude = (float) lastLocation.getLatitude();
                        localizacao.Longitude = (float) lastLocation.getLongitude();
                        localizacao.Descricao = "";


                        int raio = Integer.parseInt(raioNumero.getText().toString().replace(" km", ""));

                        new ListarPedidosPorCategoriaTask(fragment).execute(new PedidosPorCategoriaRequest(categoriaId, raio, localizacao));


                    } else {

                        List<Address> addresses = geo.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1);

                        if (addresses.isEmpty()) {
                            new ListarPedidosPorCategoriaTask(fragment).execute(new PedidosPorCategoriaRequest(categoriaId));
                        } else {
                            if (addresses.size() > 0) {

                                Localizacao localizacao = new Localizacao();
                                localizacao = new Localizacao();
                                localizacao.Latitude = (float) lastLocation.getLatitude();
                                localizacao.Longitude = (float) lastLocation.getLongitude();
                                localizacao.Descricao = String.format("%s, %s",
                                        addresses.get(0).getSubAdminArea(),
                                        addresses.get(0).getAdminArea());

                                int raio = Integer.parseInt(raioNumero.getText().toString().replace(" km", ""));

                                new ListarPedidosPorCategoriaTask(fragment).execute(new PedidosPorCategoriaRequest(categoriaId, raio, localizacao));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), getResources().getString(R.string.erro_localizacao), Toast.LENGTH_LONG).show();

            }
        }


    }
}
