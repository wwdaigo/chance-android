package br.com.iterative.chance.models;

public class CriarContaRequest extends RequestBase {

	private String Nome;
	private String Sobrenome;
	private String Email;
	private String Senha;

	public CriarContaRequest(String nome, String sobrenome, String email,
			String senha) {
		this.Nome = nome;
		this.Sobrenome = sobrenome;
		this.Email = email;
		this.Senha = senha;
	}

}
