package br.com.iterative.chance.models;

import android.app.Activity;
import br.com.iterative.rede.chance.R;

public class EditarPerfilContaRequest extends RequestBase {

	private String Nome;
	private String Sobrenome;
	private String Email;
	private String DataNascimento;
	private String Sexo;
	private Boolean PerfilEmpresa;
	private String NomeEmpresa;
	private int MidiaId;

	public EditarPerfilContaRequest(Activity activity, String token, String nome, String sobrenome, String email, String nascimento, String sexo, Boolean PerfilEmpresa,
			String nomeEmpresa, int midiaId) {
		this.Token = token;
		this.Nome = nome;
		this.Sobrenome = sobrenome;
		this.Email = email;
		this.DataNascimento = nascimento;
		this.Sexo = sexo.equals(activity.getResources().getStringArray(R.array.sexo)[0]) ? "M" : "F";
		this.PerfilEmpresa = PerfilEmpresa;
		this.NomeEmpresa = nomeEmpresa;
		this.MidiaId = midiaId;
	}

}
