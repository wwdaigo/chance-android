package br.com.iterative.chance.tasks;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.adapters.ResumoAjudaAdapter;
import br.com.iterative.chance.fragments.SolicitacoesAjudaFragment;
import br.com.iterative.chance.models.Oferta;
import br.com.iterative.chance.models.OfertaRespostaRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.rede.chance.R;

public final class ExcluirOfertaTask extends AsyncTask<Oferta, Object, ResponseBase> {

	private final ResumoAjudaAdapter resumoAjudaAdapter;

	public ExcluirOfertaTask(ResumoAjudaAdapter resumoAjudaAdapter) {
		this.resumoAjudaAdapter = resumoAjudaAdapter;
	}
	
	private Oferta oferta;

	@Override
	protected void onPostExecute(ResponseBase result) {
		super.onPostExecute(result);

		if (result.Sucesso) {
			AlertDialog.Builder builder = new Builder(this.resumoAjudaAdapter.context);
			final ResumoAjudaAdapter adapter = this.resumoAjudaAdapter;
			builder.setTitle(ChanceApplication.getContext().getResources().getString(R.string.oferta_excluida_sucesso))
					/*.setMessage(
							Html.fromHtml(MessageFormat.format("Aguarde a resposta de {0} para mais informa��es.<br /><br />Obrigado!", pedido.Autor.Apelido)))*/
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface d, int i) {
                            ((SolicitacoesAjudaFragment)adapter.fragment).recarregarTela = true;
							adapter.fragment.onStart();
						}
					});

			builder.create().show();
		} else {
			Toast.makeText(resumoAjudaAdapter.context, result.Mensagem, Toast.LENGTH_LONG).show();
		}

	}

	@Override
	protected ResponseBase doInBackground(Oferta... params) {
		this.oferta = params[0];

		OfertaRespostaRequest request = new OfertaRespostaRequest(0, params[0].PedidoId);

		return Helper.post(Helper.BASE_URL + "/ajuda/Excluir", request, ResponseBase.class);
	}
}