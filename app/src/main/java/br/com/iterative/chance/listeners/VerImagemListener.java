package br.com.iterative.chance.listeners;

import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.activities.FullScreenViewActivity;
import br.com.iterative.chance.models.Midia;
import br.com.iterative.rede.chance.R;

import com.google.gson.Gson;

public final class VerImagemListener implements OnClickListener {
	
	private Midia[] midias;
	private int posicao;
	
	public VerImagemListener(Midia[] _midias, int _posicao) {
		midias = _midias;
		posicao = _posicao;
	}
	
	@Override
	public void onClick(View imagem) {
		
		String tag = imagem.getTag().toString();
		if (tag != null) {
			
			if (tag.endsWith(".mp4") || tag.endsWith(".mov")) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				
				intent.setDataAndType(Uri.parse(tag), "video/*");
				imagem.getContext().startActivity(intent);
			}else{
				//intent.setDataAndType(Uri.parse(tag), "image/*");
				Intent intent = new Intent(imagem.getContext(), FullScreenViewActivity.class);
				
				ArrayList<Midia> midias_ = new ArrayList<Midia>();
				for(Midia midia : midias) {
					if(!midia.Url.endsWith(".mp4") && !midia.Url.endsWith(".mov")){
						midias_.add(midia);
					}
				}
				
				Gson gson = new Gson();
				String _midias = gson.toJson(midias_.toArray(new Midia[]{}));
				intent.putExtra("br.com.iterative.chance.MIDIAS", _midias);
				String _posicao = gson.toJson(posicao);
				intent.putExtra("br.com.iterative.chance.POSICAO", _posicao);
				
				imagem.getContext().startActivity(intent);
			}
		} else {
			return;
		}

		try {
			//imagem.getContext().startActivity(intent);
		} catch (Exception e) {
			Toast.makeText(imagem.getContext(),
					ChanceApplication.getContext().getResources().getString(R.string.nenhum_visualizador),
					Toast.LENGTH_LONG).show();
		}
	}
}