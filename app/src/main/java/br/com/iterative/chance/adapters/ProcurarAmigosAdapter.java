package br.com.iterative.chance.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.fragments.ProcurarAmigoFragment;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.chance.tasks.SeguirPerfilTask;
import br.com.iterative.chance.tasks.SolicitarAmizadeTask;
import br.com.iterative.chance.views.ProcurarAmigoItemView;
import br.com.iterative.rede.chance.R;

public class ProcurarAmigosAdapter extends BaseAdapter implements OnItemClickListener {

	private Pessoa[] dataSet;
	private Pessoa[] dataSetAmigos;

	public ProcurarAmigosAdapter(Pessoa[] pessoas, Pessoa[] amigos) {
		this.dataSet = pessoas;
		this.dataSetAmigos = amigos;
		

	}

	private void configurarBotoes(
			final ProcurarAmigoItemView procurarAmigoItemView,
			final Pessoa pessoa, final Context context,
			boolean fromFriendList) {

		if (pessoa.Amizade || fromFriendList) {
			procurarAmigoItemView.getBotaoAdicionar().setVisibility(
					View.INVISIBLE);
		} else {
			procurarAmigoItemView.getBotaoAdicionar().setVisibility(
					View.VISIBLE);
			procurarAmigoItemView.getBotaoAdicionar().setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) 
						{
							procurarAmigoItemView.getBotaoSeguir().setVisibility(View.INVISIBLE);
							new SolicitarAmizadeTask(context, v,
									pessoa.PerfilId, pessoa).execute();

						}
					});
		}

		if (pessoa.Seguindo || fromFriendList) {
			procurarAmigoItemView.getBotaoSeguir()
					.setVisibility(View.INVISIBLE);
		} else {
			procurarAmigoItemView.getBotaoSeguir().setVisibility(View.VISIBLE);
			procurarAmigoItemView.getBotaoSeguir().setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new SeguirPerfilTask(context, v, pessoa.PerfilId,
									pessoa).execute();
						}
					});
		}

	}

	@Override
	public int getCount() 
	{
		int count = 0;
		if (this.dataSet != null) count = this.dataSet.length;
		return count + this.dataSetAmigos.length;
	}

	@Override
	public Object getItem(int position) 
	{
		if (this.dataSet==null) return this.dataSetAmigos[position];
		
		if (position < this.dataSet.length)
			return this.dataSet[position];
		else
			return this.dataSetAmigos[position-this.dataSet.length];
	}

	@Override
	public long getItemId(int position) 
	{
		if (this.dataSet==null) return this.dataSetAmigos[position].PerfilId;
		
		if (position < this.dataSet.length)
			return this.dataSet[position].PerfilId;
		else
			return this.dataSetAmigos[position-this.dataSet.length].PerfilId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		ListView lv = (ListView)parent;
		lv.setOnItemClickListener(this);
		
		final ProcurarAmigoItemView procurarAmigoItemView;
		boolean fromFriendList;
		
		Pessoa pess;
		
		if (this.dataSet == null)
		{
			pess = this.dataSetAmigos[position];
			fromFriendList = true;
		}
		else
		{
			if (position < this.dataSet.length)
			{
				pess = this.dataSet[position];
				fromFriendList = false;
			}
			else
			{
				pess = this.dataSetAmigos[position-this.dataSet.length];
				fromFriendList = true;
			}
		}
		
		final Context context = parent.getContext();
		final Pessoa pessoa = pess;
		
		
		if (convertView != null) {
			procurarAmigoItemView = (ProcurarAmigoItemView) convertView;
		} else {
			procurarAmigoItemView = (ProcurarAmigoItemView) View.inflate(
					context, R.layout.procurar_amigo_item, null);
		}

		if (!ProcurarAmigoFragment.imageViewLoaded.contains(procurarAmigoItemView.getFotoPerfil()))
		{
			ProcurarAmigoFragment.imageViewLoaded.add(procurarAmigoItemView.getFotoPerfil());
			
		}
		Helper.downloadImageFromFoto(procurarAmigoItemView.getFotoPerfil(),
				pessoa.Foto);
		
/*
		procurarAmigoItemView.getFotoPerfil().setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						FragmentActivity activity = (FragmentActivity) v
								.getContext();
						FragmentManager fragmentManager = activity
								.getSupportFragmentManager();
						FragmentTransaction transaction = fragmentManager
								.beginTransaction();
						transaction.addToBackStack(null);

						Fragment fragment = new PerfilFragment();
						Bundle bundle = new Bundle();
						bundle.putInt(Helper.PERFIL_ID, pessoa.PerfilId);
						fragment.setArguments(bundle);

						transaction.replace(
								R.id.main_frame,
								fragment,
								"fragment"
										+ fragmentManager
												.getBackStackEntryCount());
						transaction.commit();
					}
				});
*/
		procurarAmigoItemView.getNomePerfil().setText(pessoa.Apelido);
		procurarAmigoItemView.getPontuacaoPerfil().setText(ChanceApplication.getContext().getResources().getString(R.string.pontuacao_doubledots)+" "+pessoa.Pontuacao);
		procurarAmigoItemView.getNivelPerfil().setText(ChanceApplication.getContext().getResources().getString(R.string.nivel_doubledots)+" "+pessoa.Nivel);

		configurarBotoes(procurarAmigoItemView, pessoa, context, fromFriendList);

		return procurarAmigoItemView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long id) 
	{
		
		System.out.println("AAAAA");
		
		Pessoa pess;
		
		if (this.dataSet == null)
		{
			pess = this.dataSetAmigos[position];
		}
		else
		{
			if (position < this.dataSet.length)
			{
				pess = this.dataSet[position];
			}
			else
			{
				pess = this.dataSetAmigos[position-this.dataSet.length];
			}
		}
		
		final Pessoa pessoa = pess;
		
		{
			FragmentActivity activity = (FragmentActivity) v.getContext();
			FragmentManager fragmentManager = activity
					.getSupportFragmentManager();
			FragmentTransaction transaction = fragmentManager
					.beginTransaction();
			transaction.addToBackStack(null);

			Fragment fragment = new PerfilFragment();
			Bundle bundle = new Bundle();
			bundle.putInt(Helper.PERFIL_ID, pessoa.PerfilId);
			fragment.setArguments(bundle);

			transaction.replace(
					R.id.main_frame,
					fragment,
					"fragment"
							+ fragmentManager
									.getBackStackEntryCount());
			transaction.commit();
		}
		
	}

}
