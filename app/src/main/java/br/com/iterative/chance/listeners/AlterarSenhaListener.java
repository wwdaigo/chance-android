package br.com.iterative.chance.listeners;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.EditarPerfilFragment;
import br.com.iterative.chance.models.AlterarSenhaRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.rede.chance.R;

public final class AlterarSenhaListener implements OnClickListener {
	private final class ConfirmarListener implements DialogInterface.OnClickListener {
		private final class AlterarSenhaTask extends AsyncTask<AlterarSenhaRequest, Object, ResponseBase> {
			private final DialogInterface dialog;

			private AlterarSenhaTask(DialogInterface dialog) {
				this.dialog = dialog;
			}
			
			

			protected void onPostExecute(ResponseBase result) {
				if (result.Sucesso) {
					Toast.makeText(view.getContext(), ChanceApplication.getContext().getResources().getString(R.string.senha_alterada_sucesso), Toast.LENGTH_LONG).show();
					dialog.dismiss();
				}else{
					Toast.makeText(view.getContext(), result.Mensagem, Toast.LENGTH_LONG).show();
				}
			}

			protected ResponseBase doInBackground(AlterarSenhaRequest... params) 
			{
				System.out.println("senhaparams "+params[0]);
				return Helper.post(Helper.BASE_URL + "/Conta/AlterarSenha", params[0], ResponseBase.class);
			}
		}

		private View view;

		public ConfirmarListener(View view) {
			this.view = view;
		}

		public void onClick(final DialogInterface dialog, int which) 
		{
			EditText senha = (EditText) view.findViewById(R.id.senha);
			EditText novaSenha = (EditText) view.findViewById(R.id.novaSenha);
			EditText confirmarSenha = (EditText) view.findViewById(R.id.confirmarSenha);
			
			if (senha.getText().toString().trim().equals(""))
			{
				Toast.makeText(view.getContext(), ChanceApplication.getContext().getResources().getString(R.string.digite_senha_atual), Toast.LENGTH_LONG).show();
				senha.requestFocus();
				return;
			}
			else if (novaSenha.getText().toString().trim().equals(""))
			{
				Toast.makeText(view.getContext(), ChanceApplication.getContext().getResources().getString(R.string.digite_nova_senha), Toast.LENGTH_LONG).show();
				novaSenha.requestFocus();
				return;
			}
			else if (confirmarSenha.getText().toString().trim().equals(""))
			{
				Toast.makeText(view.getContext(), ChanceApplication.getContext().getResources().getString(R.string.digite_confirmacao_senha), Toast.LENGTH_LONG).show();
				confirmarSenha.requestFocus();
				return;
			}
			else if (!novaSenha.getText().toString().trim().equals(confirmarSenha.getText().toString().trim()))
			{
				Toast.makeText(view.getContext(), ChanceApplication.getContext().getResources().getString(R.string.senhas_nao_conferem), Toast.LENGTH_LONG).show();
				novaSenha.requestFocus();
				return;
			}

			AlterarSenhaRequest request = new AlterarSenhaRequest(senha, novaSenha);

			new AlterarSenhaTask(dialog).execute(request);
		}
	}

	private final EditarPerfilFragment editarPerfilFragment;

	public AlterarSenhaListener(EditarPerfilFragment editarPerfilFragment) {
		this.editarPerfilFragment = editarPerfilFragment;
	}

	public void onClick(View v) {
		AlertDialog.Builder builder = new Builder(v.getContext());

		View view = View.inflate(this.editarPerfilFragment.getActivity(), R.layout.alterar_senha, null);

		builder.setView(view).setTitle(ChanceApplication.getContext().getResources().getString(R.string.alteracao_senha)).setPositiveButton(ChanceApplication.getContext().getResources().getString(R.string.confirmar), new ConfirmarListener(view)).setNegativeButton(ChanceApplication.getContext().getResources().getString(R.string.cancelar), null);

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}