package br.com.iterative.chance.tasks;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.models.PedirAjudaRequest;
import br.com.iterative.chance.models.PedirAjudaResponse;
import br.com.iterative.rede.chance.R;

public final class PedirAjudaTask extends AsyncTask<PedirAjudaRequest, Object, PedirAjudaResponse> {

	private Fragment fragment;

	public PedirAjudaTask(Fragment fragment) {
		this.fragment = fragment;
	}

	@Override
	protected void onPostExecute(PedirAjudaResponse result) {
		super.onPostExecute(result);

		if (result.Sucesso) {
			if(request.PedidoId > 0){
				Toast.makeText(fragment.getActivity(), ChanceApplication.getContext().getResources().getString(R.string.sua_oferta_alterada_sucesso), Toast.LENGTH_LONG).show();
				PerfilFragment.recarregarTela = true;
				Helper.setPerfilConta(null);
				fragment.getFragmentManager().popBackStack();
			}
			else{
				Toast.makeText(fragment.getActivity(), ChanceApplication.getContext().getResources().getString(R.string.pedido_ajuda_enviado), Toast.LENGTH_LONG).show();
				((PrincipalActivity) fragment.getActivity()).navegarParaAjuda();
			}
		} else {
			if (result.Mensagem.equals("The Texto field is required."))
				Toast.makeText(fragment.getActivity(), ChanceApplication.getContext().getResources().getString(R.string.digite_algo_ante_publicar), Toast.LENGTH_LONG).show();
			else
				Toast.makeText(fragment.getActivity(), result.Mensagem, Toast.LENGTH_LONG).show();
		}
	}
	
	private PedirAjudaRequest request;

	@Override
	protected PedirAjudaResponse doInBackground(PedirAjudaRequest... params) {
		request = params[0];
		String url = Helper.BASE_URL + "/ajuda/pedir";
		if(request.PedidoId > 0)
			url = Helper.BASE_URL + "/ajuda/editar"; 
		return Helper.post(url, request, PedirAjudaResponse.class);
	}
}