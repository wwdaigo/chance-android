package br.com.iterative.chance.models;

import java.util.ArrayList;

public class Oferta {
	public int PedidoId;
	public int TipoAjuda;
	public Pessoa Autor;
	public Categoria Categoria;
	public String DataCriacao;
	public Localizacao Localizacao;
	public String Texto;
	public String TextoDeContato;
}
