package br.com.iterative.chance.listeners;

import android.content.DialogInterface;

import br.com.iterative.chance.models.Categoria;

/**
 * Created by pedrohenriqueborges on 10/28/14.
 */
    public interface CategoriaListener {

    void onSelectCategoria(Categoria categoria,DialogInterface dialogInterface);

    void onSelectCategoriaForRequest(Categoria categoria,DialogInterface dialogInterface);
}
