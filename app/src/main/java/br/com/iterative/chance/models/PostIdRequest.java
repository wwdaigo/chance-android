package br.com.iterative.chance.models;

public class PostIdRequest extends RequestBase {
	public int PostId;

	public PostIdRequest(String Token, int PostId) {
		this.Token = Token;
		this.PostId = PostId;
	}
}
