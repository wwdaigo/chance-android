package br.com.iterative.chance.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.Mask;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.listeners.AlterarFotoListener;
import br.com.iterative.chance.listeners.AlterarSenhaListener;
import br.com.iterative.chance.models.EditarPerfilContaRequest;
import br.com.iterative.chance.models.EditarPerfilContaResponse;
import br.com.iterative.chance.models.VerPerfilContaRequest;
import br.com.iterative.chance.models.VerPerfilContaResponse;
import br.com.iterative.rede.chance.R;

public class EditarPerfilFragment extends Fragment {

    private EditText nome;
    private EditText sobrenome;
    private EditText nascimento;
    private ToggleButton empresa;
    private EditText nomeEmpresa;
    private EditText email;
    private Spinner sexo;
    private View progresso;
    private View tela;
    private ImageView foto;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((PrincipalActivity) getActivity()).setTitleBarText("Editar Cadastro");

        return inflater.inflate(R.layout.editar_perfil, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progresso = getActivity().findViewById(R.id.progresso);
        tela = getActivity().findViewById(R.id.tela);

        nome = (EditText) getActivity().findViewById(R.id.nome);
        sobrenome = (EditText) getActivity().findViewById(R.id.sobrenome);
        nascimento = (EditText) getActivity().findViewById(R.id.nascimento);
        empresa = (ToggleButton) getActivity().findViewById(R.id.empresa);
        nomeEmpresa = (EditText) getActivity().findViewById(R.id.nomeEmpresa);
        email = (EditText) getActivity().findViewById(R.id.email);
        sexo = (Spinner) getActivity().findViewById(R.id.sexo);
        foto = (ImageView) getActivity().findViewById(R.id.foto);

        nascimento.addTextChangedListener(Mask.insert("##/##/####", nascimento));

        new ObterPerfilTask().execute(Helper.getPerfil());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.sexo, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sexo.setAdapter(adapter);

        getActivity().findViewById(R.id.botaoAlterarSenha).setOnClickListener(new AlterarSenhaListener(this));


        getActivity().findViewById(R.id.botaoSalvar).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (nome.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.necessario_nome), Toast.LENGTH_LONG).show();
                    nome.requestFocus();
                    return;
                } else if (sobrenome.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.necessario_sobrenome), Toast.LENGTH_LONG).show();
                    sobrenome.requestFocus();
                    return;
                } else if (nascimento.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.necessario_nascimento), Toast.LENGTH_LONG).show();
                    nascimento.requestFocus();
                    return;
                } else if (email.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.necessario_email), Toast.LENGTH_LONG).show();
                    email.requestFocus();
                    return;
                } else if (empresa.isChecked() && TextUtils.isEmpty(nomeEmpresa.getText())) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.necessario_empresa), Toast.LENGTH_LONG).show();
                    nomeEmpresa.requestFocus();
                    return;
                }

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(nome.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(sobrenome.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(nascimento.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(email.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(empresa.getWindowToken(), 0);

                new EditarPerfilContaTask().execute(Helper.getToken(), nome, sobrenome, email, nascimento, sexo, empresa, nomeEmpresa, foto.getTag());
            }
        });

        getActivity().findViewById(R.id.foto).setOnClickListener(new AlterarFotoListener(getActivity()));
    }

    private final class ObterPerfilTask extends AsyncTask<Object, Object, VerPerfilContaResponse> {

        protected void onPostExecute(VerPerfilContaResponse result) {

            if(result == null)
                return;

            nome.setText(result.Nome);
            sobrenome.setText(result.Sobrenome);

            if (result.Sexo != null) {
                if (result.Sexo.equals("M")) {
                    sexo.setSelection(0);
                } else {
                    sexo.setSelection(1);
                }
            }

            nascimento.setText(result.DataNascimento);
            email.setText(result.Email);
            if (result.Email.equals("")) email.setEnabled(true);

            nomeEmpresa.setText(result.NomeEmpresa);
            empresa.setChecked(result.PerfilEmpresa);

            empresa.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (empresa.isChecked()) {
                        nomeEmpresa.setVisibility(View.VISIBLE);
                    } else {
                        nomeEmpresa.setVisibility(View.GONE);
                        nomeEmpresa.setText("");
                    }
                }
            });

            if (result.PerfilEmpresa) {
                nomeEmpresa.setVisibility(View.VISIBLE);
            } else {
                nomeEmpresa.setVisibility(View.GONE);
                nomeEmpresa.setText("");
            }

            Helper.downloadImageFromURL(foto, result.URLFoto);

            tela.setVisibility(View.VISIBLE);
            progresso.setVisibility(View.GONE);
        }

        protected void onPreExecute() {
            tela.setVisibility(View.INVISIBLE);
            progresso.setVisibility(View.GONE);
        }

        protected VerPerfilContaResponse doInBackground(Object... params) {
            try {
                VerPerfilContaRequest request = new VerPerfilContaRequest(Helper.getToken(), Helper.getPerfil());

                return Helper.post(Helper.BASE_URL + "Conta/VerPerfil", request, VerPerfilContaResponse.class);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private final class EditarPerfilContaTask extends AsyncTask<Object, Object, EditarPerfilContaResponse> {

        protected void onPostExecute(EditarPerfilContaResponse result) {
            if (result.Sucesso) {
                Toast.makeText(getActivity(), getResources().getString(R.string.alteracao_realizada_sucesso), Toast.LENGTH_LONG).show();

                Helper.setPerfilConta(null);

                ((PrincipalActivity) getActivity()).navegarParaPerfil();
            }
        }

        protected EditarPerfilContaResponse doInBackground(Object... params) {
            int midiaId;

            try {
                midiaId = (Integer) params[8];
            } catch (Exception e) {
                midiaId = 0;
            }

            EditarPerfilContaRequest request = new EditarPerfilContaRequest(getActivity(), (String) params[0], ((EditText) params[1]).getText().toString(),
                    ((EditText) params[2]).getText().toString(), ((EditText) params[3]).getText().toString(), ((EditText) params[4]).getText().toString(),
                    ((Spinner) params[5]).getSelectedItem().toString(), ((ToggleButton) params[6]).isChecked(), ((EditText) params[7]).getText().toString(),
                    midiaId);

            return Helper.post(Helper.BASE_URL + "Conta/EditarPerfil", request, EditarPerfilContaResponse.class);
        }
    }
}
