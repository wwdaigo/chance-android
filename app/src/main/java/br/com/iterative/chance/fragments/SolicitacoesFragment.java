package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.models.AprovarAmizadeRequest;
import br.com.iterative.chance.models.ListaSolicitacoesRequest;
import br.com.iterative.chance.models.ListaSolicitacoesResponse;
import br.com.iterative.chance.models.ReprovarAmizadeRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.rede.chance.R;

public class SolicitacoesFragment extends Fragment {
    public static ArrayList<ImageView> imageViewLoaded;
    public static SolicitacoesFragment instance;
    private ListView solicitacoes;

    public static SolicitacoesFragment getInstance() {
        return instance;
    }

    public ListView getSolicitacoes() {
        return solicitacoes;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        instance = this;
        return inflater.inflate(R.layout.solicitacoes, container, false);

    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        SolicitacoesFragment.imageViewLoaded = new ArrayList<ImageView>();
        // "aqui_vai_um_texto"
        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.solicitacoes));
        solicitacoes = (ListView) getActivity().findViewById(R.id.solicitacoes);

        new ListarSolicitacoesTask().execute();
    }

    private final class ListarSolicitacoesTask extends
            AsyncTask<Object, Object, Object> {
        private ListaSolicitacoesResponse responseLista;
        private ListAdapter adapter;

        protected void onPreExecute() {
            Toast.makeText(getActivity(), getResources().getString(R.string.buscando_threedots), Toast.LENGTH_SHORT)
                    .show();
        }

        protected void onPostExecute(Object result) {
            adapter = new BaseAdapter() {

                public View getView(final int position, View convertView,
                                    ViewGroup parent) {

                    View view;

                    if (convertView != null) {
                        view = convertView;
                    } else {
                        view = View.inflate(getActivity(),
                                R.layout.solicitacao_item, null);
                    }

                    TextView apelido = (TextView) view.findViewById(R.id.apelido);
                    ImageView foto = (ImageView) view.findViewById(R.id.foto);

                    TextView pontuacao_perfil = (TextView) view.findViewById(R.id.pontuacao_perfil);
                    TextView nivel_perfil = (TextView) view.findViewById(R.id.nivel_perfil);

                    apelido.setText(responseLista.Pessoas[position].Apelido);

                    pontuacao_perfil.setText(getResources().getString(R.string.pontuacao_doubledots) + " " + responseLista.Pessoas[position].Pontuacao);
                    nivel_perfil.setText("Nível: " + responseLista.Pessoas[position].Nivel);


                    if (!imageViewLoaded.contains(foto)) {
                        System.out.println("teste foto request " + foto);
                        imageViewLoaded.add(foto);
                        Helper.downloadImageFromFoto(foto, responseLista.Pessoas[position].Foto);
                    }

                    view.findViewById(R.id.botaoAceitar).setOnClickListener(
                            new OnClickListener() {
                                public void onClick(View v) {
                                    new AceitarAmizadeTask(getActivity()).execute(responseLista.Pessoas[position].PerfilId);
                                }
                            });

                    view.findViewById(R.id.botaoRecusar).setOnClickListener(
                            new OnClickListener() {
                                public void onClick(View v) {
                                    new RecusarAmizadeTask(getActivity())
                                            .execute(responseLista.Pessoas[position].PerfilId);
                                }
                            });

                    return view;
                }

                public long getItemId(int position) {
                    return 0;
                }

                public Object getItem(int position) {
                    return null;
                }

                public int getCount() {
                    return responseLista.Pessoas.length;
                }
            };

            solicitacoes.setAdapter(adapter);
        }

        protected Object doInBackground(Object... params) {
            ListaSolicitacoesRequest request = new ListaSolicitacoesRequest();


            responseLista = Helper.post(Helper.BASE_URL + "/relacionamento/listarsolicitacoes", request, ListaSolicitacoesResponse.class);


            return null;
        }

        private final class RecusarAmizadeTask extends
                AsyncTask<Object, Object, Object> {
            private ResponseBase response;
            private ProgressDialog progressDialog;
            private Context context;

            public RecusarAmizadeTask(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute() {
                progressDialog = Helper.showProgressDialog(context);
                super.onPreExecute();
            }

            protected void onPostExecute(Object result) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                onStart();

                if (response.Sucesso) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.pedido_amizade_recusado), Toast.LENGTH_LONG).show();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SolicitacoesFragment.getInstance().solicitacoes.invalidate();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(response.Mensagem).create().show();
                }
            }

            protected Object doInBackground(Object... params) {

                ReprovarAmizadeRequest request = new ReprovarAmizadeRequest(
                        (Integer) params[0]);

                response = Helper.post(Helper.BASE_URL + "/relacionamento/reprovaramizade", request, ResponseBase.class);

                return params[0];
            }
        }

        private final class AceitarAmizadeTask extends
                AsyncTask<Object, Object, Object> {
            private ResponseBase response;
            private ProgressDialog progressDialog;
            private Context context;

            public AceitarAmizadeTask(Context context) {
                this.context = context;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = Helper.showProgressDialog(context);
            }

            protected void onPostExecute(Object result) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                onStart();

                if (response.Sucesso) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.pedido_amizade_aceito),
                            Toast.LENGTH_LONG).show();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SolicitacoesFragment.getInstance().solicitacoes.invalidate();

                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(response.Mensagem).create().show();
                }
            }

            protected Object doInBackground(Object... params) {

                AprovarAmizadeRequest request = new AprovarAmizadeRequest(
                        (Integer) params[0]);


                response = Helper.post(Helper.BASE_URL + "/relacionamento/aprovaramizade", request, ResponseBase.class);


                return null;
            }
        }
    }
}
