package br.com.iterative.chance.dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.GpsActivity;
import br.com.iterative.chance.adapters.CategoriaAdapter;
import br.com.iterative.chance.listeners.CategoriaListener;
import br.com.iterative.chance.listeners.FiltroDialogListener;
import br.com.iterative.chance.models.Categoria;
import br.com.iterative.chance.models.Filtro;
import br.com.iterative.chance.models.Localizacao;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 10/27/14.
 */
public class FiltroDialog extends DialogFragment implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, CategoriaListener {


    Activity activity;
    ToggleButton botaoAmigos;
    ToggleButton botaoProximidade;
    SeekBar raio;
    TextView raioNumero;
    View linearCategorias;
    TextView textCategoria;
    View proximidadeBusca;
    Button buttonFiltro;
    Categoria[] categorias;
    List<Integer> categoriaSelecionada;



    public void setCategorias(Categoria[] categorias) {
        this.categorias = categorias;
    }

    Filtro filtro;
    FiltroDialogListener listener;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        categoriaSelecionada = new ArrayList<Integer>();

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;

    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_filtro, null, false);

        botaoAmigos = (ToggleButton) view.findViewById(R.id.botaoAmigos);
        botaoProximidade = (ToggleButton) view.findViewById(R.id.botaoProximidade);
        raio = (SeekBar) view.findViewById(R.id.raio);
        raioNumero = (TextView) view.findViewById(R.id.raioNumero);
        linearCategorias = (View) view.findViewById(R.id.linear_categorias);
        textCategoria = (TextView) view.findViewById(R.id.text_categoria);
        proximidadeBusca = (View) view.findViewById(R.id.proximidadeBusca);
        buttonFiltro = (Button) view.findViewById(R.id.button_filtro);

        this.configView();

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.populaView();

    }

    private void populaView() {
        filtro = Helper.getFiltro();

        if (filtro != null) {

            if (filtro.getTipoFiltro() == Filtro.TipoFiltro.filtroAmigos) {

                onClick(botaoAmigos);
            } else {
                onClick(botaoProximidade);

                raio.setProgress(filtro.getRaio());
                raioNumero.setText(filtro.getRaio() + " km");

            }

        }
        else {
            filtro = new Filtro();
            onClick(botaoAmigos);
        }

    }


    private void configView() {

        botaoAmigos.setOnClickListener(this);
        botaoProximidade.setOnClickListener(this);
        buttonFiltro.setOnClickListener(this);
        raio.setOnSeekBarChangeListener(this);
        linearCategorias.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view == botaoAmigos) {

            botaoProximidade.setChecked(false);
            botaoAmigos.setChecked(true);
            proximidadeBusca.setVisibility(View.GONE);
            filtro.setTipoFiltro(Filtro.TipoFiltro.filtroAmigos);


        } else if (view == botaoProximidade) {

            botaoProximidade.setChecked(true);
            botaoAmigos.setChecked(false);
            proximidadeBusca.setVisibility(View.VISIBLE);
            filtro.setTipoFiltro(Filtro.TipoFiltro.filtroProximidade);


        }
        else if(view == linearCategorias){

            showDialogCategoria();

        }
        else {


            if (listener != null) {

                if (filtro.getTipoFiltro() == Filtro.TipoFiltro.filtroProximidade) {
                    verificaGPS();
                }

                filtro.setCategoriaId(categoriaSelecionada.size() > 0 ? categoriaSelecionada : null);

                Helper.setFiltro(filtro);

                listener.onFiltrar(filtro);
            }

            this.dismiss();

        }


    }

    private void showDialogCategoria() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Selecione uma Categoria");
        dialog.setContentView(R.layout.dialog_categoria);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);


        ListView listView = (ListView) dialog.findViewById(R.id.list_categorias);

        Button buttonClose = (Button)dialog.findViewById(R.id.button_close);

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });


        CategoriaAdapter produtoAdapter = new CategoriaAdapter(getActivity(),categorias,dialog,true);

        produtoAdapter.setListener(this);
        listView.setAdapter(produtoAdapter);



        try {
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }


    private void verificaGPS() {

        if (((GpsActivity) activity).mLocationClient.isConnected()) {

            Location lastLocation = ((GpsActivity) activity).mLocationClient.getLastLocation();

            if (lastLocation == null)
                return;

            Geocoder geo = new Geocoder(activity, Locale.getDefault());  //geo = new Geocoder(getActivity(),Locale.getDefault());

            if (!geo.isPresent()) {

                Localizacao localizacao = new Localizacao();
                localizacao.Latitude = (float) lastLocation.getLatitude();
                localizacao.Longitude = (float) lastLocation.getLongitude();
                localizacao.Descricao = "";

                filtro.setLocalizacao(localizacao);


            } else {

                List<Address> addresses = null;
                try {
                    addresses = geo.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                if (addresses != null && addresses.size() > 0) {

                    Localizacao localizacao = new Localizacao();
                    localizacao = new Localizacao();
                    localizacao.Latitude = (float) lastLocation.getLatitude();
                    localizacao.Longitude = (float) lastLocation.getLongitude();
                    localizacao.Descricao = String.format("%s, %s",
                            addresses.get(0).getSubAdminArea(),
                            addresses.get(0).getAdminArea());

                    filtro.setLocalizacao(localizacao);

                } else {

                    Localizacao localizacao = new Localizacao();
                    localizacao.Latitude = (float) lastLocation.getLatitude();
                    localizacao.Longitude = (float) lastLocation.getLongitude();
                    localizacao.Descricao = "";

                    filtro.setLocalizacao(localizacao);

                }
            }


        }
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

        progress = progress + 1;
        raioNumero.setText(progress + " km");

        filtro.setRaio(progress);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void setListener(FiltroDialogListener listener) {
        this.listener = listener;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onSelectCategoria(Categoria categoria, DialogInterface dialogInterface) {


        if(categoriaSelecionada.contains(categoria.CategoriaId)){
            categoriaSelecionada.remove(Integer.valueOf(categoria.CategoriaId));
        }
        else {
            categoriaSelecionada.add(categoria.CategoriaId);

        }

        StringBuilder stringBuilder = new StringBuilder();

        for(int i = 0; i < categoriaSelecionada.size(); i++){

           stringBuilder.append(categorias[i].Nome);

            if(i < categoriaSelecionada.size() -1){

                stringBuilder.append(",");
            }
        }

        textCategoria.setText(stringBuilder.toString());




    }

    @Override
    public void onSelectCategoriaForRequest(Categoria categoria, DialogInterface dialogInterface) {

    }
}
