package br.com.iterative.chance.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.HashMap;
import java.util.Map;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.fragments.ListaPedidosAjudaFragment;
import br.com.iterative.chance.listeners.CategoriaListener;
import br.com.iterative.chance.models.Categoria;
import br.com.iterative.chance.views.CategoriaAjudaItemView;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 10/28/14.
 */
public class CategoriaAdapter extends BaseAdapter {

    Map<Integer, Categoria> categoriasSelecionadas;

    CategoriaListener listener;

    public void setListener(CategoriaListener listener) {
        this.listener = listener;
    }

    Activity activity;
    Categoria[] categorias;
    DialogInterface dialogInterface;
    boolean displayCheckBox = false;


    public CategoriaAdapter(Activity activity, Categoria[] categorias,DialogInterface dialogInterface,boolean displayCheckBox){

        categoriasSelecionadas = new HashMap<Integer, Categoria>();
        this.activity = activity;
        this.categorias = categorias;
        this.dialogInterface = dialogInterface;
        this.displayCheckBox = displayCheckBox;
    }

    public CategoriaAdapter(Activity activity, Categoria[] categorias,DialogInterface dialogInterface){

        this.activity = activity;
        this.categorias = categorias;
        this.dialogInterface = dialogInterface;
    }

    @Override
    public int getCount() {
        return categorias.length;
    }

    @Override
    public Object getItem(int i) {
        return categorias[i];
    }

    @Override
    public long getItemId(int i) {
        return categorias[i].CategoriaId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        CategoriaAjudaItemView categoriaAjudaItemView = (CategoriaAjudaItemView) View.inflate(activity, R.layout.categoria_ajuda_item, null);
        return preencherCategoriaAjudaItemView(categoriaAjudaItemView, i);
    }


    private CategoriaAjudaItemView preencherCategoriaAjudaItemView(final CategoriaAjudaItemView categoriaAjudaItemView, final int position) {


        if(displayCheckBox){

            categoriaAjudaItemView.getCheckSelecionada().setVisibility(View.VISIBLE);
        }
        else
            categoriaAjudaItemView.getCheckSelecionada().setVisibility(View.GONE);



        categoriaAjudaItemView.getNome().setText(categorias[position].Nome);
        categoriaAjudaItemView.getDescricao().setText(categorias[position].Descricao);

        categoriaAjudaItemView.getFoto().setImageResource(R.drawable.icone_96x96);

        if(categorias[position].Foto.Url != null)
            Helper.downloadImageFromFoto(categoriaAjudaItemView.getFoto(), categorias[position].Foto);



        Categoria selected = null;
        if(displayCheckBox) {
            selected = categoriasSelecionadas.get(categorias[position].CategoriaId);

            categoriaAjudaItemView.getCheckSelecionada().setChecked(selected != null);
        }


        View.OnClickListener onClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(listener != null){

                    if(displayCheckBox) {

                        Categoria newCat = categoriasSelecionadas.get(categorias[position].CategoriaId);
                        if (newCat != null) {
                            categoriasSelecionadas.remove(categorias[position].CategoriaId);
                            categoriaAjudaItemView.getCheckSelecionada().setChecked(false);
                        } else {
                            categoriaAjudaItemView.getCheckSelecionada().setChecked(true);
                            categoriasSelecionadas.put(categorias[position].CategoriaId, categorias[position]);
                        }

                        listener.onSelectCategoria(categorias[position], dialogInterface);
                    }
                    else
                        listener.onSelectCategoriaForRequest(categorias[position],dialogInterface);

                }

            }
        };

        categoriaAjudaItemView.getFoto().setOnClickListener(onClickListener);
        categoriaAjudaItemView.getCell().setOnClickListener(onClickListener);

        return categoriaAjudaItemView;

    }
}
