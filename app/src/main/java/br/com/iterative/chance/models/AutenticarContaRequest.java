package br.com.iterative.chance.models;

public class AutenticarContaRequest extends RequestBase {
	public AutenticarContaRequest(String email, String senha) {
		this.Email = email;
		this.Senha = senha;
	}

	public AutenticarContaRequest(String facebookToken) {
		this.FacebookToken = facebookToken;
	}

	private String Email;
	private String Senha;
	private String FacebookToken;
}
