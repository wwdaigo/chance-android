package br.com.iterative.chance.models;

public class ProcurarAmigosRequest extends RequestBase {
	public String Q;

	public ProcurarAmigosRequest(String Token, String Q) {
		this.Token = Token;
		this.Q = Q;
	}
}
