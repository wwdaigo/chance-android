package br.com.iterative.chance.models;

public class CriarPostRequest extends RequestBase {

	public String Texto;
	public Localizacao Localizacao;
	public String[] Pessoas;
	public String[] Midias;
	int TipoAjuda;

    public CriarPostRequest(){

    }

	public CriarPostRequest(String token, String texto,
			Localizacao localizacao, String[] pessoas, String[] midias, int tipoAjuda) {
		super();
		this.Token = token;
		this.Texto = texto;
		this.Localizacao = localizacao;
		this.Pessoas = pessoas;
		this.Midias = midias;
		this.TipoAjuda = tipoAjuda;
	}

}
