package br.com.iterative.chance.listeners;

import java.util.List;

import br.com.iterative.chance.models.Autor;
import br.com.iterative.chance.models.Contagiou;
import br.com.iterative.chance.models.Pessoa;

/**
 * Created by pedrohenriqueborges on 12/22/14.
 */
public interface QuemContagiouListener {

    void quemContagiou(Contagiou[] pessoas);
}
