package br.com.iterative.chance.tasks;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.chance.models.ProcurarAmigosResponse;
import br.com.iterative.chance.models.SeguirPerfilRequest;

public class SeguirPerfilTask extends
		AsyncTask<Object, Object, ProcurarAmigosResponse> {

	private Context context;
	private View viewToHiddenIfSucess;
	private Pessoa pessoa;
	private int perfilId;
	private ProgressDialog progressDialog;

	@Override
	protected void onPreExecute() {
		progressDialog = Helper.showProgressDialog(context);
		super.onPreExecute();
	}

	public SeguirPerfilTask(Context context, View viewToHiddenIfSucess,
			int perfilId, Pessoa pessoa) {
		this.context = context;
		this.viewToHiddenIfSucess = viewToHiddenIfSucess;
		this.pessoa = pessoa;
		this.perfilId = perfilId;
	}

	protected ProcurarAmigosResponse doInBackground(Object... params) {

		SeguirPerfilRequest request = new SeguirPerfilRequest(
				Helper.getToken(), perfilId);

		return Helper.post(Helper.URL_POST_RELACIONAMENTO_SEGUIR_PERFIL,
				request, ProcurarAmigosResponse.class);
	}

	protected void onPostExecute(ProcurarAmigosResponse result) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		if (result.Sucesso) {
			viewToHiddenIfSucess.setVisibility(View.INVISIBLE);
			if (pessoa != null) {
				pessoa.Seguindo = true;
			}
		} else {
			new AlertDialog.Builder(this.context).setMessage(result.Mensagem)
					.setPositiveButton("OK", null).create().show();
		}
	}
}