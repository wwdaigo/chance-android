package br.com.iterative.chance.models;

public class SolicitarAmizadeRequest extends RequestBase {
	public int PerfilId;

	public SolicitarAmizadeRequest(String Token, int PerfilId) {
		this.Token = Token;
		this.PerfilId = PerfilId;
	}
}
