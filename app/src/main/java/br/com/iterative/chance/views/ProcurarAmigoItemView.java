package br.com.iterative.chance.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.iterative.rede.chance.R;

public class ProcurarAmigoItemView extends LinearLayout {

	Button botaoAdicionar;

	Button botaoSeguir;

	LinearLayout botoes;

	ImageView fotoPerfil;

	TextView nomePerfil;
	TextView pontuacaoPerfil;
	TextView nivelPerfil;

	public ProcurarAmigoItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Button getBotaoAdicionar() {
		return botaoAdicionar;
	}
	

	public Button getBotaoSeguir() {
		return botaoSeguir;
	}

	public LinearLayout getBotoes() {
		return botoes;
	}

	public ImageView getFotoPerfil() {
		return fotoPerfil;
	}

	public TextView getNomePerfil() {
		return nomePerfil;
	}
	
	public TextView getPontuacaoPerfil() {
		return pontuacaoPerfil;
	}
	
	public TextView getNivelPerfil() {
		return nivelPerfil;
	}

	@Override
	protected void onFinishInflate() {

		fotoPerfil = (ImageView) findViewById(R.id.foto_perfil);
		nomePerfil = (TextView) findViewById(R.id.nome_perfil);
		pontuacaoPerfil = (TextView) findViewById(R.id.pontuacao_perfil);
		nivelPerfil = (TextView) findViewById(R.id.nivel_perfil);
		botoes = (LinearLayout) findViewById(R.id.botoes_perfil);
		botaoAdicionar = (Button) findViewById(R.id.botao_adicionar);
		botaoSeguir = (Button) findViewById(R.id.botao_seguir);
	}
}
