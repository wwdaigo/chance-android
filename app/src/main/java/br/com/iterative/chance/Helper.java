package br.com.iterative.chance;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.util.Locale;

import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.models.Filtro;
import br.com.iterative.chance.models.Foto;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.chance.models.Post;
import br.com.iterative.chance.models.RequestBase;
import br.com.iterative.chance.models.VerPerfilContaResponse;
import br.com.iterative.chance.views.FeedItemView;
import br.com.iterative.rede.chance.R;

public class Helper {

    //GOTO
//    public static final String BAipSE_URL = "http://10.10.10.195/Chance.API/api/";
//    //DEV
    public static final String BASE_URL = "http://192.168.43.57/ChanceAPI/api/";

    //CASA
//    public static final String BASE_URL = "http://192.168.0.11/Chance.API/api/";


//    public static final String VIEW_POST_URL = "http://10.10.10.196/Chance.API/ViewPost/Details/";
//    public static final String VIEW_POST_URL = "http://tda.cloudapp.net/ChanceAPI/ViewPost/Details/";

    public static final String VIEW_POST_URL = "http://chanceapp.com.br/chance.api/ViewPost/Details/";

    //Hospital
    //public static final String BASE_URL = "http://192.168.43.233/Chance.API/api/";
//    public static final String BASE_URL = "http://tda.cloudapp.net/ChanceAPI/api/";
    //public static final String BASE_URL = "http://ec2-54-187-19-8.us-west-2.compute.amazonaws.com/Chance.API/Api/";
    //public static final String BASE_URL = "http://chanceweb.cloudapp.net/chance.api/api/";
//    public static final String BASE_URL = "http://chanceapp.com.br/chance.api/api/";
    public static final String URL_POST_CONTA_VER_PERFIL = BASE_URL
            + "/Conta/VerPerfil";
    public static final String URL_POST_FEED_DE_AMIGOS = BASE_URL
            + "/Feed/DeAmigos";
    public static final String URL_POST_FEED_DE_PERFIL = BASE_URL
            + "/Feed/DePerfil";
    public static final String URL_POST_FEED_GLOBAL = BASE_URL + "/Feed/Global";
    public static final String URL_POST_POST_COMENTAR = BASE_URL
            + "/Post/Comentar";
    public static final String URL_POST_COMENTARIO_EXCLUIR = BASE_URL
            + "/Post/ExcluirComentario";
    public static final String URL_POST_POST_CONTAGIAR = BASE_URL
            + "/Post/Contagiar";
    public static final String URL_POST_POST_DENUNCIAR = BASE_URL
            + "/Post/Denunciar";
    public static final String URL_POST_EXCLUIR = BASE_URL
            + "/Post/ExcluirPost";
    public static final String URL_POST_POST_VER_POST = BASE_URL
            + "/Post/VerPost";
    public static final String URL_POST_POST_CRIAR = BASE_URL + "/Post/Criar";
    public static final String URL_POST_RELACIONAMENTO_PROCURAR_AMIGOS = BASE_URL
            + "Relacionamento/ProcurarAmigos";
    public static final String URL_POST_RELACIONAMENTO_PROCURAR = BASE_URL
            + "Relacionamento/Procurar";
    public static final String URL_POST_RELACIONAMENTO_LISTAR = BASE_URL
            + "Relacionamento/Listar";
    public static final String URL_POST_RELACIONAMENTO_SOLICITAR_AMIZADE = BASE_URL
            + "Relacionamento/SolicitarAmizade";
    public static final String URL_POST_RELACIONAMENTO_SEGUIR_PERFIL = BASE_URL
            + "Relacionamento/SeguirPerfil";
    public static final String URL_POST_AJUDA_RESUMO = BASE_URL
            + "/Ajuda/Resumo";
    public static final String URL_POST_AJUDA_VER = BASE_URL
            + "/Ajuda/Ver";
    public static final String URL_POST_AJUDA_EXCLUIR = BASE_URL
            + "/Ajuda/Excluir";
    public static final String URL_POST_MIDIA_POST_FILE = BASE_URL //"http://10.10.10.162/Chance.API/api" //BASE_URL
            + "/Midia/PostFile";
    public static final int SIZE_TRUNCATED_TEXTO = 200;
    public static final int SELECT_PHOTO = 1000;
    public static final int CHANGE_PHOTO = 666;
    public static final String PERFIL_ID = "br.com.iterative.chance.Helper.PERFIL_ID";
    public static final String POST_ID = "br.com.iterative.chance.He1024x1024.png1024x1024.pnglper.POST_ID";
    public static final String SHARED_PREFERENCES = "br.com.iterative.chance.Helper.SHARED_PREFERENCES";
    public static final String URL = "br.com.iterative.chance.Helper.URL";
    public static final int PLATAFORMA = 2;
    private static final String PROPERTY_REG_ID = "REG_ID";
    public static SharedPreferences preferences;
    public static Post PostAtual;
    private static LruCache<String, Bitmap> imageMemoryCache;
    private static VerPerfilContaResponse perfil;
    private static Gson gson;
    private static GoogleCloudMessaging gcm;
    private static String SENDER_ID = "45133707798";
    private static String regId = null;
    private static FeedItemView feedItemViewAtual;

    public static String getIdioma() {
        return Locale.getDefault().toString();
    }

    public static LruCache<String, Bitmap> getImageMemoryCache() {
        return imageMemoryCache;
    }

    public static void setImageMemoryCache(
            LruCache<String, Bitmap> newImageMemoryCache) {
        imageMemoryCache = newImageMemoryCache;
    }

    public static int getPerfil() {
        return preferences == null ? 0 : preferences.getInt("perfil", 0);
    }

    public static void setPerfil(int perfilId) {
        System.out.println("perfilperfil set " + perfilId);
        preferences.edit().putInt("perfil", perfilId).commit();
    }

    public static VerPerfilContaResponse getPerfilConta() {
        return perfil;
    }

    public static void setPerfilConta(VerPerfilContaResponse perfil) {
        Helper.perfil = perfil;
    }

    public static String getToken() {
        return preferences != null ? preferences.getString("token", null) : null;
    }

    public static void setToken(String token) {
        preferences.edit().putString("token", token).commit();
    }

    public static void setLoggedWithFacebook(Boolean isTrue) {

        preferences.edit().putBoolean("LoggedFacebook", isTrue).commit();
    }

    public static Boolean getLoggetWithFacebook() {

        return preferences.getBoolean("LoggedFacebook", false);
    }

    public static Filtro getFiltro() {


        String jsonFiltro = preferences.getString("filtro", "");


        if (gson == null)
            gson = new Gson();

        Filtro filtro = new Filtro();

        try {
            filtro = gson.fromJson(jsonFiltro, Filtro.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return filtro;

    }

    public static void setFiltro(Filtro filtro) {

        if (gson == null)
            gson = new Gson();

        String json = gson.toJson(filtro);

        preferences.edit().putString("filtro", json).commit();

    }

    public static boolean isValidEmailAddress(String email) {
        java.util.regex.Pattern p = java.util.regex.Pattern
                .compile(".+@.+\\.[a-z]+");
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static synchronized void downloadImageFromFoto(ImageView image, Foto foto) {
        downloadImageFromURL(image, foto == null ? null : foto.Url);
    }

    public static synchronized void downloadImageFromURL(ImageView image, String url) {
        System.out.println("teste image " + image + " url " + url);
        image.setImageResource(R.drawable.imgdefaultperfil);
        if (url != null && !url.equals("")) {
            ImageLoader.getInstance().displayImage(url, image);
            System.out.println("teste loaded image " + image + " url " + url);
        } else {
            System.out.println("teste rejected image " + image);
            image.setImageResource(R.drawable.imgdefaultperfil);
        }
    }

    public static <TResponse, TRequest extends RequestBase> TResponse post(String url,
                                                                           TRequest request, Class<TResponse> classOfResponse) {


        String jsonTest = "'Autor': {'Pontuacao': 7,'Nivel': '4','PerfilId': 206,'Apelido': 'pedrot birged',' Foto ': null,'Amizade': false,'Seguindo': false}";

        Gson gson = new Gson();
        try {
            Pessoa o = gson.fromJson(jsonTest, Pessoa.class);
            System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(o));
        } catch (Exception e) {
            System.out.println("invalid json format");
        }


        if (gson == null)
            gson = new Gson();

        request.DeviceToken = getRegId();
        request.Plataforma = PLATAFORMA;

        String json = gson.toJson(request);

        Log.i("request", json);

        json = HttpRequest.post(url).contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(json).body();

        Log.i("response", json);

        return gson.fromJson(json, classOfResponse);
    }

    public static String getRegId() {
        return regId;

    }

    public static void registerDevice() {
        if (regId == null) {
            regId = preferences.getString(PROPERTY_REG_ID, null);
        }

        if (regId != null) {
            return;
        }
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(ChanceApplication.getContext());
            }
            if (regId == null) {
            }
            regId = gcm.register(SENDER_ID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ProgressDialog showProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Aguarde..");
        progressDialog.show();
        return progressDialog;
    }

    public static void showAlertDlialog(Context ctx, String title, String message, String okButtonTitle) {
        AlertDialog alertDiag;

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(okButtonTitle, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        alertDiag = builder.create();
        alertDiag.show();
    }

    public static FeedItemView getFeedItemViewAtual() {
        return feedItemViewAtual;
    }

    public static void setFeedItemViewAtual(FeedItemView feedItemViewAtual) {
        Helper.feedItemViewAtual = feedItemViewAtual;
    }

}
