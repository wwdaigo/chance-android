package br.com.iterative.chance.tasks;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.models.Pessoa;
import br.com.iterative.chance.models.ProcurarAmigosResponse;
import br.com.iterative.chance.models.SolicitarAmizadeRequest;

public class SolicitarAmizadeTask extends
		AsyncTask<Object, Object, ProcurarAmigosResponse> {

	private Context context;
	private View viewToHiddenIfSucess;
	private int perfilId;
	private Pessoa pessoa;
	private ProgressDialog progressDialog;

	public SolicitarAmizadeTask(Context context, View viewToHiddenIfSucess,
			int perfilId, Pessoa pessoa) {
		this.context = context;
		this.viewToHiddenIfSucess = viewToHiddenIfSucess;
		this.perfilId = perfilId;
		this.pessoa = pessoa;
	}

	@Override
	protected void onPreExecute() {
		progressDialog = Helper.showProgressDialog(context);
		super.onPreExecute();
	}

	protected ProcurarAmigosResponse doInBackground(Object... params) {

		SolicitarAmizadeRequest request = new SolicitarAmizadeRequest(
				Helper.getToken(), perfilId);

		return Helper.post(Helper.URL_POST_RELACIONAMENTO_SOLICITAR_AMIZADE,
				request, ProcurarAmigosResponse.class);
	}

	protected void onPostExecute(ProcurarAmigosResponse result) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		if (result.Sucesso) {
			viewToHiddenIfSucess.setVisibility(View.INVISIBLE);
			if (pessoa != null) {
				pessoa.Amizade = true;
			}
		} else {
			new AlertDialog.Builder(this.context).setMessage(result.Mensagem)
					.setPositiveButton("OK", null).create().show();
		}
	}
}