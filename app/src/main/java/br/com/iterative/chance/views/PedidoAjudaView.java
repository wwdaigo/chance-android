package br.com.iterative.chance.views;

import br.com.iterative.rede.chance.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PedidoAjudaView extends LinearLayout {

	public TextView nome;
	public TextView datahora;
	public TextView localizacao;
	public TextView descricao;
	public ImageView foto;
	public TextView textoBotao;
	public View botaoOferecerAjuda;
	public ImageButton icoBotao;
    public ImageButton botaoDenunciar;

	public PedidoAjudaView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		
		this.nome = (TextView) findViewById(R.id.nome);
		this.datahora = (TextView) findViewById(R.id.datahora);
		this.localizacao = (TextView) findViewById(R.id.localizacao);
		this.descricao = (TextView) findViewById(R.id.descricao);
		this.foto = (ImageView) findViewById(R.id.foto);
		this.botaoOferecerAjuda = findViewById(R.id.botaoOferecerAjuda);
		this.textoBotao = (TextView) findViewById(R.id.textoBotaoOferecerAjuda);
		this.icoBotao = (ImageButton) findViewById(R.id.icoBotaoOferecerAjuda);
        this.botaoDenunciar = (ImageButton)findViewById(R.id.botaoDenunciar);
	}
}
