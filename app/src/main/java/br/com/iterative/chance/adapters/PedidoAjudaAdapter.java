package br.com.iterative.chance.adapters;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.listeners.DenunciarListener;
import br.com.iterative.chance.models.Pedido;
import br.com.iterative.chance.tasks.OferecerAjudaTask;
import br.com.iterative.chance.views.PedidoAjudaView;
import br.com.iterative.rede.chance.R;

public final class PedidoAjudaAdapter extends BaseAdapter {

    private Activity activity;

	private Pedido[] pedidos;

    public Fragment fragment;






    public PedidoAjudaAdapter(Fragment fragment, Pedido[] pedidos) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
        this.pedidos = pedidos;
    }

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final PedidoAjudaView view = (PedidoAjudaView) View.inflate(this.activity, R.layout.pedido_ajuda_item, null);

		view.nome.setText(pedidos[position].Autor.Apelido);
	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
	    Date convertedDate = new Date();
	    String outDate;
	    try {
	        convertedDate = dateFormat.parse(pedidos[position].DataCriacao+" +000");
	        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	        outDate = dateFormatOut.format(convertedDate);
	    } 
	    catch (ParseException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        outDate = pedidos[position].DataCriacao; 
	    }
		
		
		view.datahora.setText(outDate);
		view.localizacao.setText(pedidos[position].Localizacao.Descricao);
		view.descricao.setText(pedidos[position].Texto);

		Helper.downloadImageFromFoto(view.foto, pedidos[position].Autor.Foto);

		if (pedidos[position].TipoAjuda == 1) {
			view.textoBotao.setText(ChanceApplication.getContext().getResources().getString(R.string.pedir_ajuda));
			view.icoBotao.setImageResource(R.drawable.icopedirajuda);
		}
		
		view.botaoOferecerAjuda.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new OferecerAjudaTask(PedidoAjudaAdapter.this).execute(pedidos[position]);
			}
		});

        view.botaoDenunciar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

                builder.setMessage(
                        ChanceApplication.getContext().getResources().getString(R.string.certeza_denunciar_post))
                        .setNegativeButton(ChanceApplication.getContext().getResources().getString(R.string.cancelar), null)
                        .setPositiveButton(ChanceApplication.getContext().getResources().getString(R.string.confirmar),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {

                                        view.botaoDenunciar.setImageResource(R.drawable.icodenunciarativo);

                                    }
                                });

                builder.create().show();

            }
        });

		return view;
	}

	@Override
	public int getCount() {
		return pedidos.length;
	}

	@Override
	public Object getItem(int position) {
		return pedidos[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}