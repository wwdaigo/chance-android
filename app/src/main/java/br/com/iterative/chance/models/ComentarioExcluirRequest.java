package br.com.iterative.chance.models;

public class ComentarioExcluirRequest extends RequestBase {
	public int ComentarioId;

	public ComentarioExcluirRequest(String Token, int ComentarioId) {
		this.Token = Token;
		this.ComentarioId = ComentarioId;
	}
}
