package br.com.iterative.chance.listeners;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.rede.chance.R;
import br.com.iterative.chance.models.PostIdRequest;
import br.com.iterative.chance.models.Post;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.chance.views.FeedItemView;

public class DenunciarListener implements OnClickListener {

	private final class DenunciarTask extends
			AsyncTask<Post, Object, ResponseBase> {

		private ProgressDialog progressDialog;
		private Context context;

		public DenunciarTask(Context context) {
			this.context = context;
		}

		protected ResponseBase doInBackground(Post... params) {
			PostIdRequest request = new PostIdRequest(Helper.getToken(),
					params[0].PostId);

			return Helper.post(Helper.URL_POST_POST_DENUNCIAR, request,
					ResponseBase.class);
		}

		protected void onPreExecute() {
			progressDialog = Helper.showProgressDialog(context);
			DenunciarListener.this.feedItemView.getBotaoDenunciar()
					.setImageResource(R.drawable.icodenunciarativo);
			DenunciarListener.this.post.setDenunciado(true);
		}

		protected void onPostExecute(ResponseBase result) {
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			if (!result.Sucesso) {
				DenunciarListener.this.feedItemView.getBotaoDenunciar()
						.setImageResource(R.drawable.icodenunciar);
				DenunciarListener.this.post.setDenunciado(false);

				Toast.makeText(context, result.Mensagem, Toast.LENGTH_LONG)
						.show();
			} else {
				post.setDenunciado(true);
			}
		}
	}

	private Post post;
	private Context context;
	private FeedItemView feedItemView;

	public DenunciarListener(Context context, final Post post,
			final FeedItemView feedItemView) {
		this.post = post;
		this.context = context;
		this.feedItemView = feedItemView;
	}

	@Override
	public void onClick(View v) {

		if (!this.post.isDenunciado()) {

			AlertDialog.Builder builder = new Builder(v.getContext());
			
			builder.setMessage(
					ChanceApplication.getContext().getResources().getString(R.string.certeza_denunciar_post))
					.setNegativeButton(ChanceApplication.getContext().getResources().getString(R.string.cancelar), null)
					.setPositiveButton(ChanceApplication.getContext().getResources().getString(R.string.confirmar),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									new DenunciarTask(context).execute(post);
								}
							});

			builder.create().show();

		}
	}

}
