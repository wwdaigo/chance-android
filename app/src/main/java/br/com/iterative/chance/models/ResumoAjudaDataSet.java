package br.com.iterative.chance.models;

public class ResumoAjudaDataSet {
	public Categoria[] Categorias;
	public Oferta[] MinhasOfertasPorPedido;
	public OfertaResposta[] OferecidasParaPedido;
	public Oferta[] MinhasOfertasPorOferecimento;
	public OfertaResposta[] AceitasPorOferta;

	public ResumoAjudaDataSet(Categoria[] Categorias,
			Oferta[] MinhasOfertas,
			OfertaResposta[] Oferecidas,
			Oferta[] MeusPedidos,
			OfertaResposta[] Pedidas) {
		this.Categorias = Categorias;
		this.MinhasOfertasPorPedido = MinhasOfertas;
		this.OferecidasParaPedido = Oferecidas;
		this.MinhasOfertasPorOferecimento = MeusPedidos;
		this.AceitasPorOferta = Pedidas;
	}
}
