package br.com.iterative.chance.models;

import java.util.ArrayList;

public class VerPerfilContaResponse extends ResponseBase {
	public String Apelido;
	public String DataNascimento;
	public int Amizade;
	public int Seguindo;
	public String Email;
	public int Idade;
	public String Nivel;
	public String Nome;
	public String NomeEmpresa;
	public boolean PerfilEmpresa;
	public int PerfilId;
	public int Pontuacao;
	public String Sexo;
	public String Sobrenome;
	public String URLFoto;
	public ArrayList<Oferta> ListaSolicitacoes;

}
