package br.com.iterative.chance.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.ComentarioAdapter;
import br.com.iterative.chance.models.BuscarComentariosRequest;
import br.com.iterative.chance.models.BuscarComentariosResponse;
import br.com.iterative.chance.models.ComentarRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.rede.chance.R;

public class ComentarioFragment extends Fragment {
    private EditText txtComentario;
    private ListView comentarios;
    private int postId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comentar, container, false);

        txtComentario = (EditText) view.findViewById(R.id.comentario);
        comentarios = (ListView) view.findViewById(R.id.comentarios);
        view.findViewById(R.id.botaoPostar).setOnClickListener(new PostarComentarioListener(getActivity(), getActivity()));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.comentarios));
        this.postId = getArguments().getInt(Helper.POST_ID, -1);
        new BuscarComentariosTask().execute(postId);
    }

    private final class BuscarComentariosTask extends AsyncTask<Integer, Object, BuscarComentariosResponse> {
        private boolean finalizar;

        public BuscarComentariosTask() {
        }

        public BuscarComentariosTask(boolean finalizar) {
            this.finalizar = finalizar;
        }

        protected void onPostExecute(BuscarComentariosResponse result) {

            if (!result.Sucesso) {
                Toast.makeText(ComentarioFragment.this.getActivity(), getResources().getString(R.string.nao_foi_possivel_comentarios), Toast.LENGTH_LONG).show();
                return;
            }

            comentarios.setAdapter(new ComentarioAdapter(getActivity(), result.Comentarios));
            if (finalizar) {
                txtComentario.setText("");
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtComentario.getWindowToken(), 0);

				/*FragmentActivity activity = (FragmentActivity) getActivity();
				FragmentManager fragmentManager = activity.getSupportFragmentManager();
				fragmentManager.popBackStack();*/
            }
        }

        protected BuscarComentariosResponse doInBackground(Integer... params) {
            BuscarComentariosRequest request = new BuscarComentariosRequest(params[0]);

            return Helper.post(Helper.BASE_URL + "/post/comentarios", request, BuscarComentariosResponse.class);
        }
    }


    private final class PostarComentarioListener implements OnClickListener {

        private Context context;
        private Activity activity;

        public PostarComentarioListener(Context context, Activity activity) {
            this.context = context;
            this.activity = activity;
        }

        public void onClick(View v) {

            if (txtComentario.getText().toString().trim().equals("")) {
                new AlertDialog.Builder(getActivity()).setMessage(getResources().getString(R.string.escreva_comentario)).setPositiveButton("OK", null).create().show();
                return;
            }

            new ComentarTask(this.context, this.activity).execute(postId);
        }
    }

    private final class ComentarTask extends AsyncTask<Integer, Object, ResponseBase> {

        private ProgressDialog progressDialog;
        private Context context;
        private Activity activity;

        public ComentarTask(Context context, Activity activity) {
            this.context = context;
            this.activity = activity;
        }

        protected ResponseBase doInBackground(Integer... params) {
            ComentarRequest request = new ComentarRequest(Helper.getToken(), params[0], ComentarioFragment.this.txtComentario.getText().toString());

            this.activity.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = Helper.showProgressDialog(context);
                }
            });

            return Helper.post(Helper.URL_POST_POST_COMENTAR, request, ResponseBase.class);
        }

        protected void onPostExecute(ResponseBase result) {
            if (result.Sucesso) {

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                TextView comentarioFeed = Helper.getFeedItemViewAtual().getComentarios();
                comentarioFeed.setText(String.valueOf(Integer.parseInt(comentarioFeed.getText().toString()) + 1));
                Helper.getFeedItemViewAtual().invalidate();


                new BuscarComentariosTask(true).execute(postId);
                Toast.makeText(ComentarioFragment.this.getActivity(), getResources().getString(R.string.comentario_enviado), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ComentarioFragment.this.getActivity(), result.Mensagem, Toast.LENGTH_LONG).show();
            }
        }
    }

}
