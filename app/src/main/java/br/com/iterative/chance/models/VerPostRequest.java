package br.com.iterative.chance.models;

public class VerPostRequest extends RequestBase {
	public int PostId;

	public VerPostRequest(String Token, int PostId) {
		this.Token = Token;
		this.PostId = PostId;
	}
}
