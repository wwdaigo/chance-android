package br.com.iterative.chance.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.Arrays;

import br.com.iterative.chance.FacebookUtil;
import br.com.iterative.chance.FileHelper;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.LinearLayoutThatDetectsSoftKeyboard;
import br.com.iterative.chance.fragments.AjudaFragment;
import br.com.iterative.chance.fragments.CentralAjudaFragment;
import br.com.iterative.chance.fragments.EditarPerfilFragment;
import br.com.iterative.chance.fragments.FeedFragment;
import br.com.iterative.chance.fragments.PerfilFragment;
import br.com.iterative.chance.fragments.PostarFragment;
import br.com.iterative.chance.fragments.ProcurarAmigoFragment;
import br.com.iterative.chance.fragments.SolicitacoesFragment;
import br.com.iterative.chance.listeners.AlterarFotoListener;
import br.com.iterative.chance.models.Midia;
import br.com.iterative.rede.chance.R;

public class PrincipalActivity extends GpsActivity implements
        OnClickListener{

    private ImageButton botaoAjuda;
    private ImageButton botaoFeed;
    private ImageButton botaoFeedGlobal;
    private ImageButton botaoPerfil;
    private ImageButton botaoPostar;
    private ImageButton botaoProcurarAmigo;
    private ImageButton botaoSolicitacoes;
    private ImageButton botaoConfiguracoes;
    private RelativeLayout topo;

    private View loading;

    private void inflate(Fragment fragment, String nameFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = null;
        transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.main_frame, fragment, nameFragment);
        transaction.commit();
    }

    public void setTitleBarText(String text) {
        ((TextView) findViewById(R.id.titulo)).setText(text);
    }

    PostarFragment postar = null;

    @Override
    public void onClick(View v) {
        if (v == botaoPerfil) {
            selectTabPerfil();
            Fragment fragment = new PerfilFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(Helper.PERFIL_ID, -1);
            fragment.setArguments(bundle);
            inflate(fragment, "Perfil");
        } else if (v == botaoFeed) {
            selectTabFeedAmigo();
            Fragment fragment = new FeedFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Helper.URL, Helper.URL_POST_FEED_DE_AMIGOS);
            bundle.putBoolean("CodigoUnico",true);
            fragment.setArguments(bundle);
            inflate(fragment, "FeedAmigos");
        } else if (v == botaoPostar) {
            selectTabPostar();
            postar = new PostarFragment();
            inflate(postar, "PostarFragment");
        } else if (v == botaoFeedGlobal) {
            selectTabFeedGlobal();
            Fragment fragment = new FeedFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Helper.URL, Helper.URL_POST_FEED_GLOBAL);
            fragment.setArguments(bundle);
            inflate(fragment, "FeedGlobal");
        } else if (v == botaoAjuda) {
            selectTabAjuda();
            inflate(new CentralAjudaFragment(), "CentralAjudaFragment");
        } else if (v == botaoSolicitacoes) {
            inflate(new SolicitacoesFragment(), null);
        } else if (v == botaoProcurarAmigo) {
            Fragment fragment = new ProcurarAmigoFragment();
            Bundle bundle = new Bundle();
            fragment.setArguments(bundle);
            inflate(fragment, null);
        } else if (v == botaoConfiguracoes) {
            final Dialog dialog = new Dialog(PrincipalActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_configuracoes);
            dialog.show();

            View logout = dialog
                    .findViewById(R.id.logout);
            logout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Helper.setToken(null);
                    Helper.setPerfil(0);
                    Session session = Session.getActiveSession();
                    if (session != null && (session.isOpened() || session.isClosed())) {
                        session.closeAndClearTokenInformation();
                        session.close();


                    }

                    Intent intent = new Intent(getBaseContext(), SplashActivity.class);
                    intent.putExtra(SplashActivity.LOGOUT,"Ok");

                    startActivity(intent);
                    dialog.dismiss();
                    finish();
                }
            });

            View editarCadastro = dialog
                    .findViewById(R.id.editarCadastro);
            editarCadastro.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = PrincipalActivity.this
                            .getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager
                            .beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.main_frame,
                            new EditarPerfilFragment());
                    transaction.commit();
                    dialog.dismiss();
                }
            });

            View tutorial = dialog
                    .findViewById(R.id.tutorial);
            tutorial.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    ArrayList<Midia> midias_ = new ArrayList<Midia>(7);
                    for (int i = 1; i < 7; i++) {
                        Midia midia = new Midia();
                        midia.Url = getResources().getString(R.string.tutorial_file) + i + ".png";
                        midias_.add(midia);
                    }
                    Intent intent = new Intent(PrincipalActivity.this, FullScreenViewActivity.class);

                    Gson gson = new Gson();
                    String _midias = gson.toJson(midias_.toArray(new Midia[]{}));
                    intent.putExtra("br.com.iterative.chance.MIDIAS", _midias);
                    String _posicao = gson.toJson(0);
                    intent.putExtra("br.com.iterative.chance.POSICAO", _posicao);
                    intent.putExtra("br.com.iterative.chance.ASSET", "true");

                    startActivity(intent);
                    dialog.dismiss();

                }
            });

            View sobre = dialog.findViewById(R.id.sobre);
            sobre.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Intent intent = new Intent(PrincipalActivity.this, SobreActivity.class);
                    startActivity(intent);
                    dialog.dismiss();
                    //finish();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {

        // Se for o primeiro Fragment de qualquer Aba fecha aplica��o
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        }
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal);


        topo = (RelativeLayout)this.findViewById(R.id.topo);


        final View activityRootView = findViewById(R.id.main_frame);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                    topo.setVisibility(View.GONE);
                }
                else {
                    topo.setVisibility(View.VISIBLE);
                }
            }
        });


        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true).cacheOnDisc(true).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).defaultDisplayImageOptions(
                defaultOptions).build();
        ImageLoader.getInstance().init(config);


        loading = (View)this.findViewById(R.id.loading);

        botaoPerfil = (ImageButton) this.findViewById(R.id.perfil);
        botaoFeed = (ImageButton) this.findViewById(R.id.feed);
        botaoPostar = (ImageButton) this.findViewById(R.id.postar);
        botaoFeedGlobal = (ImageButton) this.findViewById(R.id.feedGlobal);
        botaoAjuda = (ImageButton) this.findViewById(R.id.ajuda);
        botaoSolicitacoes = (ImageButton) this
                .findViewById(R.id.botaoSolicitacoes);
        botaoProcurarAmigo = (ImageButton) this
                .findViewById(R.id.botaoProcurarAmigo);
        botaoConfiguracoes = (ImageButton) this
                .findViewById(R.id.botaoConfiguracoes);

        botaoPerfil.setOnClickListener(this);
        botaoFeed.setOnClickListener(this);
        botaoPostar.setOnClickListener(this);
        botaoFeedGlobal.setOnClickListener(this);
        botaoAjuda.setOnClickListener(this);
        botaoSolicitacoes.setOnClickListener(this);
        botaoProcurarAmigo.setOnClickListener(this);
        botaoConfiguracoes.setOnClickListener(this);



        Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
        String ids_ = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString("ids", "");
        boolean tutorial = true;

        Gson gson = new Gson();
        if (ids_.equals("")) {
            ids_ = "[]";

            gson = new Gson();
            String json = gson.toJson(new String[]{});
            prefsEditor.putString("ids", json);
            prefsEditor.commit();

        } else {
            String[] ids = gson.fromJson(ids_, String[].class);

            for (String id : ids) {
                if (id.equals(Integer.toString(Helper.getPerfil()))) {
                    tutorial = false;
                }
            }
        }

        if (tutorial) {
            String[] ids = gson.fromJson(ids_, String[].class);
            ArrayList<String> IDS = new ArrayList<String>(Arrays.asList(ids));

            IDS.add(Integer.toString(Helper.getPerfil()));
            ids = IDS.toArray(ids);

            ids_ = gson.toJson(ids);
            prefsEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
            prefsEditor.putString("ids", ids_);
            prefsEditor.commit();

            ArrayList<Midia> midias_ = new ArrayList<Midia>(7);
            for (int i = 1; i < 7; i++) {
                Midia midia = new Midia();
                midia.Url = "tutorial" + i + ".png";
                midias_.add(midia);
            }
            Intent intent = new Intent(PrincipalActivity.this, FullScreenViewActivity.class);

            gson = new Gson();
            String _midias = gson.toJson(midias_.toArray(new Midia[]{}));
            intent.putExtra("br.com.iterative.chance.MIDIAS", _midias);
            String _posicao = gson.toJson(0);
            intent.putExtra("br.com.iterative.chance.POSICAO", _posicao);
            intent.putExtra("br.com.iterative.chance.ASSET", "true");

            startActivity(intent);
        }
    }

    private boolean paused;

    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!paused) {
            botaoPerfil.performClick();
        }
        paused = false;
    }

    public void navegarParaAjuda() {
        botaoAjuda.performClick();
    }

    public void navegarParaFeedDeAmigos() {
        botaoFeed.performClick();
    }

    public void navegarParaPerfil() {
        botaoPerfil.performClick();
    }

    private void selectTabAjuda() {
        cleanBackStack();
        botaoPerfil.setImageResource(R.drawable.perfilnormal);
        botaoAjuda.setImageResource(R.drawable.ajudaativo);
        botaoFeed.setImageResource(R.drawable.feedamigonormal);
        botaoFeedGlobal.setImageResource(R.drawable.feedglobalnormal);
        botaoPostar.setImageResource(R.drawable.portarnormal);
    }

    private void selectTabFeedAmigo() {
        cleanBackStack();
        botaoPerfil.setImageResource(R.drawable.perfilnormal);
        botaoAjuda.setImageResource(R.drawable.ajudanormal);
        botaoFeed.setImageResource(R.drawable.feedamigoativo);
        botaoFeedGlobal.setImageResource(R.drawable.feedglobalnormal);
        botaoPostar.setImageResource(R.drawable.portarnormal);
    }

    private void selectTabFeedGlobal() {
        cleanBackStack();
        botaoPerfil.setImageResource(R.drawable.perfilnormal);
        botaoAjuda.setImageResource(R.drawable.ajudanormal);
        botaoFeed.setImageResource(R.drawable.feedamigonormal);
        botaoFeedGlobal.setImageResource(R.drawable.feedglobalativo);
        botaoPostar.setImageResource(R.drawable.portarnormal);
    }

    private void selectTabPerfil() {
        cleanBackStack();
        botaoPerfil.setImageResource(R.drawable.perfilativo);
        botaoAjuda.setImageResource(R.drawable.ajudanormal);
        botaoFeed.setImageResource(R.drawable.feedamigonormal);
        botaoFeedGlobal.setImageResource(R.drawable.feedglobalnormal);
        botaoPostar.setImageResource(R.drawable.portarnormal);
    }

    private void selectTabPostar() {
        cleanBackStack();
        botaoPerfil.setImageResource(R.drawable.perfilnormal);
        botaoAjuda.setImageResource(R.drawable.ajudanormal);
        botaoFeed.setImageResource(R.drawable.feedamigonormal);
        botaoFeedGlobal.setImageResource(R.drawable.feedglobalnormal);
        botaoPostar.setImageResource(R.drawable.portarativo);
    }

    private void cleanBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode){
            case FacebookUtil.FACEBOOK_PERMISSION_CODE:

                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_frame);
                fragment.onActivityResult(requestCode, resultCode, imageReturnedIntent);

                break;
            default:
            {
                if (imageReturnedIntent != null) {
                    if (resultCode == RESULT_OK) {
                        Uri selectedImage = imageReturnedIntent.getData();

                        String fileRealPath = FileHelper.getRealPath(this,
                                selectedImage);

                        if (fileRealPath != null) {

                            if (requestCode == Helper.CHANGE_PHOTO) {
                                new AlterarFotoListener(this, fileRealPath).enviar();
                            } else {
                                postar.saveTempFileRealPath(fileRealPath);
                            }
                        } else {
                            new AlertDialog.Builder(this)
                                    .setMessage(
                                            getResources().getString(R.string.arquivo_invalido_local))
                                    .setPositiveButton("OK", null).create().show();
                        }
                    }
                }

            }
                break;
        }


    }

    public void setLoading(final boolean isLoading) {


        if(isLoading){

            botaoPerfil.setEnabled(false);
            botaoAjuda.setEnabled(false);
            botaoFeed.setEnabled(false);
            botaoFeedGlobal.setEnabled(false);
            botaoPostar.setEnabled(false);

        }
        else{

            botaoPerfil.setEnabled(true);
            botaoAjuda.setEnabled(true);
            botaoFeed.setEnabled(true);
            botaoFeedGlobal.setEnabled(true);
            botaoPostar.setEnabled(true);
        }

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            }
        });

    }


}
