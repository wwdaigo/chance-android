package br.com.iterative.app;

import android.app.Application;
import android.content.Context;

public class ChanceApplication extends Application{
	private static Context applicationContext;
	
	public void onCreate() {
		super.onCreate();
		
		applicationContext = this.getApplicationContext();
	}
	
    public static Context getContext() {
    	return applicationContext;
    }

}
