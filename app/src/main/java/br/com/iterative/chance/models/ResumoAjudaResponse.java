package br.com.iterative.chance.models;

public class ResumoAjudaResponse extends ResponseBase {
	public Categoria[] Categorias;
	public Oferta[] MinhasOfertasPorPedido;
	public OfertaResposta[] OferecidasParaPedido;
	public Oferta[] MinhasOfertasPorOferecimento;
	public OfertaResposta[] AceitasPorOferta;
}
