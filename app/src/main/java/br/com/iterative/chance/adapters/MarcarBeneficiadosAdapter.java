package br.com.iterative.chance.adapters;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.iterative.chance.Helper;
import br.com.iterative.rede.chance.R;
import br.com.iterative.chance.fragments.PostarFragment;
import br.com.iterative.chance.models.Pessoa;

public class MarcarBeneficiadosAdapter extends BaseAdapter {

	private Pessoa[] dataSet;

	public MarcarBeneficiadosAdapter(Pessoa[] pessoas) {
		this.dataSet = pessoas;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final LinearLayout marcarBeneficiadoItemView;
		final Pessoa pessoa = this.dataSet[position];
		final Context context = parent.getContext();

		if (convertView != null) {
			marcarBeneficiadoItemView = (LinearLayout) convertView;
		} else {
			marcarBeneficiadoItemView = (LinearLayout) View.inflate(context,
					R.layout.marcar_beneficiado_item, null);
		}

		LinearLayout cell = (LinearLayout) marcarBeneficiadoItemView
				.findViewById(R.id.cell);
		ImageView fotoPerfil = (ImageView) marcarBeneficiadoItemView
				.findViewById(R.id.foto_perfil);
		TextView nomePerfil = (TextView) marcarBeneficiadoItemView
				.findViewById(R.id.nome_perfil);

		Helper.downloadImageFromFoto(fotoPerfil, pessoa.Foto);
		nomePerfil.setText(pessoa.Apelido);

		cell.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PostarFragment.saveSelectPessoaOuEmailTemp(context,
						String.valueOf(pessoa.PerfilId), pessoa.Apelido);

			}
		});

		return marcarBeneficiadoItemView;
	}

	@Override
	public int getCount() {
		return this.dataSet.length;
	}

	@Override
	public Object getItem(int position) {
		return this.dataSet[position];
	}

	@Override
	public long getItemId(int position) {
		return this.dataSet[position].PerfilId;
	}

}
