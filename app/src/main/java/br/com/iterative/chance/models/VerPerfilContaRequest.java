package br.com.iterative.chance.models;

public class VerPerfilContaRequest extends RequestBase {

	private int Id;

	public VerPerfilContaRequest(String token, int id) {
		this.Token = token;
		this.Id = id;
	}

}
