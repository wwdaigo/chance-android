package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.iterative.chance.FacebookUtil;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.FeedAdapter;
import br.com.iterative.chance.listeners.PedirAjudaListener;
import br.com.iterative.chance.models.FeedsRequest;
import br.com.iterative.chance.models.FeedsResponse;
import br.com.iterative.chance.models.Foto;
import br.com.iterative.chance.models.Oferta;
import br.com.iterative.chance.models.VerPerfilContaRequest;
import br.com.iterative.chance.models.VerPerfilContaResponse;
import br.com.iterative.chance.tasks.SeguirPerfilTask;
import br.com.iterative.chance.tasks.SolicitarAmizadeTask;
import br.com.iterative.rede.chance.R;

public class PerfilFragment extends Fragment {

    public static boolean recarregarTela = false;
    public LinearLayout listaAjuda;
    public LinearLayout listaPosts;
    private Button botaoAdicionar;
    private Button botaoEditarPerfil;
    private Button botaoSeguir;
    private LinearLayout botoesAutor;
    private ImageView fotoPerfil;
    private TextView idadeSexo;
    private ImageView imgArgolas;
    private TextView nivel;
    private TextView nome;
    private ObterPerfilTask obterPerfilTask;
    private VerFeedsTask obterPostsTask;
    private View perfil;
    private int perfilId;
    private TextView pontuacao;
    private View progresso;
    private LinearLayout screen;
    private LinearLayout layoutAjuda;

    private FacebookUtil facebookUtil;

    /**
     * Esta tela � utilizada para abrir o perfil do usuario e de autores de
     * Post, quando for de autores de post, deve se alterar os bot�es para
     * seguir e adicionar.
     */
    private void adequarBotoesAoPerfil() {

        botaoEditarPerfil.setVisibility(View.GONE);

        if (this.perfilId > 0) {
            botoesAutor.setVisibility(View.VISIBLE);
            //botaoEditarPerfil.setVisibility(View.GONE);
        } else {
            botoesAutor.setVisibility(View.GONE);
            //botaoEditarPerfil.setVisibility(View.VISIBLE);

        }
    }

    private void configuraBotoes(final VerPerfilContaResponse response) {

        if (PerfilFragment.this.perfilId > 0) {
            if (response.Amizade == 1) this.botaoAdicionar.setVisibility(View.INVISIBLE);
            this.botaoAdicionar.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    new SolicitarAmizadeTask(getActivity(), v,
                            response.PerfilId, null).execute();

                }
            });
            if (response.Seguindo == 1) this.botaoSeguir.setVisibility(View.INVISIBLE);
            this.botaoSeguir.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    new SeguirPerfilTask(getActivity(), v, response.PerfilId,
                            null).execute();
                }
            });
        } else {
            botaoEditarPerfil.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getActivity()
                            .getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager
                            .beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.main_frame,
                            new EditarPerfilFragment());
                    transaction.commit();
                }
            });
        }
    }

    private void configuraDadosPessoais(VerPerfilContaResponse response) {
        // "aqui_vai_um_texto"
        if (response.Sexo != null) {
            String sexo = response.Sexo.equals("M") ? getResources()
                    .getStringArray(R.array.sexo)[0] : getResources()
                    .getStringArray(R.array.sexo)[1];

            idadeSexo.setText(String
                    .format("%s anos, %s", response.Idade, sexo));
        } else {
            idadeSexo.setText(String.format("%s anos", response.Idade));
        }
        System.out.println("testtest " + response.Apelido);
        nome.setText(response.Apelido);

        nivel.setText(response.Nivel);
    }

    private void configuraFotoPerfil(VerPerfilContaResponse response) {
        Helper.downloadImageFromURL(fotoPerfil, response.URLFoto);
    }

    private void configuraImgArgolas(VerPerfilContaResponse response) {

        int level = 0;

        if (response.Nivel != null)
            level = Integer.parseInt(response.Nivel);

        if (level == 0)
            imgArgolas.setImageResource(R.drawable.argolanivel0);
        else if (level == 1)
            imgArgolas.setImageResource(R.drawable.argolanivel1);
        else if (level == 2)
            imgArgolas.setImageResource(R.drawable.argolanivel2);
        else if (level == 3)
            imgArgolas.setImageResource(R.drawable.argolanivel3);
        else if (level == 4)
            imgArgolas.setImageResource(R.drawable.argolanivel4);
        else if (level == 5)
            imgArgolas.setImageResource(R.drawable.argolanivel5);
        else if (level >= 6)
            imgArgolas.setImageResource(R.drawable.argolanivel6);

        pontuacao.setText(String.valueOf(response.Pontuacao));
        nivel.setText(String.valueOf(response.Nivel));
    }

    private void configuraPerfil(VerPerfilContaResponse response) {
        PerfilFragment.this.configuraFotoPerfil(response);

        PerfilFragment.this.configuraDadosPessoais(response);

        PerfilFragment.this.configuraImgArgolas(response);

        PerfilFragment.this.configuraBotoes(response);

        if (this.perfilId < 0) {
            PerfilFragment.this.configuraAjuda(response);
            layoutAjuda.setVisibility(View.VISIBLE);
        } else {
            layoutAjuda.setVisibility(View.GONE);
        }
        perfil.setVisibility(View.VISIBLE);
        progresso.setVisibility(View.GONE);
    }

    private void configuraAjuda(VerPerfilContaResponse response) {

        try {
            for (Oferta oferta : response.ListaSolicitacoes) {
                View view = View.inflate(getActivity(), R.layout.oferta_ajuda_detalhe, null);
                final Foto autorFoto = oferta.Autor.Foto;
                final String txtNome = oferta.Autor.Apelido;
                final String categoriaDescricao = oferta.Categoria.Nome;
                final String dataCriacao = oferta.DataCriacao;
                final String localizacaoDescricao = oferta.Localizacao.Descricao;
                final String texto = oferta.Texto;
                final String textoDeContato = oferta.TextoDeContato;

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
                Date convertedDate = new Date();
                try {
                    convertedDate = dateFormat.parse(dataCriacao + " +000");
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                System.out.println("convertedDate Perfil " + dateFormatOut.format(convertedDate));


                Helper.downloadImageFromFoto((ImageView) view.findViewById(R.id.foto), autorFoto);
                ((TextView) view.findViewById(R.id.text_nome)).setText(txtNome);
                ((TextView) view.findViewById(R.id.categoria)).setText(categoriaDescricao);
                ((TextView) view.findViewById(R.id.data_criacao)).setText(dateFormatOut.format(convertedDate));
                ((TextView) view.findViewById(R.id.localizacao)).setText(localizacaoDescricao);

                ((TextView) view.findViewById(R.id.texto)).setText(texto);
                ((TextView) view.findViewById(R.id.texto_contato)).setText(textoDeContato);
                (view.findViewById(R.id.conteudo)).setTag(oferta.TipoAjuda);
                (view.findViewById(R.id.conteudo)).setOnClickListener(new PedirAjudaListener(this, oferta.Categoria.CategoriaId, oferta.Categoria.Foto.Url, oferta.PedidoId));
                listaAjuda.addView(view);

            }
        }
        catch (Exception ex){

            Toast.makeText(getActivity(), ex.getMessage(),Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }
//		}
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recarregarTela = true;

        facebookUtil = new FacebookUtil(savedInstanceState,getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ((PrincipalActivity) getActivity()).setLoading(true);

        recarregarTela = true;
        screen = (LinearLayout) inflater.inflate(R.layout.perfil, container,
                false);
        this.fotoPerfil = (ImageView) screen.findViewById(R.id.foto_perfil);
        this.imgArgolas = (ImageView) screen.findViewById(R.id.img_argolas);
        this.perfil = screen.findViewById(R.id.perfil);
        this.progresso = screen.findViewById(R.id.progresso);
        this.nome = (TextView) screen.findViewById(R.id.nome);
        this.idadeSexo = (TextView) screen.findViewById(R.id.idadeSexo);
        this.nivel = (TextView) screen.findViewById(R.id.nivel);
        this.pontuacao = (TextView) screen.findViewById(R.id.pontuacao);
        this.botaoEditarPerfil = (Button) screen.findViewById(R.id.botaoEditarPerfil);
        this.botoesAutor = (LinearLayout) screen.findViewById(R.id.botoesAutor);
        this.botaoAdicionar = (Button) screen.findViewById(R.id.botaoAdicionar);
        this.botaoSeguir = (Button) screen.findViewById(R.id.botaoSeguir);

        layoutAjuda = (LinearLayout) screen.findViewById(R.id.layoutAjuda);
        listaAjuda = (LinearLayout) screen.findViewById(R.id.listaAjuda);
        listaPosts = (LinearLayout) screen.findViewById(R.id.listaPosts);


        // "aqui_vai_um_texto"
        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.perfil));
        this.perfilId = getArguments().getInt(Helper.PERFIL_ID);
        // Caso o usuario clicou em sua propria foto em um post
        this.perfilId = this.perfilId == Helper.getPerfil() ? -1
                : this.perfilId;

        if (recarregarTela) {
            //this.progresso.setVisibility(View.VISIBLE);
            this.perfil.setVisibility(View.GONE);

            populaPerfil();

            recarregarTela = false;
        }


        return screen;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroy() {
        if (obterPerfilTask != null) {
            obterPerfilTask.cancel(true);
        }
        super.onDestroy();
    }

    private void populaPerfil() {

        adequarBotoesAoPerfil();

        obterPerfilTask = new ObterPerfilTask();
        obterPerfilTask.execute();
        /*
		if (this.perfilId < 0 && Helper.getPerfilConta() != null) {
            configuraPerfil(Helper.getPerfilConta());
        } else {
			obterPerfilTask = new ObterPerfilTask();
			obterPerfilTask.execute();
		}*/
    }

    private void populaPosts() {
        obterPostsTask = new VerFeedsTask();

        obterPostsTask.execute(Helper.getToken(), String.valueOf(this.perfilId < 0 ? Helper.getPerfil() : this.perfilId));
    }

    private void configuraPosts(FeedsResponse result) {
        ListAdapter adapter = new FeedAdapter(this, result.Posts,facebookUtil);
        final int adapterCount = adapter.getCount();

        for (int i = 0; i < adapterCount; i++) {
            View item = adapter.getView(i, null, null);
            listaPosts.addView(item);
        }

    }

    private final class ObterPerfilTask extends
            AsyncTask<Object, Object, Object> {

        private VerPerfilContaResponse response;

        protected Object doInBackground(Object... params) {
            if (!isCancelled()) {

                VerPerfilContaRequest request = new VerPerfilContaRequest(
                        Helper.getToken(),
                        PerfilFragment.this.perfilId < 0 ? Helper.getPerfil()
                                : PerfilFragment.this.perfilId
                );

                System.out.println("perfilperfil new perfilId " + PerfilFragment.this.perfilId);


                try {
                    response = Helper.post(Helper.URL_POST_CONTA_VER_PERFIL,
                            request, VerPerfilContaResponse.class);

                    if (PerfilFragment.this.perfilId < 0) {
                        Helper.setPerfilConta(response);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            if (!isCancelled()) {
                PerfilFragment.this.configuraPerfil(response);
            }

            populaPosts();

            obterPerfilTask = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!isCancelled()) {
                PerfilFragment.this.perfil.setVisibility(View.INVISIBLE);
                //PerfilFragment.this.progresso.setVisibility(View.VISIBLE);
            }
        }

    }

    private final class VerFeedsTask extends
            AsyncTask<String, Object, FeedsResponse> {

        protected FeedsResponse doInBackground(String... params) {
            if (!isCancelled()) {
                FeedsRequest request = new FeedsRequest(params[0], Integer.parseInt(params[1]));

                return Helper.post(Helper.URL_POST_FEED_DE_PERFIL, request,
                        FeedsResponse.class);
            } else {
                return null;
            }
        }

        protected void onPostExecute(FeedsResponse result) {


            if (result == null || isCancelled()) {


                ((PrincipalActivity) getActivity()).setLoading(false);

                return;
            }
            if (result.Sucesso) {
                PerfilFragment.this.configuraPosts(result);

            } else {
                new AlertDialog.Builder(getActivity()).setMessage(result.Mensagem).setPositiveButton("OK", null).create().show();
            }

            ((PrincipalActivity) getActivity()).setLoading(false);
        }
    }
}
