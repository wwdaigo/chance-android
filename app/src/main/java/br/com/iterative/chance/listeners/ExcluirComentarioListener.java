package br.com.iterative.chance.listeners;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import br.com.iterative.app.ChanceApplication;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.models.Comentario;
import br.com.iterative.chance.models.ComentarioExcluirRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.chance.views.ComentarioView;
import br.com.iterative.rede.chance.R;

public class ExcluirComentarioListener implements OnClickListener {
	private final class ExcluirTask extends AsyncTask<Comentario, Object, ResponseBase> {

		private ProgressDialog progressDialog;
		private Context context;

		public ExcluirTask(Context context) {
			this.context = context;
		}
		
		protected ResponseBase doInBackground(Comentario... params) {
			ComentarioExcluirRequest request = new ComentarioExcluirRequest(Helper.getToken(),
					params[0].ComentarioId);

			return Helper.post(Helper.URL_POST_COMENTARIO_EXCLUIR, request, ResponseBase.class);
		}

		protected void onPreExecute() {
			progressDialog = Helper.showProgressDialog(context);
		}

		protected void onPostExecute(ResponseBase result) {
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			if (!result.Sucesso) {

				Toast.makeText(context, result.Mensagem, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(context, ChanceApplication.getContext().getResources().getString(R.string.comentario_excluido_sucesso), Toast.LENGTH_LONG).show();
				comentarioView.excluir();
			}
		}
	}

	private Comentario comentario;
	private Context context;
	private ComentarioView comentarioView;

	public ExcluirComentarioListener(Context context, final Comentario comentario,
			final ComentarioView comentarioView) {
		this.comentario = comentario;
		this.context = context;
		this.comentarioView = comentarioView;
	}

	@Override
	public void onClick(View v) {

		AlertDialog.Builder builder = new Builder(v.getContext());

		builder.setMessage(
				ChanceApplication.getContext().getResources().getString(R.string.certeza_excluir_comentario))
				.setNegativeButton(ChanceApplication.getContext().getResources().getString(R.string.cancelar), null)
				.setPositiveButton(ChanceApplication.getContext().getResources().getString(R.string.confirmar),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								new ExcluirTask(context).execute(comentario);
							}
						});

		builder.create().show();

	}

}
