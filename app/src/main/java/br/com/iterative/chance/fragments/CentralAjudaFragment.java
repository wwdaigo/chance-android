package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.PedidoAjudaAdapter;
import br.com.iterative.chance.dialogs.FiltroDialog;
import br.com.iterative.chance.listeners.FiltroDialogListener;
import br.com.iterative.chance.models.Categoria;
import br.com.iterative.chance.models.Filtro;
import br.com.iterative.chance.models.PedidosPorCategoriaRequest;
import br.com.iterative.chance.models.PedidosPorCategoriaResponse;
import br.com.iterative.chance.models.ResumoAjudaRequest;
import br.com.iterative.chance.models.ResumoAjudaResponse;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 10/27/14.
 */
public class CentralAjudaFragment extends Fragment implements FiltroDialogListener, View.OnClickListener {

    TextView textQtdSolicitacoes;
    ListView listCentral;
    View viewSolicitacoes;
    Button buttonPedirAjuda;
    Button buttonFiltrar;

    Fragment context;
    ResumoAjudaResponse dataSet;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.central_de_ajuda));

        if (getActivity() != null)
            ((PrincipalActivity) getActivity()).setLoading(true);

        context = this;
        View view = inflater.inflate(R.layout.central_ajuda_fragment, container, false);


        textQtdSolicitacoes = (TextView) view.findViewById(R.id.text_qtd_solicitacoes);
        listCentral = (ListView) view.findViewById(R.id.list_central);
        buttonFiltrar = (Button) view.findViewById(R.id.button_filtrar);
        viewSolicitacoes = (View) view.findViewById(R.id.view_solicitacoes);
        buttonPedirAjuda = (Button) view.findViewById(R.id.button_pedir_ajuda);

        configView();


        new ResumoAjudaTask().execute();


        return view;

    }

    public void configView() {

        buttonFiltrar.setOnClickListener(this);
        viewSolicitacoes.setOnClickListener(this);
        buttonPedirAjuda.setOnClickListener(this);
    }

    public void pedirAjuda(View view) {

    }

    @Override
    public void onFiltrar(Filtro filtro) {


        PedidosPorCategoriaRequest request;
        if (filtro.getTipoFiltro() == Filtro.TipoFiltro.filtroAmigos) {


            request = new PedidosPorCategoriaRequest();

            if (filtro.getCategoriaId() == null || filtro.getCategoriaId().contains(0)) {

                request = new PedidosPorCategoriaRequest(0);
            } else {


                List<Categoria> cats = new ArrayList<Categoria>();

                for (Integer integer : filtro.getCategoriaId()) {

                    Categoria cat = new Categoria();
                    cat.CategoriaId = integer;
                    cats.add(cat);
                }

                request = new PedidosPorCategoriaRequest(null,cats );
            }
        } else {

            if (filtro.getLocalizacao() == null) {

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Ocorreu um erro ao obter sua localização", Toast.LENGTH_SHORT).show();
                    }
                });

                if (getActivity() != null)
                    ((PrincipalActivity) getActivity()).setLoading(false);
                return;


            } else {

                if (filtro.getCategoriaId() == null || filtro.getCategoriaId().contains(0)) {

                    request = new PedidosPorCategoriaRequest(0, filtro.getRaio(), filtro.getLocalizacao());
                } else {

                    List<Categoria> cats = new ArrayList<Categoria>();

                    for (Integer integer : filtro.getCategoriaId()) {

                        Categoria cat = new Categoria();
                        cat.CategoriaId = integer;
                        cats.add(cat);
                    }

                    request = new PedidosPorCategoriaRequest(null, filtro.getRaio(), filtro.getLocalizacao(), cats);
                }
            }

        }


        if (getActivity() != null)
            ((PrincipalActivity) getActivity()).setLoading(true);

        new ListarPedidosTask().execute(request);


    }


    @Override
    public void onClick(View view) {


        if (view == buttonFiltrar) {
            try {
                FiltroDialog filtroDialog = new FiltroDialog();
                filtroDialog.setCategorias(dataSet.Categorias);
                filtroDialog.setListener(this);
                filtroDialog.setActivity(getActivity());

                filtroDialog.show(getActivity().getSupportFragmentManager(), "Filtro");
            } catch (Exception e) {
                Toast.makeText(getActivity(),R.string.falha_conectar_servidor,Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (view == viewSolicitacoes) {

            FragmentManager fragmentManager = ((FragmentActivity) getActivity()).getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);

            SolicitacoesAjudaFragment fragment = new SolicitacoesAjudaFragment();
            fragment.setDataSet(dataSet);

            transaction.replace(R.id.main_frame, fragment);
            transaction.commit();

        } else {

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            PedirAjudaFragment ajudaFragment = new PedirAjudaFragment(dataSet.Categorias);
//            Bundle bundle = new Bundle();
//            bundle.putInt("categoriaId", categoriaId);
//            bundle.putString("categoriaFoto", categoriaFoto);
//            bundle.putInt("tipoAjuda", Integer.parseInt(v.getTag().toString()));
//            if(pedidoId > 0)
//                bundle.putInt("pedidoId", pedidoId);
//            ajudaFragment.setArguments(bundle);
            transaction.replace(R.id.main_frame, ajudaFragment);
            transaction.commit();

        }
    }

    private final class ListarPedidosTask extends AsyncTask<PedidosPorCategoriaRequest, Object, PedidosPorCategoriaResponse> {


        public ListarPedidosTask() {

        }

        @Override
        protected void onPostExecute(PedidosPorCategoriaResponse result) {
            super.onPostExecute(result);

            if (result.Sucesso) {
                listCentral.setAdapter(new PedidoAjudaAdapter(context, result.Pedidos));
            } else {

                Toast.makeText(getActivity(), result.Mensagem, Toast.LENGTH_SHORT);
            }

            if (getActivity() != null)
                ((PrincipalActivity) getActivity()).setLoading(false);

        }

        @Override
        protected PedidosPorCategoriaResponse doInBackground(PedidosPorCategoriaRequest... params) {
            return Helper.post(Helper.BASE_URL + "/ajuda/pedidosporcategoria", params[0], PedidosPorCategoriaResponse.class);
        }
    }


    private final class ResumoAjudaTask extends AsyncTask<Object, Object, ResumoAjudaResponse> {

        protected ResumoAjudaResponse doInBackground(Object... params) {
            ResumoAjudaRequest request = new ResumoAjudaRequest(Helper.getToken());

            return Helper.post(Helper.URL_POST_AJUDA_RESUMO, request, ResumoAjudaResponse.class);
        }

        protected void onPostExecute(ResumoAjudaResponse result) {
            if (result.Sucesso) {

                dataSet = result;

                int qtd = dataSet.MinhasOfertasPorPedido.length +
                        dataSet.MinhasOfertasPorOferecimento.length +
                        dataSet.AceitasPorOferta.length +
                        dataSet.OferecidasParaPedido.length;

                textQtdSolicitacoes.setText(String.valueOf(qtd));

                Filtro filtro = Helper.getFiltro();


                if (filtro == null) {
                    filtro = new Filtro();
                }

                filtro.setTipoFiltro(Filtro.TipoFiltro.filtroAmigos);

                onFiltrar(filtro);


            } else {

                if (getActivity() != null)
                    ((PrincipalActivity) getActivity()).setLoading(false);


                new AlertDialog.Builder(getActivity()).setMessage(result.Mensagem).setPositiveButton("OK", null).create().show();


            }


        }
    }

}
