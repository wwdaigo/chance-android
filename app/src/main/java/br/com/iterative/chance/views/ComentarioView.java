package br.com.iterative.chance.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.iterative.chance.adapters.ComentarioAdapter;
import br.com.iterative.rede.chance.R;

public class ComentarioView extends LinearLayout {

	public ComentarioView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImageView foto;
	public TextView apelido;
	public TextView texto;
	public Button botaoExcluir;
	public int comentarioId;
	public ComentarioAdapter comentarioAdapter;
	public int perfilId;
	
	
	protected void onFinishInflate() {
		foto = (ImageView) findViewById(R.id.foto);
		apelido = (TextView) findViewById(R.id.apelido);
		texto = (TextView) findViewById(R.id.texto);
		botaoExcluir = (Button) findViewById(R.id.botaoExcluir);
	}
	
	public void excluir(){
		comentarioAdapter.excluir(comentarioId);
	}

}
