package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.ProcurarAmigosAdapter;
import br.com.iterative.chance.models.ProcurarAmigosRequest;
import br.com.iterative.chance.models.ProcurarAmigosResponse;
import br.com.iterative.chance.models.RequestBase;
import br.com.iterative.rede.chance.R;

public class ProcurarAmigoFragment extends Fragment {

    public static ArrayList<ImageView> imageViewLoaded;
    ProgressDialog progressDialog;
    private ListView listaPessoas;
    //	private ListView listaAmigos;
    private View screenLayout;
    private TextView txtBusca;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        screenLayout = inflater.inflate(R.layout.procurar_amigo, container,
                false);

        return screenLayout;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.txtBusca.getWindowToken(), 0);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        ProcurarAmigoFragment.imageViewLoaded = new ArrayList<ImageView>();
        System.out.println("start");
        // "aqui_vai_um_texto"
        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.procurar_amigo));

        listaPessoas = (ListView) screenLayout.findViewById(R.id.lista_pessoas);
        // listaAmigos = (ListView) screenLayout.findViewById(R.id.lista_amigos);
        txtBusca = (TextView) screenLayout.findViewById(R.id.txt_busca);

        new RealizarBuscaTask().execute("");
        /*
		listaPessoas.setOnItemClickListener(new AdapterView.OnItemClickListener()
	    {
	    	@Override
	    	public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
	    	{
	    		System.out.println("AAAA "+parent);
	    	}
	    });
		*/

        ((Button) screenLayout.findViewById(R.id.botao_busca))
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        hideKeyboard();

                        if (ProcurarAmigoFragment.this.txtBusca.getText().toString().equals("")) {
                            // Alert
                            Helper.showAlertDlialog(getActivity(), getResources().getString(R.string.atencao_exclamacao), getResources().getString(R.string.digite_nome_amigo_procurar), getResources().getString(R.string.corrigir));
                        } else {
                            progressDialog = Helper.showProgressDialog(getActivity());

                            new RealizarBuscaTask()
                                    .execute(ProcurarAmigoFragment.this.txtBusca
                                            .getText().toString());
                        }

                    }
                });

        new ListarAmigosTask().execute();

    }

    @Override
    public void onDestroy() {
        this.listaPessoas.setAdapter(null);
        super.onDestroy();
    }

    private final class RealizarBuscaTask extends
            AsyncTask<String, Object, ProcurarAmigosResponse[]> {

        private String query;

        @Override
        protected ProcurarAmigosResponse[] doInBackground(String... params) {
            ProcurarAmigosResponse[] results = new ProcurarAmigosResponse[2];
            query = params[0];

            if (!query.equals("")) {
                ProcurarAmigosRequest request = new ProcurarAmigosRequest(
                        Helper.getToken(), query);

                ProcurarAmigosResponse procurarAmigosBusca = Helper.post(Helper.URL_POST_RELACIONAMENTO_PROCURAR,
                        request, ProcurarAmigosResponse.class);

                results[0] = procurarAmigosBusca;
            } else {
                results[0] = null;
            }

            ProcurarAmigosResponse procurarAmigos = Helper.post(Helper.URL_POST_RELACIONAMENTO_LISTAR,
                    new RequestBase(), ProcurarAmigosResponse.class);


            results[1] = procurarAmigos;

            return results;
            //return Helper.post(Helper.URL_POST_RELACIONAMENTO_PROCURAR, request, ProcurarAmigosResponse.class);
        }

        protected void onPostExecute(ProcurarAmigosResponse[] result) {

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (result[1].Sucesso) {
                int count;
                int count0 = 0;

                if (result[0] != null) count0 = result[0].Pessoas.length;

                count = count0 + result[1].Pessoas.length;
                if (count == 0) {
                    try {
                        Helper.showAlertDlialog(ProcurarAmigoFragment.this.getActivity(), getResources().getString(R.string.atencao_exclamacao), getResources().getString(R.string.nenhum_usuario_encontrado_exclamacao), getResources().getString(R.string.fechar));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }

                ProcurarAmigosAdapter adapter;

                if (result[0] != null)
                    adapter = new ProcurarAmigosAdapter(result[0].Pessoas, result[1].Pessoas);
                else
                    adapter = new ProcurarAmigosAdapter(null, result[1].Pessoas);

                ProcurarAmigoFragment.this.listaPessoas.setAdapter(adapter);
                ProcurarAmigoFragment.this.listaPessoas
                        .setVisibility(View.VISIBLE);
            } else {
                new AlertDialog.Builder(
                        ProcurarAmigoFragment.this.getActivity())
                        .setMessage(result[1].Mensagem)
                        .setPositiveButton("OK", null).create().show();
            }
        }
    }

    private final class ListarAmigosTask extends
            AsyncTask<Object, Object, ProcurarAmigosResponse> {

        @Override
        protected ProcurarAmigosResponse doInBackground(Object... params) {

            return Helper.post(Helper.URL_POST_RELACIONAMENTO_LISTAR,
                    new RequestBase(), ProcurarAmigosResponse.class);
        }

        protected void onPostExecute(ProcurarAmigosResponse result) {
            if (result.Sucesso) {

                //ListarAmigosAdapter adapter = new ListarAmigosAdapter(result.Pessoas);
//				ProcurarAmigoFragment.this.listaAmigos.setAdapter(adapter);
                //ProcurarAmigoFragment.this.listaAmigos.setVisibility(View.VISIBLE);
            } else {
                new AlertDialog.Builder(
                        ProcurarAmigoFragment.this.getActivity())
                        .setMessage(result.Mensagem)
                        .setPositiveButton("OK", null).create().show();
            }
        }
    }
}
