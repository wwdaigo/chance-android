package br.com.iterative.chance.listeners;

import br.com.iterative.chance.models.Filtro;

/**
 * Created by pedrohenriqueborges on 10/27/14.
 */
public interface FiltroDialogListener {

    void onFiltrar(Filtro filtro);

}
