package br.com.iterative.chance.tasks;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.models.AutenticarContaRequest;
import br.com.iterative.chance.models.AutenticarContaResponse;

public final class FacebookLoginTask extends AsyncTask<String, Object, AutenticarContaResponse> {

	private FragmentActivity activity;

    private DialogInterface.OnClickListener listener;

    public FacebookLoginTask(FragmentActivity activity, DialogInterface.OnClickListener  listener) {
        this.listener = listener;
		this.activity = activity;

	}

	@Override
	protected void onPostExecute(AutenticarContaResponse response) {
		super.onPostExecute(response);
		
		if (response.Sucesso) {
			Helper.setToken(response.Token);
			Helper.setPerfil(response.PerfilId);

            Helper.setLoggedWithFacebook(true);

			Intent intent = new Intent(activity, PrincipalActivity.class);
			activity.startActivity(intent);
			activity.finish();
		} else {
			new AlertDialog.Builder(activity).setMessage(response.Mensagem).setPositiveButton("OK", listener).create().show();
		}
	}

	@Override
	protected AutenticarContaResponse doInBackground(String... params) {

		AutenticarContaRequest request = new AutenticarContaRequest(params[0]);

		return Helper.post(Helper.BASE_URL + "/conta/autenticar", request, AutenticarContaResponse.class);
	}
}