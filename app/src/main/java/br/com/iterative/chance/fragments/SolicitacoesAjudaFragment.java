package br.com.iterative.chance.fragments;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.PrincipalActivity;
import br.com.iterative.chance.adapters.ResumoAjudaAdapter;
import br.com.iterative.chance.models.Categoria;
import br.com.iterative.chance.models.ResumoAjudaRequest;
import br.com.iterative.chance.models.ResumoAjudaResponse;
import br.com.iterative.rede.chance.R;

/**
 * Created by pedrohenriqueborges on 10/28/14.
 */
public class SolicitacoesAjudaFragment extends Fragment {


    public boolean recarregarTela;
    ResumoAjudaResponse dataSet;
    ExpandableListView expandableListView;
    SolicitacoesAjudaFragment context;

    public void setDataSet(ResumoAjudaResponse dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context = this;
        View view = inflater.inflate(R.layout.fragment_solicitacoes, container, false);
        expandableListView = (ExpandableListView) view.findViewById(R.id.list_solicitacoes);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (dataSet == null)
            dataSet = new ResumoAjudaResponse();

        dataSet.Categorias = new Categoria[]{};

        ResumoAjudaAdapter adapter = new ResumoAjudaAdapter(getActivity(), dataSet, this);

        expandableListView.setAdapter(adapter);
        expandableListView.setDividerHeight(0);
        expandableListView.setGroupIndicator(null);
        expandableListView.expandGroup(0);
        expandableListView.expandGroup(1);
        expandableListView.expandGroup(2);
        expandableListView.expandGroup(3);
        expandableListView.expandGroup(4);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {

                return true;
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();

        // "aqui_vai_um_texto"
        ((PrincipalActivity) getActivity()).setTitleBarText(getResources().getString(R.string.central_de_ajuda));

        if (recarregarTela) {
            recarregarTela = false;

            ResumoAjudaTask resumoAjudaTask = new ResumoAjudaTask();
            resumoAjudaTask.execute();
        }
    }

    private final class ResumoAjudaTask extends AsyncTask<Object, Object, ResumoAjudaResponse> {

        protected ResumoAjudaResponse doInBackground(Object... params) {
            ResumoAjudaRequest request = new ResumoAjudaRequest(Helper.getToken());

            return Helper.post(Helper.URL_POST_AJUDA_RESUMO, request, ResumoAjudaResponse.class);
        }

        protected void onPostExecute(ResumoAjudaResponse result) {
            if (result.Sucesso) {

                dataSet = result;

                dataSet.Categorias = new Categoria[]{};


                ResumoAjudaAdapter adapter = new ResumoAjudaAdapter(getActivity(), dataSet, context);

                expandableListView.setAdapter(adapter);


            } else {
                new AlertDialog.Builder(getActivity()).setMessage(result.Mensagem).setPositiveButton("OK", null).create().show();
            }
        }
    }

}
