package br.com.iterative.chance.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.GpsActivity;
import br.com.iterative.chance.adapters.CategoriaAdapter;
import br.com.iterative.chance.listeners.CategoriaListener;
import br.com.iterative.chance.models.Categoria;
import br.com.iterative.chance.models.Localizacao;
import br.com.iterative.chance.models.OferecerAjudaRequest;
import br.com.iterative.chance.models.Oferta;
import br.com.iterative.chance.models.PedirAjudaRequest;
import br.com.iterative.chance.models.ResponseBase;
import br.com.iterative.chance.tasks.PedirAjudaTask;
import br.com.iterative.rede.chance.R;

@SuppressLint("ValidFragment")
public class PedirAjudaFragment extends Fragment implements OnClickListener, CategoriaListener {

    private ImageView foto;
    private MultiAutoCompleteTextView texto;
    private MultiAutoCompleteTextView textoContato;
    private Button botaoPedirAjuda;
    private int categoriaId;
    private String categoriaFoto;
    private int tipoAjuda;
    private int pedidoId;
    private Categoria[] categorias;
    private View viewCategoria;
    private TextView textCategoriaSelecionada;


    private ToggleButton toggleOferecer;
    private ToggleButton togglePedir;

    public PedirAjudaFragment() {

    }

    public PedirAjudaFragment(Categoria[] categorias) {

        List<Categoria> categoriaList = new ArrayList<Categoria>();

        for (int i = 0; i < categorias.length; i++) {

            if (i == 0)
                continue;

            categoriaList.add(categorias[i]);
        }

        this.categorias = categoriaList.toArray(new Categoria[categoriaList.size()]);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		categoriaId = getArguments().getInt("categoriaId");
//		categoriaFoto = getArguments().getString("categoriaFoto");
//		tipoAjuda = getArguments().getInt("tipoAjuda");
        if (getArguments() != null && getArguments().containsKey("pedidoId")) {
            pedidoId = getArguments().getInt("pedidoId");
            new ObterAjudaTask().execute(pedidoId);

        }
        return inflater.inflate(R.layout.pedir_ajuda, container, false);
    }

    @Override
    public void onStart() {

        FragmentActivity activity = getActivity();

        textCategoriaSelecionada = (TextView) activity.findViewById(R.id.text_categoria_selecionada);
        viewCategoria = (View) activity.findViewById(R.id.view_categoria);
        toggleOferecer = (ToggleButton) activity.findViewById(R.id.toggle_oferecer);
        togglePedir = (ToggleButton) activity.findViewById(R.id.toggle_pedir);


        texto = (MultiAutoCompleteTextView) activity.findViewById(R.id.texto);
        textoContato = (MultiAutoCompleteTextView) activity.findViewById(R.id.textoContato);


        botaoPedirAjuda = (Button) activity.findViewById(R.id.botaoPedirAjuda);
        if (pedidoId > 0) {
            botaoPedirAjuda.setText("Salvar");
            activity.findViewById(R.id.botaoExcluir).setVisibility(View.VISIBLE);
            activity.findViewById(R.id.botaoExcluir).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View view) {
                    new ExcluirAjudaTask().execute(pedidoId);
                }
            });
        }

//		Helper.downloadImageFromURL(foto, categoriaFoto);

        viewCategoria.setOnClickListener(this);
        botaoPedirAjuda.setOnClickListener(this);
        togglePedir.setOnClickListener(this);
        toggleOferecer.setOnClickListener(this);

        onClick(toggleOferecer);

        super.onStart();
    }

    @Override
    public void onClick(View v) {

        if (v == togglePedir) {


            togglePedir.setChecked(true);
            toggleOferecer.setChecked(false);
            tipoAjuda = 0;
            botaoPedirAjuda.setText(R.string.publicar_pedido_ajuda);
            texto.setHint(R.string.descreva_pedido_ajuda);

        } else if (v == toggleOferecer) {

            togglePedir.setChecked(false);
            toggleOferecer.setChecked(true);
            tipoAjuda = 1;
            botaoPedirAjuda.setText(R.string.publicar_oferta_ajuda);
            texto.setHint(R.string.descreva_oferta_ajuda);
        } else if (v == viewCategoria) {
            showDialogCategoria();
        } else {

            if (categoriaId == 0) {

                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

                dialogBuilder.setTitle("Ocorreu um erro");

                dialogBuilder.setMessage("Selecione uma categoria!");

                dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });


                AlertDialog dialog = dialogBuilder.create();

                dialog.show();

                return;

            }

            Localizacao localizacao = new Localizacao();

            if (((GpsActivity) getActivity()).mLocationClient != null && ((GpsActivity) getActivity()).mLocationClient.isConnected()) {

                Location lastLocation = ((GpsActivity) getActivity()).mLocationClient.getLastLocation();


                if (lastLocation != null) {
                    localizacao.Latitude = (float) lastLocation.getLatitude();
                    localizacao.Longitude = (float) lastLocation.getLongitude();

                }

            }

            PedirAjudaRequest request = new PedirAjudaRequest(texto, textoContato, localizacao, categoriaId, tipoAjuda);
            request.PedidoId = pedidoId;
            new PedirAjudaTask(this).execute(request);
        }
    }

    private void showDialogCategoria() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Selecione uma Categoria");
        dialog.setContentView(R.layout.dialog_categoria);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);


        ListView listView = (ListView) dialog.findViewById(R.id.list_categorias);


        CategoriaAdapter produtoAdapter = new CategoriaAdapter(getActivity(), categorias, dialog);

        produtoAdapter.setListener(this);
        listView.setAdapter(produtoAdapter);


        try {
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public void onSelectCategoria(Categoria categoria, DialogInterface dialogInterface) {

        categoriaId = categoria.CategoriaId;
        textCategoriaSelecionada.setText(categoria.Nome);
        dialogInterface.dismiss();

    }

    @Override
    public void onSelectCategoriaForRequest(Categoria categoria, DialogInterface dialogInterface) {

        categoriaId = categoria.CategoriaId;
        textCategoriaSelecionada.setText(categoria.Nome);
        dialogInterface.dismiss();
    }


    private final class ExcluirAjudaTask extends AsyncTask<Integer, Object, ResponseBase> {

        @Override
        protected ResponseBase doInBackground(Integer... args) {
            ResponseBase response = null;
            if (!isCancelled()) {
                OferecerAjudaRequest request = new OferecerAjudaRequest(args[0]);
                response = Helper.post(Helper.URL_POST_AJUDA_EXCLUIR, request, ResponseBase.class);

            }
            return response;
        }

        @Override
        protected void onPostExecute(ResponseBase response) {
            try {
                if (response == null) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.falha_conectar_servidor), Toast.LENGTH_LONG).show();
                    return;
                }
                if (!response.Sucesso) {

                    Toast.makeText(getActivity(), response.Mensagem, Toast.LENGTH_LONG).show();
                    return;
                }
                Toast.makeText(getActivity(), getResources().getString(R.string.pedido_ajuda_excluido), Toast.LENGTH_LONG).show();
                PerfilFragment.recarregarTela = true;
                Helper.setPerfilConta(null);
                PedirAjudaFragment.this.getFragmentManager().popBackStack();
            }
            catch (Exception e){

                Toast.makeText(getActivity(), getResources().getString(R.string.falha_conectar_servidor), Toast.LENGTH_LONG).show();
            }
        }

    }

    private final class ObterAjudaTask extends AsyncTask<Integer, Object, Oferta> {

        @Override
        protected Oferta doInBackground(Integer... args) {
            Oferta response = null;
            if (!isCancelled()) {
                OferecerAjudaRequest request = new OferecerAjudaRequest(args[0]);
                response = Helper.post(Helper.URL_POST_AJUDA_VER, request, Oferta.class);

            }
            return response;
        }

        @Override
        protected void onPostExecute(Oferta oferta) {
            if (oferta == null)
                return;
            texto.setText(oferta.Texto);
            textoContato.setText(oferta.TextoDeContato);
            categoriaId = oferta.Categoria.CategoriaId;
            textCategoriaSelecionada.setText(oferta.Categoria.Nome);

        }

    }

}
