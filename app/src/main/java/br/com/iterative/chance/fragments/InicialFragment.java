package br.com.iterative.chance.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;

import java.util.Arrays;

import br.com.iterative.chance.Helper;
import br.com.iterative.chance.activities.CadastroActivity;
import br.com.iterative.chance.activities.LoginActivity;
import br.com.iterative.chance.tasks.FacebookLoginTask;
import br.com.iterative.rede.chance.R;

public class InicialFragment extends Fragment implements DialogInterface.OnClickListener   {


    private View loading;
    private static final String TAG = "InicialFragment";
    private UiLifecycleHelper uiHelper;
    private boolean onProcess;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        System.out.println("state " + state);
        if (state.isOpened()) {
            Log.i(TAG, "Logged in as " + session.getAccessToken());


            if (!onProcess) new FacebookLoginTask(getActivity(),this).execute(session.getAccessToken());
            onProcess = true;

        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Helper.getPerfil() != 0) {

            onProcess = false;
            Session session = Session.getActiveSession();
            if (session != null && (session.isOpened() || session.isClosed())) {
                onSessionStateChange(session, session.getState(), null);
            }
        }

        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    public void connectToFacebook() {

        Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            session.openForRead(new Session.OpenRequest(this)
                    .setPermissions(Arrays.asList("public_profile", "user_birthday", "email"))
                    .setCallback(callback));
        } else {
            Session.openActiveSession(getActivity(), this, true, callback);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_inicial, container, false);


        loading = view.findViewById(R.id.loading);

        view.findViewById(R.id.botaoLogin).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.botaoCriarConta).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CadastroActivity.class);
                startActivity(intent);
            }
        });

        Button authButton = (Button) view.findViewById(R.id.authButton);

        authButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                showProgress(true);
                connectToFacebook();

            }
        });

		
		/*view.findViewById(R.id.authButton).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(InicialFragment.this.getActivity()).setMessage("Servi�o indispon�vel, por favor tente mais tarde.").setPositiveButton("OK", null).create().show();
			}
		});*/

        return view;
    }


    private void showProgress(boolean show){

        loading.setVisibility(show? View.VISIBLE: View.GONE);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

        dialogInterface.dismiss();
        showProgress(false);

    }
}
