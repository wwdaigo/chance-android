package br.com.iterative.chance.models;

import android.widget.EditText;

public class AlterarSenhaRequest extends RequestBase {

	private String NovaSenha;
	private String SenhaAtual;

	public AlterarSenhaRequest(EditText senhaAtual, EditText novaSenha) 
	{
		this.SenhaAtual = senhaAtual.getText().toString();
		this.NovaSenha = novaSenha.getText().toString();
	}
}
